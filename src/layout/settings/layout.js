import React, { useState, useEffect } from 'react';
import Content from '../content/Content';
import { Icon } from '../../components/Component';
import { Card } from 'reactstrap';
import { Route, Switch, Link } from 'react-router-dom';
import AppSettings from '../../pages/settings/app-settings/Index';
import CompanySettings from '../../pages/settings/company-settings/Index';
import PaymentSettings from '../../pages/settings/payment-settings/Index';
import NotificationSettings from '../../pages/settings/notification-settings/Index';
import TaxSettings from '../../pages/settings/tax-settings/Index';
import LanguageSettings from '../../pages/settings/language-settings/Index';
import SocialLoginSettings from '../../pages/settings/social-settings/Index';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';

const Layout = ({ pageTitle }) => {
    const [sm, updateSm] = useState(false);
    const [mobileView, setMobileView] = useState(false);

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // function to change the design view under 990 px
    const viewChange = () => {
        if (window.innerWidth < 990) {
            setMobileView(true);
        } else {
            setMobileView(false);
            updateSm(false);
        }
    };

    useEffect(() => {
        viewChange();
        window.addEventListener('load', viewChange);
        window.addEventListener('resize', viewChange);
        document.getElementsByClassName('nk-header')[0].addEventListener('click', function () {
            updateSm(false);
        });
        return () => {
            window.removeEventListener('resize', viewChange);
            window.removeEventListener('load', viewChange);
        };
    }, []);

    return (
        <>
            <Content>
                <Card className="card-bordered">
                    <div className="card-aside-wrap">
                        <div
                            className={`card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg ${
                                sm ? 'content-active' : ''
                            }`}
                        >
                            <div className="card-inner-group">
                                <div className="card-inner p-0">
                                    <ul className="link-list-menu">

                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/company-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/company-settings` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="building"></Icon>
                                                <span>Company Settings</span>
                                            </Link>
                                        </li>

                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/app-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/app-settings` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="opt-dot"></Icon>
                                                <span>App Settings</span>
                                            </Link>
                                        </li>

                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/payment-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/payment-settings`
                                                        ? 'active'
                                                        : ''
                                                }
                                            >
                                                <Icon name="money"></Icon>
                                                <span>Payment Credentials</span>
                                            </Link>
                                        </li>

                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/notification-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/notification-settings`
                                                        ? 'active'
                                                        : ''
                                                }
                                            >
                                                <Icon name="bell"></Icon>
                                                <span>Notification Settings</span>
                                            </Link>
                                        </li>

                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/tax-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/tax-settings` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="coins"></Icon>
                                                <span>Tax Settings</span>
                                            </Link>
                                        </li>

                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/language-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/language-settings` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="globe"></Icon>
                                                <span>Language Settings</span>
                                            </Link>
                                        </li>
                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/social-login-settings`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/social-login-settings` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="list-round"></Icon>
                                                <span>Social Settings</span>
                                            </Link>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div
                            // className="card-inner card-inner-lg"
                            className={
                                (window.location.pathname === `${process.env.PUBLIC_URL}/account/payment-settings` || window.location.pathname === `${process.env.PUBLIC_URL}/account/notification-settings` || window.location.pathname === `${process.env.PUBLIC_URL}/account/social-login-settings`) ? 'card-inner card-inner-lg card-setting-full-width' : 'card-inner card-inner-lg'
                            }
                        >
                            {sm && mobileView && <div className="toggle-overlay" onClick={() => updateSm(!sm)}></div>}
                            <Switch>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/company-settings`}
                                    render={() => <CompanySettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/app-settings`}
                                    render={() => <AppSettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/payment-settings`}
                                    render={() => <PaymentSettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/notification-settings`}
                                    render={() => <NotificationSettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/tax-settings`}
                                    render={() => <TaxSettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/language-settings`}
                                    render={() => <LanguageSettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/social-login-settings`}
                                    render={() => <SocialLoginSettings pageTitle={pageTitle} updateSm={updateSm} sm={sm} />}
                                ></Route>
                            </Switch>
                        </div>
                    </div>
                </Card>
            </Content>
        </>
    );
};

Layout.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Layout;
