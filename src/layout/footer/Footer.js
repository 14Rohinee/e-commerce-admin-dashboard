import React from 'react';

const Footer = () => {
    return (
        <div className="nk-footer">
            <div className="container-fluid">
                <div className="nk-footer-wrap">
                    <div className="nk-footer-copyright">
                        {' '}
            &copy; Rohinee Tandekar 2024. All Rights Reserved.
                    </div>
                    <div className="nk-footer-links">
                        <p>Beta Version</p>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Footer;
