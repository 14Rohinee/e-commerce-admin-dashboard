const menu = [
    {
        icon: 'home',
        text: 'Dashboard',
        link: '/account',
        extraLinks: []
    },
    {
        icon: 'users',
        text: 'Users',
        active: false,
        link: '/account/users',
        extraLinks: ['account/users/create', 'account/users/edit/:id', 'account/users/show/:id']
    },
    {
        icon: 'masonry',
        text: 'Categories',
        active: false,
        link: '/account/categories',
        extraLinks: ['account/categories/create', 'account/categories/edit/:id']
    },
    {
        icon: 'list-index',
        text: 'Sub Categories',
        active: false,
        link: '/account/sub-categories',
        extraLinks: ['account/sub-categories/create', 'account/sub-categories/edit/:id']
    },
    {
        icon: 'tags',
        text: 'Brands',
        active: false,
        link: '/account/brands',
        extraLinks: ['account/brands/create', 'account/brands/edit/:id']
    },
    {
        icon: 'card-view',
        text: 'Products',
        active: false,
        link: '/account/products',
        extraLinks: ['account/products/create', 'account/products/edit/:id']
    },
    {
        icon: 'ticket',
        text: 'Coupons',
        active: false,
        link: '/account/coupons',
        extraLinks: ['account/coupons/create', 'account/coupons/edit/:id']
    },
    {
        icon: 'cart',
        text: 'Orders',
        link: '/account/orders',
        extraLinks: ['account/orders/show/:id']
    },
    {
        icon: 'tranx',
        text: 'Payments',
        link: '/account/payments',
        extraLinks: ['account/payments/view']
    },
    {
        icon: 'setting',
        text: 'Settings',
        link: '/account/company-settings',
        extraLinks: ['account/app-settings', 'account/notification-settings', 'account/payment-settings', 'account/tax-settings', 'account/social-login-settings', 'account/language-settings', 'account/tax-settings']
    }
];
export default menu;
