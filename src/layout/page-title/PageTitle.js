import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { useTitle } from '../../contexts/TitleContext';
import { Link, useLocation } from 'react-router-dom';

const PageTitle = () => {
    const { title } = useTitle();
    const location = useLocation();
    const pathSegments = location.pathname.split('/').filter((segment) => segment !== '');

    return (
        <div className="nk-news-list mt-1">
            <div className='d-flex'>
                <h6>
                    { title || 'No Page Title Given' }
                </h6>
            </div>

            <Breadcrumb>
                {pathSegments.map((segment, index) => (
                    <BreadcrumbItem key={index} active={index === pathSegments.length - 1}>
                        {index === pathSegments.length - 1
                            ? (
                                // Remove - from the segment
                                segment.replace(/-/g, ' ')
                            )
                            : (
                                <Link to={`/${pathSegments.slice(0, index + 1).join('/')}`}>
                                    {
                                        // Remove - from the segment
                                        segment.replace(/-/g, ' ')
                                    }
                                </Link>
                            )}
                    </BreadcrumbItem>
                ))}
            </Breadcrumb>
        </div>
    );
};

export default PageTitle;
