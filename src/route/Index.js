import React, { Suspense, useLayoutEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { RedirectAs404 } from '../utils/Utils';

import Homepage from '../pages/Homepage';
import UserDetailsPage from '../pages/users/UserDetails';
import UserProfileLayout from '../pages/users/UserProfileLayout';

// Products
import ProductList from '../pages/products/Index';
import ProductEdit from '../pages/products/Edit';
import CreateProduct from '../pages/products/Create';

// Employees
import UserList from '../pages/users/Index';
import UserCreate from '../pages/users/Create';
import UserEdit from '../pages/users/Edit';

// Orders
import OrderList from '../pages/orders/Index';
import OrderDetails from '../pages/orders/Show';

// Orders
import PaymentList from '../pages/payments/Index';
import PaymentDetails from '../pages/payments/Show';

// Categories
import CategoryList from '../pages/categories/Index';
import CreateCategory from '../pages/categories/Create';
import EditCategory from '../pages/categories/Edit';

// Sub Categories
import SubCategoryList from '../pages/sub-categories/Index';
import CreateSubCategory from '../pages/sub-categories/Create';
import EditSubCategory from '../pages/sub-categories/Edit';

// Brands
import BrandList from '../pages/brands/Index';
import CreateBrand from '../pages/brands/Create';
import EditBrand from '../pages/brands/Edit';

// Brands
import CouponList from '../pages/coupons/Index';
import CreateCoupon from '../pages/coupons/Create';
import EditCoupon from '../pages/coupons/Edit';

// Settings
import Layout from '../layout/settings/layout';

const Pages = () => {
    useLayoutEffect(() => {
        window.scrollTo(0, 0);
    });

    return (
        <Suspense fallback={<div />}>
            <Switch>

                {/* Dashboard */}
                <Route exact path={`${process.env.PUBLIC_URL}/account`} render={() => <Homepage pageTitle="Dashboard" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/dashboard`} render={() => <Homepage pageTitle="Dashboard" />}></Route>

                {/* START: Users Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/users`} render={() => <UserList pageTitle="Users" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/users/create`} render={() => <UserCreate pageTitle="Add User" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/users/edit/:id`} render={() => <UserEdit pageTitle="Update User" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/users/show/:id`} render={() => <UserDetailsPage pageTitle="User Detail" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/profile/`} render={() => <UserProfileLayout pageTitle="Personal Information" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/users/show/:id`} render={() => <UserDetailsPage pageTitle="User Details" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/user-profile-regular/`} render={() => <UserProfileLayout pageTitle="Profile" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/user-profile-notification/`} render={() => <UserProfileLayout pageTitle="Notifications" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/user-profile-activity/`} render={() => <UserProfileLayout pageTitle="Profile Activity" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/user-profile-setting/`} render={() => <UserProfileLayout pageTitle="Security Settings" />}></Route>
                {/* START: Users Routes */}

                {/* START: Products */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/products`} render={() => <ProductList pageTitle="Products" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/products/create`} render={() => <CreateProduct pageTitle="Add Product" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/products/edit/:id`} render={() => <ProductEdit pageTitle="Update Product" />}></Route>
                {/* END: Products */}

                {/* START: Orders Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/orders`} render={() => <OrderList pageTitle="Orders" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/orders/show/:id`} render={() => <OrderDetails pageTitle="Order Details" />}></Route>
                {/* END: Orders Routes */}

                {/* START: Orders Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/payments`} render={() => <PaymentList pageTitle="Payments" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/payments/show/:id`} render={() => <PaymentDetails pageTitle="Payment Details" />}></Route>
                {/* END: Orders Routes */}

                {/* START: Categories Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/categories`} render={() => <CategoryList pageTitle="Categories" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/categories/create`} render={() => <CreateCategory pageTitle="Add Category" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/categories/edit/:id`} render={() => <EditCategory pageTitle="Update Category" />}></Route>
                {/* END: Categories Routes */}

                {/* START: Sub Categories Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/sub-categories`} render={() => <SubCategoryList pageTitle="Sub Categories" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/sub-categories/create`} render={() => <CreateSubCategory pageTitle="Add Sub Category" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/sub-categories/edit/:id`} render={() => <EditSubCategory pageTitle="Update Sub Category" />}></Route>
                {/* END: Sub Categories Routes */}

                {/* START: Brands Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/brands`} render={() => <BrandList pageTitle="Brands" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/brands/create`} render={() => <CreateBrand pageTitle="Add Brand" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/brands/edit/:id`} render={() => <EditBrand pageTitle="Update Brand" />}></Route>
                {/* END: Brands Routes */}

                {/* START: Setting Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/company-settings`} render={() => <Layout pageTitle="Company Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/app-settings`} render={() => <Layout pageTitle="App Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/payment-settings`} render={() => <Layout pageTitle="Payment Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/notification-settings`} render={() => <Layout pageTitle="Notification Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/sms-settings`} render={() => <Layout pageTitle="SMS Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/tax-settings`} render={() => <Layout pageTitle="Tax Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/language-settings`} render={() => <Layout pageTitle="Language Settings" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/social-login-settings`} render={() => <Layout pageTitle="Social Auth Settings" />}></Route>
                {/* END: Setting Routes */}

                {/* START: Coupons Routes */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/coupons`} render={() => <CouponList pageTitle="Coupons" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/coupons/create`} render={() => <CreateCoupon pageTitle="Add Coupon" />}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/coupons/edit/:id`} render={() => <EditCoupon pageTitle="Update Coupon" />}></Route>
                {/* END: Coupons Routes */}

                <Route component={RedirectAs404}></Route>
            </Switch>
        </Suspense>
    );
};
export default Pages;
