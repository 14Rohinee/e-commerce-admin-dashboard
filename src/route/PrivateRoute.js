import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const auth = localStorage.getItem('accessToken');

const PrivateRoute = ({ exact, component: Component, ...rest }) => (
    <Route
        exact={!!exact}
        rest
        render={(props) =>
            auth
                ? (
                    <Component {...props} {...rest}></Component>
                )
                : (
                    <Redirect to={`${process.env.PUBLIC_URL}/login`}></Redirect>
                )
        }
    ></Route>
);

PrivateRoute.propTypes = {
    exact: PropTypes.bool,
    component: PropTypes.elementType.isRequired
};

export default PrivateRoute;
