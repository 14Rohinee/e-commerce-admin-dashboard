import React, { useEffect } from 'react';
import { useQuill } from 'react-quilljs';
import PropTypes from 'prop-types';

const QuillComponent = (props) => {
    console.log(props.value);
    const placeholder = props.placeholder || 'Write something awesome...';
    const { quill, quillRef } = useQuill({ placeholder });

    useEffect(() => {
        if (quill) {
            const delta = quill.clipboard.convert(props.value || '');
            quill.setContents(delta);

            quill.on('text-change', (delta, oldDelta, source) => {
                props.onChange(quillRef.current.firstChild.innerHTML);
            });
        }
    }, [quill, props.value, quillRef]);

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <div ref={quillRef} />
        </div>
    );
};

QuillComponent.propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};

export default QuillComponent;
