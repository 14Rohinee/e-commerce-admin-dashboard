import React from 'react';
import PropTypes from 'prop-types';

const ColorOptions = ({ value, label }) => {
    return (
        <div className="d-flex">
            <span className={`dot dot-${value} m-1`}></span>
            {label}
        </div>
    );
};

ColorOptions.propTypes = {
    value: PropTypes.string,
    label: PropTypes.string
};

export default ColorOptions;
