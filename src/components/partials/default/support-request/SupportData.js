export const supportData = [
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'VL',
        theme: 'purple',
        name: 'Vincent Lopez',
        text: 'Thanks for contact us with your issues...',
        time: '6 min ago',
        status: 'Pending'
    },
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'DM',
        theme: 'purple-dim',
        name: 'Daniel Moore',
        text: 'Thanks for contacting us with your issues',
        time: '2 hours ago',
        status: 'Open'
    },
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'LH',
        theme: 'purple',
        name: 'Larry Henry',
        text: 'Thanks for contact us with your issues...',
        time: '3 hours ago',
        status: 'Solved'
    }
];
