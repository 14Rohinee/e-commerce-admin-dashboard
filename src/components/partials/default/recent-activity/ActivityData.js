export const activityData = [
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'KJ',
        theme: 'success',
        name: 'Kieth Jensen',
        activity: 'requested to Withdrawal',
        time: '2 hours ago'
    },
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'HS',
        theme: 'warning',
        name: 'Harry Simpson',
        activity: 'placed a Order',
        time: '2 hours ago'
    },
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'SM',
        theme: 'azure',
        name: 'Stephenie Marshall',
        activity: 'got a huge bonus',
        time: '2 hours ago'
    },
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'NC',
        theme: 'purple',
        name: 'Nicolas Carr',
        activity: 'deposited funds',
        time: '2 hours ago'
    },
    {
        img: 'https://www.w3schools.com/howto/img_avatar2.png',
        initial: 'TM',
        theme: 'pink',
        name: 'Timothy Moreno',
        activity: 'placed a Order',
        time: '2 hours ago'
    }
];
