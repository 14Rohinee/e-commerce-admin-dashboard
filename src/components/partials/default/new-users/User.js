import React, { useEffect, useState } from 'react';
import UserAvatar from '../../../user/UserAvatar';
import Icon from '../../../icon/Icon';
import { DropdownMenu, DropdownToggle, UncontrolledDropdown, CardTitle, DropdownItem } from 'reactstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';
import { showToast } from '../../../../utils/Utils';
import { APP_URL, findUpper, token } from '../../../../utils/Constants';

const NewsUsers = () => {
    const [data, setData] = useState([]);
    const history = useHistory();
    const theme = ['purple-dim', 'danger-dim', 'warning-dim', 'success-dim'];

    // Function to get all the users
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/users`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        // Get only latest 5 users
        setData(response.data.users.slice(0, 5));
    };

    // Function to get all the users on page load
    useEffect(() => {
        getData();
    }, []);

    const DropdownTrans = (id) => {
        return (
            <UncontrolledDropdown>
                <DropdownToggle tag="a" className="dropdown-toggle btn btn-icon btn-trigger me-n1">
                    <Icon name="more-h"></Icon>
                </DropdownToggle>
                <DropdownMenu end>
                    <ul className="link-list-opt no-bdr">
                        <li>
                            <DropdownItem
                                tag="a"
                                href="#dropdownitem"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    history.push(
                                        `${process.env.PUBLIC_URL}/account/users/edit/${id.id}`
                                    );
                                }}
                            >
                                <Icon name="pen"></Icon>
                                <span>Edit</span>
                            </DropdownItem>
                        </li>
                    </ul>
                </DropdownMenu>
            </UncontrolledDropdown>
        );
    };
    return (
        <div className="card-inner-group">
            <div className="card-inner">
                <div className="card-title-group">
                    <CardTitle>
                        <h6 className="title">New Users</h6>
                    </CardTitle>
                    <div className="card-tools">
                        <Link to={`${process.env.PUBLIC_URL}/account/users`} className="link">
                            View All
                        </Link>
                    </div>
                </div>
            </div>
            {data.map((item, idx) => {
                return (
                    <div className="card-inner card-inner-md" key={idx}>
                        <div className="user-card">
                            <UserAvatar
                                theme={ theme[Math.floor(Math.random() * theme.length)] }
                                text={findUpper(item.name)}>
                            </UserAvatar>

                            <div className="user-info">
                                <span className="lead-text">{item.name}</span>
                                <span className="sub-text">{item.email}</span>
                            </div>
                            <div className="user-action">
                                <DropdownTrans id={item._id} />
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
};
export default NewsUsers;
