import React, { useState } from 'react';
import PropTypes from 'prop-types';

const InputSwitch = ({ label, id, checked }) => {
    const [inputCheck, setCheck] = useState(!!checked);

    return (
        <>
            <input
                type="checkbox"
                className="custom-control-input"
                defaultChecked={inputCheck}
                onClick={() => setCheck(!inputCheck)}
                id={id}
            />
            <label className="custom-control-label" htmlFor={id}>
                {label}
            </label>
        </>
    );
};

InputSwitch.propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,
    checked: PropTypes.bool
};

export default InputSwitch;
