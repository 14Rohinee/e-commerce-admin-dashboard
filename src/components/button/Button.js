import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Button = ({ color, size, className, outline, disabled, ...props }) => {
    const buttonClass = classNames({
        btn: true,
        [`btn-${color}`]: !outline,
        [`btn-outline-${color}`]: outline,
        [`btn-${size}`]: size,
        disabled,
        [`${className}`]: className
    });
    return (
        <button className={buttonClass} {...props}>
            {props.children}
        </button>
    );
};

Button.propTypes = {
    color: PropTypes.string,
    size: PropTypes.string,
    className: PropTypes.string,
    outline: PropTypes.bool,
    disabled: PropTypes.bool,
    children: PropTypes.node
};

export default Button;
