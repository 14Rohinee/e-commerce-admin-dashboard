import React from 'react';
import PropTypes from 'prop-types';
import { BlockHead, BlockHeadContent, BlockBetween, Icon } from '../Component';
import ButtonPrimary from '../form/ButtonPrimary';
import { Spinner } from 'reactstrap';

const Header = ({ action, fieldLabel, redirectUrl, exportData, exportLoading }) => {
    return (
        <BlockHead>
            <BlockBetween>
                <BlockHeadContent>
                    <ul className="nk-block-tools g-3">
                        {!action && (
                            <li className="nk-block-tools-opt">
                                <ButtonPrimary
                                    fieldLabel={fieldLabel}
                                    icon="plus"
                                    redirectUrl={redirectUrl} />
                            </li>)}
                        <li>
                            <button type='button' disabled={exportLoading} className="btn btn-white btn-outline-light"
                                onClick={async () => {
                                    await exportData();
                                } }>
                                {exportLoading
                                    ? (
                                        <>
                                            <Spinner size="sm" color="secondary" />
                                            <span className='text-secondary'>Downloading...</span>
                                        </>
                                    )
                                    : (
                                        <>
                                            <Icon name="download-cloud"></Icon>
                                            <span>Export</span>
                                        </>
                                    )}
                            </button>
                        </li>
                    </ul>
                </BlockHeadContent>
            </BlockBetween>
        </BlockHead>
    );
};

Header.propTypes = {
    action: PropTypes.string,
    fieldLabel: PropTypes.string,
    redirectUrl: PropTypes.string.isRequired,
    exportData: PropTypes.func,
    exportLoading: PropTypes.bool
};

export default Header;
