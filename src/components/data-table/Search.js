import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from '../Component';

export const Search = ({ toggleSearchFilter }) => {
    return (
        <a
            href="#search"
            onClick={(ev) => {
                ev.preventDefault();
                toggleSearchFilter();
            }}
            className="btn btn-icon search-toggle toggle-search"
        >
            <Icon name="search"></Icon>
        </a>
    );
};

export const OnSearch = ({ placeholder, setSearchText, toggleSearchFilter, onSearchText }) => {
    return (
        <div className="card-body">
            <div className="search-content">
                <Button
                    className="search-back btn-icon toggle-search active"
                    onClick={() => {
                        setSearchText('');
                        toggleSearchFilter();
                    }}
                >
                    <Icon name="arrow-left"></Icon>
                </Button>
                <input
                    type="text"
                    className="border-transparent form-focus-none form-control"
                    placeholder={placeholder}
                    value={onSearchText}
                    onChange={(e) =>
                        setSearchText(e.target.value)
                    }
                />
                <Button className="search-submit btn-icon">
                    <Icon name="search"></Icon>
                </Button>
            </div>
        </div>
    );
};

Search.propTypes = {
    toggleSearchFilter: PropTypes.func.isRequired
};

OnSearch.propTypes = {
    placeholder: PropTypes.string.isRequired,
    setSearchText: PropTypes.func.isRequired,
    toggleSearchFilter: PropTypes.func.isRequired,
    onSearchText: PropTypes.string.isRequired
};
