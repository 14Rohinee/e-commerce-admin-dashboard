import React from 'react';
import PropTypes from 'prop-types';
import { DataTableRow } from '../Component';

export const TableHeader = ({ headers, onSelectAllChange, table }) => {
    // Return the table head row
    return (
        <>
            {table !== 'Payment' && (
                <DataTableRow className="nk-tb-col-check">
                    <div className="custom-control custom-control-sm custom-checkbox notext">
                        <input type="checkbox" className="custom-control-input" onChange={(e) =>
                            onSelectAllChange(e) } id="uid"
                        />
                        <label className="custom-control-label" htmlFor="uid"></label>
                    </div>
                </DataTableRow>
            )}
            {headers.map((row, index) => (
                <DataTableRow key={index}>
                    <span className="sub-text">{row}</span>
                </DataTableRow>
            ))}
        </>
    );
};

TableHeader.propTypes = {
    headers: PropTypes.array.isRequired,
    onSelectAllChange: PropTypes.func,
    table: PropTypes.string
};

export default TableHeader;
