import React from 'react';
import { Button, Icon, RSelect } from '../Component';
import PropTypes from 'prop-types';

const BulkAction = ({ options, bulkAction, onBulkActionDropdownChange, onBulkActionButtonClick }) => {
    let disabled = true;

    if (bulkAction.bulkActionOption !== '' && bulkAction.actionCheckboxes.length !== 0) {
        disabled = false;
    }

    return (
        <div className="form-inline flex-nowrap gx-3">
            <div className="form-wrap">
                <RSelect
                    options={options}
                    className="w-130px"
                    placeholder="Bulk Action"
                    onChange={(e) => onBulkActionDropdownChange(e)}
                    id="bulk-action"
                />
            </div>
            <div className="btn-wrap">
                <span className="d-none d-md-block">
                    <Button
                        disabled={disabled}
                        color="light"
                        outline
                        className="btn-dim"
                        onClick={(e) => onBulkActionButtonClick(e)}
                    >
                        Apply
                    </Button>
                </span>
                <span className="d-md-none">
                    <Button
                        color="light"
                        outline
                        disabled={bulkAction.bulkActionOption === '' && bulkAction.actionCheckboxes.length === 0}
                        className="btn-dim  btn-icon"
                        onClick={(e) => onBulkActionButtonClick(e)
                        }
                    >
                        <Icon name="arrow-right"></Icon>
                    </Button>
                </span>
            </div>
        </div>
    );
};

BulkAction.propTypes = {
    options: PropTypes.array,
    bulkAction: PropTypes.object,
    onBulkActionDropdownChange: PropTypes.func,
    onBulkActionButtonClick: PropTypes.func
};

export default BulkAction;
