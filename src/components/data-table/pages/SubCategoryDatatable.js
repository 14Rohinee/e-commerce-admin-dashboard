import React, { useEffect, useState } from 'react';
import TableFilter from '../filter/TableFilter';
import Pagination from '../Pagination';
import { APP_URL, capitalize, formatDate } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import { DataTable, DataTableBody, DataTableHead, DataTableItem, DataTableRow } from '../../table/DataTable';
import axios from 'axios';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import TableHeader from '../TableHeader';
import { Link, useHistory } from 'react-router-dom/cjs/react-router-dom';
import { Icon } from '../../Component';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import Checkbox from '../Checkbox';
import TableLoader from '../../page-loader/TableLoader';

function SubCategoryDatatable ({ data, setData, getData, originalData, total, setTotal, pagination, setPagination }) {
    const history = useHistory();
    const [onSearch, setonSearch] = useState(true);
    const [onSearchText, setSearchText] = useState('');
    const [tablesm, updateTableSm] = useState(false);
    const [tableLoading, setTableLoading] = useState(false);
    const headers = ['Sub Category Name', 'CategoryName', 'Status', 'Created At', 'Updated At', 'Action'];
    const placeholder = 'Search by sub category name, category name or status';
    const [action, setAction] = useState({ bulkActionOption: '', actionCheckboxes: [] });


    // State for filters
    const [appliedFilters, setAppliedFilter] = useState({
        start: null,
        end: null,
        status: ''
    });

    // Function to set the action to be taken in table header
    const onBulkActionDropdownChange = (e) => {
        setAction({ ...action, bulkActionOption: e.value });
    };

    // Function to change the selected property of an item
    const onSelectChange = (e, id) => {
        const newData = data;
        const index = newData.findIndex((item) => item._id === id);
        newData[index].checked = e.currentTarget.checked;
        setAction({ ...action, actionCheckboxes: newData[index]._id });
    };

    // Function which selects all the items
    const onSelectAllChange = (e) => {
        data.map((item) => {
            item.checked = e.currentTarget.checked;
            return item;
        });

        setData([...data]);
        setAction({ ...action, actionCheckboxes: data.map((item) => item._id) });
    };

    // function which fires on applying selected action
    const onBulkActionButtonClick = async (e) => {
        // Get all the checked data's id and store it in an array
        let checkedData = data.filter((item) => item.checked === true);
        checkedData = checkedData.map((item) => item._id);

        try {
            setTableLoading(true);
            await axios.post(`${APP_URL}/sub-categories/bulk-action`, {
                action: action.bulkActionOption,
                subCategoryIds: checkedData
            });

            // Fetching data from api
            getData();
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        } finally {
            setTableLoading(false);
        }

        resetBulkActionAndCheckedRows();
    };

    // Function to reset the selected data and action
    const resetBulkActionAndCheckedRows = () => {
        // Uncheck checkbox with an id of uid
        const checkbox = document.getElementById('uid');
        checkbox.checked = false;
        setAction({ bulkActionOption: '', actionCheckboxes: [] });
    };

    // Function to toggle the search option
    const toggleSearchFilter = () => setonSearch(!onSearch);

    // Function to get filtered user
    const fetchData = async (params) => {
        setTableLoading(true);
        const response = await axios.get(`${APP_URL}/sub-categories`, { params });
        return response.data ? response.data : { subCategories: [], total: 0 };
    };

    // Function to search user
    const searchData = async (onSearchText) => {
        try {
            const params = { search: onSearchText, ...pagination, ...appliedFilters };
            const data = await fetchData(params);
            setData(data.subCategories);
            setTotal(data.total);
            setPagination(prevState => ({ ...prevState, currentPage: 1 }));
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        } finally {
            setTableLoading(false);
        }
    };

    // Changing state value when searching name
    useEffect(() => {
        onSearchText !== '' ? searchData(onSearchText) : getData();
    }, [onSearchText, setData]);

    // onChange function for date range filter
    const onDateRangeFilterChange = (dates) => {
        const [start, end] = dates;
        setAppliedFilter((prevState) => ({ ...prevState, start, end }));
    };

    // onChange function for status filter
    const onStatusFilterChange = (status) => {
        setAppliedFilter((prevState) => ({ ...prevState, status }));
    };

    // Function to apply filters like date range and status
    const applyFilters = async () => {
        const params = { ...pagination, ...appliedFilters, currentPage: 1 };
        const data = await fetchData(params);
        setData(data.subCategories);
        setPagination(prevState => ({ ...prevState, currentPage: 1 }));
        setTotal(data.total);
        setTableLoading(false);
    };

    // Function to apply date range filter
    const filterDateRange = async (startDate, endDate) => {
        await applyFilters({ startDate, endDate });
    };

    // Function to apply status filter
    const filterStatus = async (status) => {
        await applyFilters({ status });
    };

    useEffect(() => {
        if (appliedFilters.start !== null && appliedFilters.end !== null) {
            filterDateRange(appliedFilters.start, appliedFilters.end);
        }
        if (appliedFilters.status !== '') {
            filterStatus(appliedFilters.status);
        }
    }, [appliedFilters]);

    // Function to sort user
    const onSort = async (params) => {
        const data = await fetchData({ ...pagination, sort: params, ...appliedFilters });
        setData(data.subCategories);
        setPagination(prevState => ({ ...prevState, sort: params }));
        setTableLoading(false);
    };

    // Function to paginate user
    const paginate = async (pageNumber) => {
        const data = await fetchData({ ...pagination, currentPage: pageNumber, ...appliedFilters, search: onSearchText });
        setData(data.subCategories);
        setPagination(prevState => ({ ...prevState, currentPage: pageNumber }));
        setTotal(data.total);
        setTableLoading(false);
    };

    // Function to set limit per page
    const setPageLimit = async (itemPerPage) => {
        const data = await fetchData({ itemPerPage, sort: pagination.sort, ...appliedFilters });
        setData(data.subCategories);
        setPagination(prevState => ({ ...prevState, itemPerPage, currentPage: 1 }));
        setTableLoading(false);
    };

    const resetFilter = async () => {
        // Reset the date range and status filter
        setAppliedFilter((prevState) => ({
            ...prevState,
            status: '',
            start: null,
            end: null
        }));

        getData();
    };

    // Function to delete a user
    const onDeleteClick = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const result = await axios.delete(
                        `${APP_URL}/sub-categories/delete/${id}`
                    );
                    showToast('success', result.data.message);
                    getData();
                } catch (error) {
                    showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
                }
            }
        });
    };

    return (
        <DataTable className="card-stretch">
            {/* Table Header */}
            <TableFilter
                placeholder={placeholder}
                data={data}
                getData={getData}
                originalData={originalData}
                setData={setData}
                itemPerPage={pagination.itemPerPage}
                setPagination={setPagination}
                bulkAction={action}
                onBulkActionDropdownChange={onBulkActionDropdownChange}
                onBulkActionButtonClick={onBulkActionButtonClick}
                tablesm={tablesm}
                updateTableSm={updateTableSm}
                sort={pagination}
                setPageLimit={setPageLimit}
                onDateRangeFilterChange={onDateRangeFilterChange}
                resetFilter={resetFilter}
                onStatusFilterChange={onStatusFilterChange}
                appliedFilters={appliedFilters}
                onSort={onSort}
                onSearchText={onSearchText}
                setSearchText={setSearchText}
                toggleSearchFilter={toggleSearchFilter}
                onSearch={onSearch}
            />
            {/* Table Body */}
            <DataTableBody compact>
                <DataTableHead>
                    <TableHeader
                        headers={headers}
                        onSelectAllChange={onSelectAllChange}
                    />
                </DataTableHead>
                {/* Head */}
                {tableLoading
                    ? <TableLoader />
                    : total > 0
                        ? data.map((item) => {
                            return (
                                <DataTableItem key={item._id}>
                                    <Checkbox item={item} onSelectChange={onSelectChange} />
                                    <DataTableRow>
                                        <Link to={`${process.env.PUBLIC_URL}/account/sub-categories/edit/${item._id}`}>
                                            <div className="user-card">
                                                <div className="user-info">
                                                    <span className="tb-lead">{capitalize(item.name)}</span>
                                                </div>
                                            </div>
                                        </Link>
                                    </DataTableRow>
                                    <DataTableRow>
                                        <Link to={`${process.env.PUBLIC_URL}/account/sub-categories/edit/${item.category._id}`}>
                                            <div className="user-card">
                                                <div className="user-info">
                                                    <span className="tb-lead">{item.category.name}</span>
                                                </div>
                                            </div>
                                        </Link>
                                    </DataTableRow>
                                    <DataTableRow>
                                        <span className={`tb-status text-${(item.deletedAt === undefined || item.deletedAt === null || item.deletedAt === '') ? (item.status === 'active' ? 'success' : 'danger') : 'danger'
                                        }`}>
                                            {capitalize(item.deletedAt ? 'Deleted' : item.status)}
                                        </span>
                                    </DataTableRow>
                                    <DataTableRow size="lg">
                                        <span>{formatDate(item.createdAt)}</span>
                                    </DataTableRow>
                                    <DataTableRow size="lg">
                                        <span>{item.updatedAt ? formatDate(item.updatedAt) : '--'}</span>
                                    </DataTableRow>
                                    <DataTableRow className="nk-tb-col-tools">
                                        <ul className="nk-tb-actions gx-1">
                                            <li>
                                                <UncontrolledDropdown>
                                                    <DropdownToggle tag="a" className="dropdown-toggle btn btn-icon btn-trigger">
                                                        <Icon name="more-h" />
                                                    </DropdownToggle>
                                                    <DropdownMenu end>
                                                        <ul className="link-list-opt no-bdr">
                                                            <li>
                                                                <DropdownItem
                                                                    tag="a"
                                                                    href="#edit"
                                                                    onClick={(ev) => {
                                                                        ev.preventDefault();
                                                                        history.push(`${process.env.PUBLIC_URL}/account/sub-categories/edit/${item._id}`);
                                                                    }}
                                                                >
                                                                    <Icon name="edit" />
                                                                    <span>Edit</span>
                                                                </DropdownItem>
                                                                <DropdownItem
                                                                    tag="a"
                                                                    href="#delete"
                                                                    onClick={(ev) => {
                                                                        ev.preventDefault();
                                                                        onDeleteClick(item._id);
                                                                    }}
                                                                >
                                                                    <Icon name="trash" />
                                                                    <span>Delete</span>
                                                                </DropdownItem>
                                                            </li>
                                                        </ul>
                                                    </DropdownMenu>
                                                </UncontrolledDropdown>
                                            </li>
                                        </ul>
                                    </DataTableRow>
                                </DataTableItem>
                            );
                        })
                        : null}
            </DataTableBody>

            {/* Pagination */}
            <Pagination
                total={total}
                currentPage={pagination.currentPage}
                itemPerPage={pagination.itemPerPage}
                paginate={paginate}
            />
        </DataTable>
    );
};

SubCategoryDatatable.propTypes = {
    data: PropTypes.array.isRequired,
    setData: PropTypes.func.isRequired,
    getData: PropTypes.func.isRequired,
    originalData: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
    setTotal: PropTypes.func.isRequired,
    pagination: PropTypes.object.isRequired,
    setPagination: PropTypes.func.isRequired
};

export default SubCategoryDatatable;
