import React, { useEffect, useState } from 'react';
import TableFilter from '../filter/TableFilter';
import Pagination from '../Pagination';
import { APP_URL, capitalize } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import { DataTable, DataTableBody, DataTableHead, DataTableItem, DataTableRow } from '../../table/DataTable';
import axios from 'axios';
import PropTypes from 'prop-types';
import TableHeader from '../TableHeader';
import TableLoader from '../../page-loader/TableLoader';

const ShippingAddressDatatable = ({ data, setData, getData, originalData, total, setTotal, pagination, setPagination }) => {
    const [onSearch, setonSearch] = useState(true);
    const [onSearchText, setSearchText] = useState('');
    const [tablesm, updateTableSm] = useState(false);
    const [tableLoading, setTableLoading] = useState(false);
    const headers = ['User Name', 'Phone Number', 'Address', 'City', 'State', 'Country', 'Pin Code', 'Status'];
    const placeholder = 'Search by user name, address, city, state, country, pin code or status';

    // State for filters
    const [appliedFilters, setAppliedFilter] = useState({
        start: null,
        end: null,
        status: ''
    });

    // Function to toggle the search option
    const toggleSearchFilter = () => setonSearch(!onSearch);

    // Function to get filtered shipping addresses
    const fetchData = async (params) => {
        setTableLoading(true);
        const response = await axios.get(`${APP_URL}/shipping-addresses`, { params });
        return response.data ? response.data : { shippingAddresses: [], total: 0 };
    };

    // Function to search shipping addresses
    const searchData = async (onSearchText) => {
        try {
            const params = { search: onSearchText, ...pagination, ...appliedFilters };
            const data = await fetchData(params);
            setData(data.shippingAddresses);
            setTotal(data.total);
            setPagination(prevState => ({ ...prevState, currentPage: 1 }));
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        } finally {
            setTableLoading(false);
        }
    };

    // Changing state value when searching name
    useEffect(() => {
        onSearchText !== '' ? searchData(onSearchText) : getData();
    }, [onSearchText, setData]);

    // onChange function for date range filter
    const onDateRangeFilterChange = (dates) => {
        const [start, end] = dates;
        setAppliedFilter((prevState) => ({ ...prevState, start, end }));
    };

    // onChange function for status filter
    const onStatusFilterChange = (status) => {
        setAppliedFilter((prevState) => ({ ...prevState, status }));
    };

    // Function to apply filters like date range and status
    const applyFilters = async () => {
        const params = { ...pagination, ...appliedFilters, currentPage: 1 };
        const data = await fetchData(params);
        setData(data.shippingAddresses);
        setPagination(prevState => ({ ...prevState, currentPage: 1 }));
        setTotal(data.total);
        setTableLoading(false);
    };

    // Function to apply date range filter
    const filterDateRange = async (startDate, endDate) => {
        await applyFilters({ startDate, endDate });
    };

    // Function to apply status filter
    const filterStatus = async (status) => {
        await applyFilters({ status });
    };

    useEffect(() => {
        if (appliedFilters.start !== null && appliedFilters.end !== null) {
            filterDateRange(appliedFilters.start, appliedFilters.end);
        }
        if (appliedFilters.status !== '') {
            filterStatus(appliedFilters.status);
        }
    }, [appliedFilters]);

    // Function to sort shipping addresses
    const onSort = async (params) => {
        const data = await fetchData({ ...pagination, sort: params, ...appliedFilters });
        setData(data.shippingAddresses);
        setPagination(prevState => ({ ...prevState, sort: params }));
        setTableLoading(false);
    };

    // Function to paginate shipping addresses
    const paginate = async (pageNumber) => {
        const data = await fetchData({ ...pagination, currentPage: pageNumber, ...appliedFilters, search: onSearchText });
        setData(data.shippingAddresses);
        setPagination(prevState => ({ ...prevState, currentPage: pageNumber }));
        setTotal(data.total);
        setTableLoading(false);
    };

    // Function to set limit per page
    const setPageLimit = async (itemPerPage) => {
        const data = await fetchData({ itemPerPage, sort: pagination.sort, ...appliedFilters });
        setData(data.shippingAddresses);
        setPagination(prevState => ({ ...prevState, itemPerPage, currentPage: 1 }));
        setTableLoading(false);
    };

    const resetFilter = async () => {
        // Reset the date range and status filter
        setAppliedFilter((prevState) => ({
            ...prevState,
            status: '',
            start: null,
            end: null
        }));

        getData();
    };

    return (
        <DataTable className="card-stretch">
            {/* Table Header */}
            <TableFilter
                placeholder={placeholder}
                table="Payment"
                data={data}
                getData={getData}
                originalData={originalData}
                setData={setData}
                itemPerPage={pagination.itemPerPage}
                setPagination={setPagination}
                tablesm={tablesm}
                updateTableSm={updateTableSm}
                sort={pagination}
                setPageLimit={setPageLimit}
                onDateRangeFilterChange={onDateRangeFilterChange}
                resetFilter={resetFilter}
                onStatusFilterChange={onStatusFilterChange}
                appliedFilters={appliedFilters}
                onSort={onSort}
                onSearchText={onSearchText}
                setSearchText={setSearchText}
                toggleSearchFilter={toggleSearchFilter}
                onSearch={onSearch}
            />
            {/* Table Body */}
            <DataTableBody compact>
                <DataTableHead>
                    <TableHeader
                        headers={headers}
                        table="Payment"
                    />
                </DataTableHead>
                {/* Head */}
                {tableLoading
                    ? <TableLoader />
                    : total > 0
                        ? data.map((item) => {
                            return (
                                <DataTableItem key={item._id}>
                                    <DataTableRow>
                                        <span>{item.user.name}</span>
                                    </DataTableRow>
                                    <DataTableRow size="lg">
                                        <span className="tb-odr-date">{item.phone}</span>
                                    </DataTableRow>
                                    <DataTableRow>
                                        <span>{item.address}</span>
                                    </DataTableRow>
                                    <DataTableRow>
                                        <span>{item.city}</span>
                                    </DataTableRow>
                                    <DataTableRow>
                                        <span>{item.state}</span>
                                    </DataTableRow>
                                    <DataTableRow size="lg">
                                        <span>{item.country}</span>
                                    </DataTableRow>
                                    <DataTableRow size="lg">
                                        <span>{item.pincode}</span>
                                    </DataTableRow>
                                    <DataTableRow size="lg">
                                        <span className={`tb-status text-${item.status === 'active' ? 'success' : 'danger'}`}>
                                            <span>{capitalize(item.status)}</span>
                                        </span>
                                    </DataTableRow>
                                </DataTableItem>
                            );
                        })
                        : null}
            </DataTableBody>
            {/* Pagination */}
            <Pagination
                total={total}
                currentPage={pagination.currentPage}
                itemPerPage={pagination.itemPerPage}
                paginate={paginate}
            />
        </DataTable>
    );
};

ShippingAddressDatatable.propTypes = {
    data: PropTypes.array,
    setData: PropTypes.func,
    getData: PropTypes.func,
    originalData: PropTypes.array,
    total: PropTypes.number,
    setTotal: PropTypes.func,
    pagination: PropTypes.object,
    setPagination: PropTypes.func
};

export default ShippingAddressDatatable;
