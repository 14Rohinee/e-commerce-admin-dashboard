import React from 'react';
import PropTypes from 'prop-types';
import { DropdownItem } from 'reactstrap';

const SortDropdown = ({ sort, setSortState, onSort }) => {
    return (
        <ul className="link-check">
            <li><span>Order</span></li>
            {['dsc', 'asc'].map((order) => (
                <li key={order} className={sort.sort === order ? 'active' : ''}>
                    <DropdownItem tag="a" href="#dropdownitem" onClick={(ev) => { ev.preventDefault(); setSortState({ ...sort, sort: order }); onSort(order); }}>{order.toUpperCase()}</DropdownItem>
                </li>
            ))}
        </ul>
    );
};

SortDropdown.propTypes = {
    sort: PropTypes.object.isRequired,
    setSortState: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired
};

export default SortDropdown;
