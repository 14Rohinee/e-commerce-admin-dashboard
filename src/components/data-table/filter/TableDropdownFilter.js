import React from 'react';
import { Button, Col, Icon, RSelect, Row } from '../../Component';
import {
    DropdownMenu,
    DropdownToggle,
    UncontrolledDropdown
} from 'reactstrap';
import PropTypes from 'prop-types';
import ItemPerPageDropdown from './ItemPerPageDropdown';
import SortDropdown from './SortDropdown';
import DatePicker from 'react-datepicker';
import { capitalize, statuses } from '../../../utils/Constants';

const TableDropdownFilter = ({ table, tablesm, updateTableSm, appliedFilters, onDateRangeFilterChange, onStatusFilterChange, resetFilter, sort, setPagination, onSort, filterStatuses, setPageLimit }) => {
    const options = filterStatuses || statuses;

    return (
        <div className="toggle-wrap">
            <Button className={`btn-icon btn-trigger toggle ${tablesm ? 'active' : ''}`}
                onClick={() => updateTableSm(true)}
            >
                <Icon name="menu-right"></Icon>
            </Button>
            <div className={`toggle-content ${tablesm ? 'content-active' : ''}`}>
                <ul className="btn-toolbar gx-1">
                    <li className="toggle-close">
                        <Button className="btn-icon btn-trigger toggle"
                            onClick={() => updateTableSm(false)}
                        >
                            <Icon name="arrow-left"></Icon>
                        </Button>
                    </li>
                    <li>
                        <UncontrolledDropdown>
                            <DropdownToggle tag="a" className="btn btn-trigger btn-icon dropdown-toggle">
                                {(appliedFilters.start !== null || appliedFilters.end !== null || appliedFilters.status !== '') ? <div className="dot dot-primary"></div> : ''}
                                <Icon name="filter-alt"></Icon>
                            </DropdownToggle>
                            <DropdownMenu end className="filter-wg dropdown-menu-xl" style={{ overflow: 'visible' }}>
                                <div className="dropdown-head">
                                    <span className="sub-title dropdown-title">Filter {table}</span>
                                </div>
                                <div className="dropdown-body dropdown-body-rg">
                                    <Row className="gx-6 gy-3">
                                        <Col size="12">
                                            <div className="form-group">
                                                <label className="overline-title overline-title-alt">{table} Created Between</label>
                                                <div className="form-control-wrap">
                                                    <DatePicker
                                                        selected={appliedFilters.start}
                                                        startDate={appliedFilters.start}
                                                        onChange={onDateRangeFilterChange}
                                                        endDate={appliedFilters.end}
                                                        selectsRange
                                                        className="form-control date-picker"
                                                    />
                                                </div>
                                                <div className="form-note">Date Format <code>mm/dd/yyyy</code></div>
                                            </div>
                                        </Col>
                                        <Col size="12">
                                            <div className="form-group">
                                                <label className="overline-title overline-title-alt">Status</label>
                                                <RSelect
                                                    options={options}
                                                    placeholder="Select"
                                                    onChange={(selectedOption) => {
                                                        onStatusFilterChange(selectedOption.value);
                                                    }}
                                                    value={appliedFilters.status ? { value: appliedFilters.status, label: capitalize(appliedFilters.status) } : null}
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                                <div className="dropdown-foot between">
                                    <a href="#reset" onClick={(ev) => { ev.preventDefault(); resetFilter(); }} className="clickable">Reset Filter</a>
                                </div>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </li>
                    <li>
                        <UncontrolledDropdown>
                            <DropdownToggle tag="a" className="btn btn-trigger btn-icon dropdown-toggle">
                                {((sort.itemPerPage && sort.itemPerPage !== 10) || (sort.sort && sort.sort !== 'asc')) ? <div className="dot dot-primary"></div> : ''}
                                <Icon name="setting"></Icon>
                            </DropdownToggle>
                            <DropdownMenu end className="dropdown-menu-xs">
                                <ItemPerPageDropdown sort={sort} setPageLimit={setPageLimit} />
                                <SortDropdown sort={sort} setSortState={setPagination} onSort={onSort} />
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </li>
                </ul>
            </div>
        </div>
    );
};

TableDropdownFilter.propTypes = {
    table: PropTypes.string,
    tablesm: PropTypes.bool.isRequired,
    updateTableSm: PropTypes.func.isRequired,
    appliedFilters: PropTypes.object.isRequired,
    onDateRangeFilterChange: PropTypes.func.isRequired,
    onStatusFilterChange: PropTypes.func.isRequired,
    resetFilter: PropTypes.func.isRequired,
    sort: PropTypes.object.isRequired,
    setPagination: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired,
    filterStatuses: PropTypes.array,
    setPageLimit: PropTypes.func
};

export default TableDropdownFilter;
