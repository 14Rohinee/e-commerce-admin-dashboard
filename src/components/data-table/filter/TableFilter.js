import React from 'react';
import { Search, OnSearch } from '../Search';
import BulkAction from '../BulkAction';
import PropTypes from 'prop-types';
import TableDropdownFilter from './TableDropdownFilter';
import { bulkActionOptions } from '../../../utils/Constants';

const TableFilter = ({ placeholder, table, data, originalData, setData, setPagination, bulkAction, onBulkActionDropdownChange, onBulkActionButtonClick, toggleSearchFilter, tablesm, updateTableSm, sort, setPageLimit, onDateRangeFilterChange, resetFilter, onStatusFilterChange, onSort, appliedFilters, onSearch, setSearchText, onSearchText, filterStatuses }) => {
    const options = filterStatuses || bulkActionOptions;

    return (
        <div className="card-inner position-relative card-tools-toggle">
            <div className="card-title-group">
                <div className="card-tools">
                    {table !== 'Payment' && (<BulkAction
                        bulkAction={bulkAction}
                        options={options}
                        onBulkActionDropdownChange={onBulkActionDropdownChange}
                        onBulkActionButtonClick={onBulkActionButtonClick}
                    />)}
                </div>
                <div className="card-tools me-n1">
                    <ul className="btn-toolbar gx-1">
                        <li>
                            <Search toggleSearchFilter={toggleSearchFilter} />
                        </li>
                        <li className="btn-toolbar-sep"></li>
                        <li>
                            <TableDropdownFilter
                                table={table}
                                tablesm={tablesm}
                                originalData={originalData}
                                data={data}
                                updateTableSm={updateTableSm}
                                sort={sort}
                                setData={setData}
                                setPagination={setPagination}
                                onDateRangeFilterChange={onDateRangeFilterChange}
                                resetFilter={resetFilter}
                                onStatusFilterChange={onStatusFilterChange}
                                onSort={onSort}
                                appliedFilters={appliedFilters}
                                filterStatuses={filterStatuses}
                                setPageLimit={setPageLimit}
                            />
                        </li>
                    </ul>
                </div>
            </div>
            <div className={`card-search search-wrap ${!onSearch && 'active'}`}>
                <OnSearch
                    placeholder={placeholder}
                    setSearchText={setSearchText}
                    toggleSearchFilter={toggleSearchFilter}
                    onSearchText={onSearchText}
                />
            </div>
        </div>
    );
};

TableFilter.propTypes = {
    placeholder: PropTypes.string.isRequired,
    table: PropTypes.string,
    data: PropTypes.array.isRequired,
    originalData: PropTypes.array.isRequired,
    setData: PropTypes.func.isRequired,
    setPagination: PropTypes.func.isRequired,
    bulkAction: PropTypes.object,
    onBulkActionDropdownChange: PropTypes.func,
    onBulkActionButtonClick: PropTypes.func,
    toggleSearchFilter: PropTypes.func.isRequired,
    tablesm: PropTypes.bool.isRequired,
    updateTableSm: PropTypes.func.isRequired,
    sort: PropTypes.object.isRequired,
    setPageLimit: PropTypes.func.isRequired,
    onDateRangeFilterChange: PropTypes.func.isRequired,
    resetFilter: PropTypes.func.isRequired,
    onStatusFilterChange: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired,
    appliedFilters: PropTypes.object.isRequired,
    onSearch: PropTypes.bool.isRequired,
    setSearchText: PropTypes.func.isRequired,
    onSearchText: PropTypes.string.isRequired,
    filterStatuses: PropTypes.array
};

export default TableFilter;
