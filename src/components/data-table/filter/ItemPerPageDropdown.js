import React from 'react';
import PropTypes from 'prop-types';
import { DropdownItem } from 'reactstrap';

const ItemPerPageDropdown = ({ sort, setPageLimit }) => {
    return (
        <ul className="link-check">
            <li><span>Show</span></li>
            {[10, 20, 50].map((perPage) => (
                <li key={perPage} className={sort.itemPerPage === perPage ? 'active' : ''}>
                    <DropdownItem tag="a" href="#dropdownitem" onClick={() => setPageLimit(perPage)}>{perPage}</DropdownItem>
                </li>
            ))}
        </ul>

    );
};

ItemPerPageDropdown.propTypes = {
    sort: PropTypes.object.isRequired,
    setPageLimit: PropTypes.func
};

export default ItemPerPageDropdown;
