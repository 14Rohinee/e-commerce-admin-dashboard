import React from 'react';
import PropTypes from 'prop-types';
import { PaginationComponent } from '../Component';

const Pagination = ({ total, currentPage, itemPerPage, paginate }) => {
    return (
        <div className="card-inner">
            {total > 0
                ? (
                    <PaginationComponent
                        itemPerPage={itemPerPage}
                        totalItems={total}
                        paginate={paginate}
                        currentPage={currentPage}
                    />
                )
                : (
                    <div className="text-center">
                        <span className="text-silent">
                            No data found
                        </span>
                    </div>
                )}
        </div>
    );
};

Pagination.propTypes = {
    total: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    itemPerPage: PropTypes.number.isRequired,
    paginate: PropTypes.func.isRequired
};

export default Pagination;
