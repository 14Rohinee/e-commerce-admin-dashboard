import React from 'react';
import PropTypes from 'prop-types';
import { DataTableRow } from '../Component';

const Checkbox = ({ item, onSelectChange }) => {
    return (
        <DataTableRow className="nk-tb-col-check">
            <div className="custom-control custom-control-sm custom-checkbox notext">
                <input
                    type="checkbox"
                    className="custom-control-input"
                    defaultChecked={
                        item.checked
                    }
                    id={item._id + 'uid1'}
                    key={Math.random()}
                    onChange={(e) => onSelectChange(e, item._id)}
                />
                <label className="custom-control-label" htmlFor={item._id + 'uid1'}
                ></label>
            </div>
        </DataTableRow>
    );
};

Checkbox.propTypes = {
    item: PropTypes.object.isRequired,
    onSelectChange: PropTypes.func.isRequired
};

export default Checkbox;
