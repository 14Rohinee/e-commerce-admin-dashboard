import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

const RSelect = ({ ...props }) => {
    return (
        <div className="form-control-select">
            <Select
                isClearable={true}
                className={`react-select-container ${props.className ? props.className : ''}`}
                classNamePrefix="react-select"
                {...props}
                value={props.value}
            />
        </div>
    );
};

RSelect.propTypes = {
    className: PropTypes.string,
    value: PropTypes.object
};

export default RSelect;
