import React from 'react';
import PropTypes from 'prop-types';

const UserGroup = ({ className, ...props }) => {
    return <div className={`user-avatar-group ${className || ''}`}>{props.children}</div>;
};

UserGroup.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

export default UserGroup;
