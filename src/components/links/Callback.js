import React from 'react';
import PropTypes from 'prop-types';
import { Label } from 'reactstrap';
import { APP_URL } from '../../utils/Constants';
import { Icon } from '../Component';
import { showToast } from '../../utils/Utils';

const Callback = ({ fieldLabel, fieldId, fieldName }) => {
    return (
        <div className="form-group">
            <Label
                className="form-label"
                htmlFor={fieldId}
            >
                {fieldLabel}
            </Label>
            <div className="form-control-wrap">
                <span className="form-span">
                    {APP_URL}/callback/{fieldName}
                </span>{' '}
                <a
                    href="#export"
                    onClick={(ev) => {
                        ev.preventDefault();
                        navigator.clipboard.writeText(`${APP_URL}/callback/${fieldName}`);
                        showToast('success', 'Copied to clipboard');
                    }}
                    className="btn btn-white btn-outline-light ml-2"
                >
                    <Icon name="copy"></Icon>
                    <span>Copy</span>
                </a>
            </div>
        </div>
    );
};

Callback.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldURL: PropTypes.string
};

export default Callback;
