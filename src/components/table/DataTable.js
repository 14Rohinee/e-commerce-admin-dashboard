import classNames from 'classnames';
import React from 'react';
import { Card } from 'reactstrap';
import PropTypes from 'prop-types';

const DataTable = ({ className, bodyClassName, title, ...props }) => {
    return (
        <Card className={`card-bordered ${className || ''}`}>
            <div className="card-inner-group">{props.children}</div>
        </Card>
    );
};

DataTable.propTypes = {
    className: PropTypes.string,
    bodyClassName: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.node
};

const DataTableTitle = ({ ...props }) => {
    return (
        <div className="card-inner position-relative card-tools-toggle">
            <div className="card-title-group">{props.children}</div>
        </div>
    );
};

DataTableTitle.propTypes = {
    children: PropTypes.node
};

const DataTableBody = ({ compact, className, bodyclass, ...props }) => {
    return (
        <div className={`card-inner p-0 table-responsive ${className || ''}`}>
            <div className={`nk-tb-list nk-tb-ulist ${bodyclass || ''} ${compact ? 'is-compact' : ''}`}>
                {props.children}
            </div>
        </div>
    );
};

DataTableBody.propTypes = {
    compact: PropTypes.bool,
    className: PropTypes.string,
    bodyclass: PropTypes.string,
    children: PropTypes.node
};

const DataTableHead = ({ ...props }) => {
    return <div className="nk-tb-item nk-tb-head">{props.children}</div>;
};

DataTableHead.propTypes = {
    children: PropTypes.node
};

const DataTableRow = ({ className, size, ...props }) => {
    const rowClass = classNames({
        'nk-tb-col': true,
        [`${className}`]: className,
        [`tb-col-${size}`]: size
    });
    return <div className={rowClass}>{props.children}</div>;
};

DataTableRow.propTypes = {
    className: PropTypes.string,
    size: PropTypes.string,
    children: PropTypes.node
};

const DataTableItem = ({ className, ...props }) => {
    return <div className={`nk-tb-item ${className || ''}`}>{props.children}</div>;
};

DataTableItem.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

export { DataTable, DataTableTitle, DataTableBody, DataTableHead, DataTableRow, DataTableItem };
