import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ name, id, className, style, ...props }) => {
    const iconClass = classNames({
        [`${className}`]: className,
        icon: true,
        ni: true,
        [`ni-${name}`]: true
    });
    return <em className={iconClass} id={id} style={style} {...props}></em>;
};

Icon.propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object
};

export default Icon;
