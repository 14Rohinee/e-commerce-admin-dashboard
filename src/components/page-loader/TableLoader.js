import React from 'react';

const TableLoader = (props) => {
    return (
        <div className="overlay nk-tb-list nk-tb-ulist  is-compact align-items-center">
            <div className="d-flex align-items-center">
                <strong className='position text-dark align-items-center' style={{ position: 'absolute', top: '80%', left: '48%' }}>Loading...</strong>
            </div>
        </div>
    );
};

export default TableLoader;
