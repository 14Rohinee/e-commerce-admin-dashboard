import React from 'react';
import PropTypes from 'prop-types';

const PageLoader = ({ text, theme }) => {
    return (
        <div className="overlay" style={{ position: 'fixed', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(0, 0, 0, 0.5)', zIndex: 9999 }}>
            <div className="d-flex align-items-center">
                <strong className='position text-light' style={{ position: 'absolute', top: '55%', left: '48%' }}>{text}</strong>
                <div className="spinner-border text-light" role="status" style={{ position: 'absolute', top: '50%', left: '50%' }}>
                </div>
            </div>
        </div>
    );
};

PageLoader.propTypes = {
    text: PropTypes.string,
    theme: PropTypes.string
};

export default PageLoader;
