import React from 'react';
import Icon from '../icon/Icon';
import { UncontrolledTooltip } from 'reactstrap';
import PropTypes from 'prop-types';

const TooltipComponent = ({ iconClass, icon, id, direction, text, containerClassName, ...props }) => {
    return (
        <>
            {props.tag
                ? (
                    <props.tag className={containerClassName} id={id}>
                        {' '}
                        <Icon className={`${iconClass || ''}`} name={icon}></Icon>
                    </props.tag>
                )
                : (
                    <Icon className={`${iconClass || ''}`} name={icon} id={id}></Icon>
                )}
            <UncontrolledTooltip autohide={false} placement={direction} target={id}>
                {text}
            </UncontrolledTooltip>
        </>
    );
};

TooltipComponent.propTypes = {
    iconClass: PropTypes.string,
    icon: PropTypes.string,
    id: PropTypes.string,
    direction: PropTypes.string,
    text: PropTypes.string,
    containerClassName: PropTypes.string,
    tag: PropTypes.string
};

export default TooltipComponent;
