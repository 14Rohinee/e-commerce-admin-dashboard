import React from 'react';
import { Label } from 'reactstrap';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';

function Number ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldValue, fieldReadOnly, fieldDisabled, fieldPlaceHolder, fieldToolTip, fieldToolTipContent, ...props }) {
    return (
        <>
            <div className="form-group">
                <Label
                    className="form-label"
                    htmlFor={fieldId}
                >
                    {fieldLabel}
                </Label>
                {' '}
                {
                    fieldRequired && (
                        <sup className="text-danger">*</sup>
                    )
                }
                {' '}
                {
                    fieldToolTip && (
                        <>
                            <TooltipComponent
                                icon="help-fill"
                                iconClass="card-hint text-secondary"
                                direction="top"
                                id="tooltip-1"
                                text={fieldToolTipContent}
                            />
                        </>
                    )
                }

                <div className="form-control-wrap">
                    <input
                        ref={
                            props.register &&
                            props.register({
                                required: !!fieldRequired
                            })
                        }
                        type="number"
                        id={fieldId}
                        name={fieldName}
                        className={`form-control  ${props.errors[fieldName] ? 'is-invalid' : ''}`}
                        autoComplete="new-password"
                        defaultValue={fieldValue}
                        placeholder={fieldPlaceHolder}
                        readOnly={fieldReadOnly}
                        disabled={fieldDisabled}
                        onChange={props.onChange}
                        min={props.min}
                        max={props.max}
                    />
                    {props.errors[fieldName] && props.errors[fieldName].type === 'required' && (
                        <span className="invalid">
                            {fieldLabel} is required
                        </span>
                    )}
                </div>
            </div>
        </>
    );
}

Number.propTypes = {
    fieldLabel: PropTypes.string.isRequired,
    fieldName: PropTypes.string.isRequired,
    fieldId: PropTypes.string.isRequired,
    fieldPlaceHolder: PropTypes.string.isRequired,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    register: PropTypes.func,
    errors: PropTypes.object,
    onChange: PropTypes.func,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number
};

export default Number;
