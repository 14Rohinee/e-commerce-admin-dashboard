import React from 'react';
import { Label } from 'reactstrap';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';

function Radio ({ fieldLabel, fieldName, fieldRequired, fieldValue, fieldToolTip, fieldToolTipContent, fieldOptions, ...props }) {
    if (fieldValue === undefined) {
        return null;
    }

    return (
        <>
            <div className="form-group">
                <Label
                    className="form-label"
                    htmlFor="fv-gender"
                >
                    {fieldLabel}
                    {' '}
                    {
                        fieldRequired && (
                            <sup className="text-danger">*</sup>
                        )
                    }
                </Label>
                {
                    fieldToolTip && (
                        <>
                            {' '}
                            <TooltipComponent
                                icon="help-fill"
                                iconClass="card-hint text-secondary"
                                direction="top"
                                id="tooltip-1"
                                text={fieldToolTipContent}
                            />
                        </>
                    )
                }
                <ul className="custom-control-group g-3 align-center flex-wrap">
                    {
                        fieldOptions.map((option, index) => (
                            <li key={index}>
                                <div className="custom-control custom-radio">
                                    <input
                                        ref={
                                            props.register &&
                                            props.register({
                                                required: !!fieldRequired
                                            })
                                        }
                                        type="radio"
                                        id={'radio' + option.value}
                                        name={fieldName}
                                        className={`custom-control-input ${props.errors[fieldName] ? 'is-invalid' : ''}`}
                                        autoComplete="new-password"
                                        defaultChecked={fieldValue === option.value}
                                        value={option.value}
                                        placeholder={option.label}
                                        {...props.register(fieldName)}
                                        onChange={(e) => {
                                            props.setValue(fieldName, e.target.value);
                                        }}
                                    />
                                    <label
                                        className="custom-control-label"
                                        htmlFor={'radio' + option.value}
                                    >
                                        {option.label}
                                    </label>
                                </div>
                            </li>
                        ))
                    }
                </ul>
            </div>
        </>
    );
}

Radio.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    fieldOptions: PropTypes.array,
    register: PropTypes.func,
    errors: PropTypes.object,
    setValue: PropTypes.func
};

export default Radio;
