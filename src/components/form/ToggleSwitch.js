import React from 'react';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';

function ToggleSwitch ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldDisabled, fieldToolTip, fieldToolTipContent, fieldChecked, ...props }) {
    return (
        <div className="form-group">
            <div className="preview-block">
                <span className="preview-title form-label">
                    {fieldLabel}
                    {' '}
                    {
                        fieldRequired && (
                            <sup className="text-danger">*</sup>
                        )
                    }
                    {
                        fieldToolTip && (
                            <>
                                {' '}
                                <TooltipComponent
                                    icon="help-fill"
                                    iconClass="card-hint text-secondary"
                                    direction="top"
                                    id="tooltip-1"
                                    text={fieldToolTipContent}
                                />
                            </>
                        )
                    }
                </span>
                <div className="custom-control custom-switch">
                    <input
                        type="checkbox"
                        className="custom-control-input"
                        defaultChecked={fieldChecked}
                        id={fieldId}
                        name={fieldName}
                        {...props.register(fieldName)}
                    />
                    <label className="custom-control-label" htmlFor={fieldId}>
                    </label>
                </div>
            </div>
        </div>
    );
}

ToggleSwitch.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    register: PropTypes.func,
    fieldChecked: PropTypes.bool
};

export default ToggleSwitch;
