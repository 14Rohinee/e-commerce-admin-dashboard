import React, { forwardRef, useState } from 'react';
import { Label } from 'reactstrap';
import PropTypes from 'prop-types';
import { Icon, TooltipComponent } from '../Component';
import DatePicker from 'react-datepicker';

const ExampleCustomInput = forwardRef(({ value, onClick, onChange }, ref) => (
    <div onClick={onClick} ref={ref}>
        <div className="form-icon form-icon-left">
            <Icon name="calendar"></Icon>
        </div>
        <input className="form-control date-picker" type="text" value={value} onChange={onChange} />
    </div>
));

ExampleCustomInput.propTypes = {
    value: PropTypes.string,
    onClick: PropTypes.func,
    onChange: PropTypes.func
};

// Add a displayName property to the functional component
ExampleCustomInput.displayName = 'ExampleCustomInput';

function DateRangePicker ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldValue, fieldReadOnly, fieldDisabled, fieldPlaceHolder, fieldToolTip, fieldToolTipContent, selected, onChange, ...props }) {
    const [rangeDate, setRangeDate] = useState({
        start: new Date(),
        end: null
    });

    const onRangeChange = (dates) => {
        const [start, end] = dates;
        setRangeDate({ start, end });
    };
    return (
        <div className="form-group">
            <Label>
                {fieldLabel}
                {' '}
                {
                    fieldRequired && (
                        <sup className="text-danger">*</sup>
                    )
                }
            </Label>
            {' '}
            {
                fieldToolTip && (
                    <>
                        <TooltipComponent
                            icon="help-fill"
                            iconClass="card-hint text-secondary"
                            direction="top"
                            id="tooltip-1"
                            text={fieldToolTipContent}
                        />
                    </>
                )
            }
            <div className="form-control-wrap">
                <div className="form-icon form-icon-left">
                    <Icon name="calendar"></Icon>
                </div>
                <DatePicker
                    className="form-control date-picker"
                    onChange={onRangeChange}
                    customInput={<ExampleCustomInput />}
                    selected={selected}
                    startDate={rangeDate.start}
                    endDate={rangeDate.end}
                    selectsRange
                />
            </div>
        </div>
    );
}

DateRangePicker.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    register: PropTypes.func,
    errors: PropTypes.object,
    fieldPlaceHolder: PropTypes.string,
    selected: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.oneOf([undefined])
    ]),
    onChange: PropTypes.func
};

export default DateRangePicker;
