import React from 'react';
import PropTypes from 'prop-types';
import { Label } from 'reactstrap';
import { TooltipComponent } from '../Component';

function Checkbox ({ fieldLabel, fieldName, fieldRequired, fieldValue, fieldToolTip, fieldToolTipContent, fieldOptions, ...props }) {
    return (
        <div className="form-group">
            <Label
                className="form-label"
            >
                {fieldLabel}
            </Label>
            {' '}
            {
                fieldRequired && (
                    <sup className="text-danger">*</sup>
                )
            }
            {' '}
            {
                fieldToolTip && (
                    <>
                        <TooltipComponent
                            icon="help-fill"
                            iconClass="card-hint text-secondary"
                            direction="top"
                            id="tooltip-1"
                            text={fieldToolTipContent}
                        />
                    </>
                )
            }
            <div className="g-3 align-center flex-wrap">
                {
                    fieldOptions.map((option, index) => (
                        <div key={index} className="g">
                            <div className="custom-control custom-checkbox">
                                <input
                                    ref={
                                        props.register &&
                                        props.register({
                                            required: !!fieldRequired
                                        })
                                    }
                                    type="checkbox"
                                    id={'checkbox' + option.value}
                                    name={fieldName}
                                    className={`custom-control-input ${props.errors[fieldName] ? 'is-invalid' : ''}`}
                                    autoComplete="new-password"
                                    defaultChecked={fieldValue === option.value}
                                    value={option.value}
                                    placeholder={option.label}
                                    {...props.register(fieldName)}
                                    onChange={(e) => {
                                        props.setValue(fieldName, e.target.value);
                                    }}
                                />
                                <label className="custom-control-label" htmlFor={'checkbox' + option.value}>
                                    {option.label}
                                </label>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );
}

Checkbox.propTypes = {
    register: PropTypes.func,
    errors: PropTypes.object,
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    fieldOptions: PropTypes.array,
    setValue: PropTypes.func
};

export default Checkbox;
