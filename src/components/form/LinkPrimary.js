import React from 'react';
import PropTypes from 'prop-types';
import { Icon, TooltipComponent } from '../Component';

function LinkPrimary ({ fieldLabel, fieldIcon, fieldToolTip, fieldToolTipContent, handleChange }) {
    return (
        <>
            <a href="#">
                <Icon name="plus"></Icon>
                {' '}
                {fieldLabel}
                {
                    fieldToolTip && (
                        <>
                            <TooltipComponent
                                icon={fieldIcon}
                                iconClass="card-hint text-secondary"
                                direction="top"
                                id="tooltip-1"
                                text={fieldToolTipContent}
                            />
                        </>
                    )
                }
            </a>
        </>
    );
}

LinkPrimary.propTypes = {
    fieldLabel: PropTypes.string,
    fieldIcon: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    handleChange: PropTypes.func
};

export default LinkPrimary;
