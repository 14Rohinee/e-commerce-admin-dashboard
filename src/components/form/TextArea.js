import React from 'react';
import { Label } from 'reactstrap';
import QuillComponent from '../partials/rich-editor/QuillComponent';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';

function TextArea ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldValue, fieldReadOnly, fieldDisabled, fieldPlaceHolder, fieldToolTip, fieldToolTipContent, ...props }) {
    return (
        <div className="form-group">
            <Label className="form-label">
                {fieldLabel}
                {' '}
                {
                    fieldRequired && (
                        <sup className="text-danger">*</sup>
                    )
                }

                {
                    fieldToolTip && (
                        <>
                            <TooltipComponent
                                icon="help-fill"
                                iconClass="card-hint text-secondary"
                                direction="top"
                                id="tooltip-1"
                                text={fieldToolTipContent}
                            />
                        </>
                    )
                }
            </Label>
            <div className="form-control-wrap">
                <QuillComponent placeholder={fieldPlaceHolder} onChange={props.onChange} value={fieldValue} />
            </div>
        </div>
    );
}

TextArea.propTypes = {
    fieldLabel: PropTypes.string.isRequired,
    fieldName: PropTypes.string.isRequired,
    fieldId: PropTypes.string.isRequired,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldPlaceHolder: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    onChange: PropTypes.func
};

export default TextArea;
