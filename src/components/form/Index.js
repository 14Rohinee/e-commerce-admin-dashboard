// Import all components of current folder here
import Text from './Text';
import TextArea from './TextArea';
import Select2 from './Select2';
import Tel from './Tel';
import DatePicker from './DatePicker';
import Radio from './Radio';
import Password from './Password';
import Email from './Email';
import ButtonPrimary from './ButtonPrimary';
import ButtonSecondary from './ButtonSecondary';
import Checkbox from './Checkbox';

export { Text, Select2, Tel, DatePicker, Radio, Password, Email, ButtonPrimary, ButtonSecondary, TextArea, Checkbox };
