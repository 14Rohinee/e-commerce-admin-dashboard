import React from 'react';
import Icon from '../icon/Icon';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const Block = ({ className, size, ...props }) => {
    const blockClass = classNames({
        'nk-block': true,
        [`nk-block-${size}`]: size,
        [`${className}`]: className
    });
    return <div className={blockClass}>{props.children}</div>;
};

Block.propTypes = {
    className: PropTypes.string,
    size: PropTypes.string,
    children: PropTypes.node
};

const BlockContent = ({ className, ...props }) => {
    const blockContentClass = classNames({
        'nk-block-content': true,
        [`${className}`]: className
    });
    return <div className={blockContentClass}>{props.children}</div>;
};

BlockContent.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

const BlockBetween = ({ className, ...props }) => {
    return <div className={`nk-block-between ${className || ''}`}>{props.children}</div>;
};

BlockBetween.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

const BlockHead = ({ className, size, wide, ...props }) => {
    const blockHeadClass = classNames({
        'nk-block-head': true,
        [`nk-block-head-${size}`]: size,
        [`wide-${wide}`]: wide,
        [`${className}`]: className
    });
    return <div className={blockHeadClass}>{props.children}</div>;
};

BlockHead.propTypes = {
    className: PropTypes.string,
    size: PropTypes.string,
    wide: PropTypes.bool,
    children: PropTypes.node
};

const BlockHeadContent = ({ className, ...props }) => {
    return <div className={[`nk-block-head-content${className ? ' ' + className : ''}`]}>{props.children}</div>;
};

BlockHeadContent.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

const BlockTitle = ({ className, page, ...props }) => {
    const classes = [`nk-block-title ${page ? 'page-title' : 'title'}${className ? ' ' + className : ''}`];
    return (
        <>
            {!props.tag
                ? (
                    <h3 className={classes}>{props.children}</h3>
                )
                : (
                    <props.tag className={classes}>{props.children}</props.tag>
                )}
        </>
    );
};

BlockTitle.propTypes = {
    className: PropTypes.string,
    page: PropTypes.bool,
    tag: PropTypes.string,
    children: PropTypes.node
};

const BlockDes = ({ className, page, ...props }) => {
    const classes = [`nk-block-des${className ? ' ' + className : ''}`];
    return <div className={classes}>{props.children}</div>;
};

BlockDes.propTypes = {
    className: PropTypes.string,
    page: PropTypes.bool,
    children: PropTypes.node
};

const BackTo = ({ className, link, icon, ...props }) => {
    const classes = [`back-to${className ? ' ' + className : ''}`];
    return (
        <div className="nk-block-head-sub">
            <Link className={classes} to={process.env.PUBLIC_URL + link}>
                <Icon name={icon} />
                <span>{props.children}</span>
            </Link>
        </div>
    );
};

BackTo.propTypes = {
    className: PropTypes.string,
    link: PropTypes.string,
    icon: PropTypes.string,
    children: PropTypes.node
};

export { Block, BlockContent, BlockBetween, BlockHead, BlockHeadContent, BlockTitle, BlockDes, BackTo };
