import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const Progress = ({ value, size, className }) => {
    const progressClass = classNames({
        progress: true,
        [`progress-${size}`]: size,
        [`${className}`]: className
    });
    return (
        <div className={progressClass}>
            <div className="progress-bar" style={{ width: `${value}%`, backgroundColor: '#6576ff' }}></div>
            <div className="progress-amount">{value}%</div>
        </div>
    );
};

Progress.propTypes = {
    value: PropTypes.number,
    size: PropTypes.string,
    className: PropTypes.string
};

export default Progress;
