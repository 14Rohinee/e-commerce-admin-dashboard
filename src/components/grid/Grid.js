import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const Col = ({ sm, lg, md, xxl, size, className, ...props }) => {
    const classNames = classnames({
        [`col-sm-${sm}`]: sm,
        [`col-lg-${lg}`]: lg,
        [`col-md-${md}`]: md,
        [`col-xxl-${xxl}`]: xxl,
        [`col-${size}`]: size,
        [`${className}`]: className
    });
    return <div className={classNames}>{props.children}</div>;
};

Col.propTypes = {
    sm: PropTypes.string,
    lg: PropTypes.string,
    md: PropTypes.string,
    xxl: PropTypes.string,
    size: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.node
};

const Row = ({ className, ...props }) => {
    const rowClass = classnames({
        row: true,
        [`${className}`]: className
    });
    return <div className={rowClass}>{props.children}</div>;
};

Row.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

export { Col, Row };
