import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const OverlineTitle = ({ className, alt, ...props }) => {
    const classes = classNames({
        'overline-title': true,
        [`${className}`]: className,
        'overline-title-alt': alt
    });
    return (
        <>
            {!props.tag
                ? (
                    <h6 className={classes}>{props.children}</h6>
                )
                : (
                    <props.tag className={classes}>{props.children}</props.tag>
                )}
        </>
    );
};

OverlineTitle.propTypes = {
    className: PropTypes.string,
    alt: PropTypes.bool,
    tag: PropTypes.string,
    children: PropTypes.node
};

export default OverlineTitle;
