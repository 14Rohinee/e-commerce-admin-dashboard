import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory, useParams } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, slugify, token, statuses } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import {
    Select2,
    Text,
    ButtonPrimary,
    ButtonSecondary
} from '../../components/form/Index';
import FileUpload from '../../components/form/FileUpload';
import PageLoader from '../../components/page-loader/PageLoader';

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [data, setData] = useState({});
    const [formValues, setFormValues] = useState({ status: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [files, setFiles] = useState([]);
    const [image, setImage] = useState(null);

    const [pageLoading, setPageLoading] = useState(true);
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    // Category id from params
    const { id } = useParams();

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const handleDropChange = (acceptedFiles) => {
        const updatedFiles = acceptedFiles.map((file) => {
            return Object.assign(file, {
                preview: URL.createObjectURL(file)
            });
        });
        setFiles(updatedFiles);
    };

    const handleRemoveFile = (index) => {
        setImage(null);

        if (index !== null) {
            const newFiles = [...files];
            newFiles.splice(index, 1);
            setFiles(newFiles);
        }
    };

    const getCategories = async () => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/categories/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } }
            );

            setData(response.data.data.category);
            if (response.data.data.category.imageName) {
                setImage(`${response.data.data.category.imagePath}/categories/${response.data.data.category.imageName}`);
            }

            setFormValues((prevState) => ({
                ...prevState,
                status: response.data.data.category.status
            }));
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.data.category);
        setPageLoading(false);
    };

    useEffect(() => {
        // Fetching data from api
        getCategories();
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.slug = slugify(e.name);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.image = files[0];

        try {
            const response = await axios.put(`${APP_URL}/categories/update/${data._id}`, e, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/categories`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Category Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <Form
                                        className={formClass}
                                        onSubmit={handleSubmit(onFormSubmit)}
                                        encType="multipart/form-data"
                                    >
                                        <Row className="g-gs">
                                            <Col lg="9">
                                                <PreviewCard>
                                                    <Row className="g-gs">
                                                        <Col md="6">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Name"
                                                                fieldName="name"
                                                                fieldId="name"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="E.g. Electronic"
                                                                fieldValue={data.name}
                                                            />
                                                        </Col>
                                                        <Col md="6">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Status"
                                                                fieldName="status"
                                                                fieldOptions={statuses}
                                                                setValue={onFormChange} // Pass setValue function
                                                                fieldValue={
                                                                    statuses.find(option => option.value === formValues.status)
                                                                }
                                                            />
                                                        </Col>
                                                        <Col md="12">
                                                            <ButtonPrimary
                                                                fieldLabel="Save"
                                                                formLoading={isFormSubmitted}
                                                                formLoadingLabel="Saving..."
                                                            />
                                                        </Col>
                                                    </Row>
                                                </PreviewCard>
                                            </Col>
                                            <Col lg="3">
                                                <PreviewCard>
                                                    <Col md="12">
                                                        <FileUpload
                                                            fieldLabel="Upload Image"
                                                            fieldName="image-dropzone"
                                                            fieldId="image-dropzone"
                                                            handleDropChange={handleDropChange}
                                                            handleRemoveFile={handleRemoveFile}
                                                            fieldFiles={files}
                                                            fieldDefaultFile={image}
                                                        />
                                                    </Col>
                                                </PreviewCard>
                                            </Col>
                                        </Row>
                                    </Form>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
