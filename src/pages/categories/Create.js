import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, slugify, token, statuses } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import { Select2, Text, ButtonSecondary, ButtonPrimary } from '../../components/form/Index';
import FileUpload from '../../components/form/FileUpload';

const Create = ({ pageTitle }) => {
    const [formValues, setFormValues] = useState({ status: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [files, setFiles] = useState([]);

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const handleDropChange = (acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })
            )
        );
    };

    const handleRemoveFile = (index) => {
        const newFiles = [...files];
        newFiles.splice(index, 1);
        setFiles(newFiles);
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.slug = slugify(e.name);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.image = files[0];
        try {
            const response = await axios.post(`${APP_URL}/categories/store`, e, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/categories`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            <Head title={pageTitle}></Head>
            <Content page="component" CardSize="xl">
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent>
                            <BlockDes className="text-soft"></BlockDes>
                        </BlockHeadContent>
                        <BlockHeadContent>
                            <ButtonSecondary
                                fieldLabel="Back"
                                icon="arrow-left"
                                redirectUrl="go-back"
                            />
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block size="sm">
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                        encType="multipart/form-data"
                    >
                        <Row className="g-gs">
                            <Col lg="9">
                                <PreviewCard>
                                    <Row className="g-gs">
                                        <Col md="6">
                                            <Text
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Name"
                                                fieldName="name"
                                                fieldId="name"
                                                fieldRequired={true}
                                                fieldPlaceHolder="E.g. Electronic"
                                            />
                                        </Col>
                                        <Col md="6">
                                            <Select2
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Status"
                                                fieldName="status"
                                                fieldOptions={statuses}
                                                setValue={onFormChange}
                                            />
                                        </Col>
                                        <Col md="12">
                                            <ButtonPrimary
                                                fieldLabel="Save"
                                                formLoading={isFormSubmitted}
                                                formLoadingLabel="Saving..."
                                            />
                                        </Col>
                                    </Row>
                                </PreviewCard>
                            </Col>
                            <Col lg="3">
                                <PreviewCard>
                                    <Col md="12">
                                        <FileUpload
                                            fieldLabel="Upload Image"
                                            fieldName="image-dropzone"
                                            fieldId="image-dropzone"
                                            handleDropChange={handleDropChange}
                                            fieldFiles={files}
                                            handleRemoveFile={handleRemoveFile}
                                        />
                                    </Col>
                                </PreviewCard>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </Content>
            <ToastContainer />
        </>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
