import React, { useEffect, useState } from 'react';

import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import CategoryDatatable from '../../components/data-table/pages/CategoryDatatable';
import { APP_URL, exportData, token } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import PageLoader from '../../components/page-loader/PageLoader';
import { useTitle } from '../../contexts/TitleContext';
import ButtonPrimary from '../../components/form/ButtonPrimary';

const Index = ({ pageTitle }) => {
    const [data, setData] = useState([]);
    const [originalData, setOriginalData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the categories
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/categories`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.categories);
        setOriginalData(response.data.categories);
        setTotal(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the categories on page load
    useEffect(() => {
        getData();
    }, []);

    // Function to export the data
    const exportCategories = async () => {
        const fieldName = ['Name', 'Status'];

        // fileName, title, fieldName, data
        exportData('categories.csv', 'Categories', fieldName, data);
    };
    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Categories..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li className="nk-block-tools-opt">
                                                    <ButtonPrimary
                                                        fieldLabel={pageTitle}
                                                        icon="plus"
                                                        redirectUrl={`${process.env.PUBLIC_URL}/account/categories/create`} />
                                                </li>
                                                <li>
                                                    <button type='button' className="btn btn-white btn-outline-light"
                                                        onClick={() => {
                                                            exportCategories();
                                                        } }>
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <CategoryDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
