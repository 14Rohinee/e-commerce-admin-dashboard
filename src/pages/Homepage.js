import React, { useEffect } from 'react';
import Head from '../layout/head/Head';
import Content from '../layout/content/Content';
import NewsUsers from '../components/partials/default/new-users/User';
import {
    Block,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    Row,
    Col,
    BlockBetween
} from '../components/Component';
import PropTypes from 'prop-types';
import { useTitle } from '../contexts/TitleContext';
import { Card } from 'reactstrap';

const Homepage = ({ pageTitle }) => {
    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    return (
        <>
            <Head title={pageTitle}></Head>
            <Content>
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent>
                            {/* <BlockTitle page tag="h3">
                                Sales Overview
                            </BlockTitle> */}
                            <BlockDes className="text-soft">
                                <p>Welcome to MERN Ecommerce Dashboard</p>
                            </BlockDes>
                        </BlockHeadContent>
                        {/* <BlockHeadContent>
                            <div className="toggle-wrap nk-block-tools-toggle">
                                <Button
                                    className={`btn-icon btn-trigger toggle-expand me-n1 ${sm ? 'active' : ''}`}
                                    onClick={() => updateSm(!sm)}
                                >
                                    <Icon name="more-v" />
                                </Button>
                                <div className="toggle-expand-content" style={{ display: sm ? 'block' : 'none' }}>
                                    <ul className="nk-block-tools g-3">
                                        <li>
                                            <UncontrolledDropdown>
                                                <DropdownToggle tag="a" className="dropdown-toggle btn btn-white btn-dim btn-outline-light">
                                                    <Icon className="d-none d-sm-inline" name="calender-date" />
                                                    <span>
                                                        <span className="d-none d-md-inline">Last</span> 30 Days
                                                    </span>
                                                    <Icon className="dd-indc" name="chevron-right" />
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    <ul className="link-list-opt no-bdr">
                                                        <li>
                                                            <DropdownItem
                                                                tag="a"
                                                                onClick={(ev) => {
                                                                    ev.preventDefault();
                                                                }}
                                                                href="#!"
                                                            >
                                                                <span>Last 30 days</span>
                                                            </DropdownItem>
                                                        </li>
                                                        <li>
                                                            <DropdownItem
                                                                tag="a"
                                                                onClick={(ev) => {
                                                                    ev.preventDefault();
                                                                }}
                                                                href="#dropdownitem"
                                                            >
                                                                <span>Last 6 months</span>
                                                            </DropdownItem>
                                                        </li>
                                                        <li>
                                                            <DropdownItem
                                                                tag="a"
                                                                onClick={(ev) => {
                                                                    ev.preventDefault();
                                                                }}
                                                                href="#dropdownitem"
                                                            >
                                                                <span>Last 3 weeks</span>
                                                            </DropdownItem>
                                                        </li>
                                                    </ul>
                                                </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </li>
                                        <li className="nk-block-tools-opt">
                                            <Button color="primary">
                                                <Icon name="reports" />
                                                <span>Reports</span>
                                            </Button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </BlockHeadContent> */}
                    </BlockBetween>
                </BlockHead>
                <Block>
                    <Row className="g-gs">
                        {/* <Col xxl="6">
                            <Row className="g-gs">
                                <Col lg="6" xxl="12">
                                    <PreviewCard>
                                        <SaleRevenue />
                                    </PreviewCard>
                                </Col>
                                <Col lg="6" xxl="12">
                                    <Row className="g-gs">
                                        <Col sm="6" lg="12" xxl="6">
                                            <PreviewAltCard>
                                                <ActiveSubscription />
                                            </PreviewAltCard>
                                        </Col>
                                        <Col sm="6" lg="12" xxl="6">
                                            <PreviewAltCard>
                                                <AvgSubscription />
                                            </PreviewAltCard>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col> */}
                        {/* <Col xxl="6">
                            <PreviewAltCard className="h-100">
                                <SalesOverview />
                            </PreviewAltCard>
                        </Col>
                        <Col xxl="8">
                            <Card className="card-bordered card-full">
                                <TransactionTable />
                            </Card>
                        </Col>
                        <Col xxl="4" md="6">
                            <Card className="card-bordered card-full">
                                <RecentActivity />
                            </Card>
                        </Col> */}
                        <Col xxl="4" md="6">
                            <Card className="card-bordered card-full">
                                <NewsUsers />
                            </Card>
                        </Col>
                        {/* <Col lg="6" xxl="4">
                            <Card className="card-bordered h-100">
                                <Support />
                            </Card>
                        </Col>
                        <Col lg="6" xxl="4">
                            <Card className="card-bordered h-100">
                                <Notifications />
                            </Card>
                        </Col> */}
                    </Row>
                </Block>
            </Content>
        </>
    );
};

Homepage.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Homepage;
