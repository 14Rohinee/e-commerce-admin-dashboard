import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    BlockDes,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween
} from '../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, genders, roles, statuses, token } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import { useHistory, useParams } from 'react-router';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import {
    Select2,
    Text,
    Email,
    Password,
    Tel,
    DatePicker,
    ButtonPrimary,
    ButtonSecondary,
    Radio
} from '../../components/form/Index';
import FileUpload from '../../components/form/FileUpload';
import PageLoader from '../../components/page-loader/PageLoader';

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });
    const [pageLoading, setPageLoading] = useState(true);
    const [DOB, setDOB] = useState();
    const [data, setData] = useState(0);
    const [formValues, setFormValues] = useState({
        role: '',
        status: '',
        gender: null
    });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [files, setFiles] = useState([]);
    const [image, setImage] = useState(null);

    // Category id from params
    const { id } = useParams();

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const handleDropChange = (acceptedFiles) => {
        const updatedFiles = acceptedFiles.map((file) => {
            return Object.assign(file, {
                preview: URL.createObjectURL(file)
            });
        });
        setFiles(updatedFiles);
    };

    const handleRemoveFile = (index) => {
        setImage(null);

        if (index !== null) {
            const newFiles = [...files];
            newFiles.splice(index, 1);
            setFiles(newFiles);
        }
    };

    const fetchUser = async () => {
        try {
            const response = await axios.get(`${APP_URL}/users/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } });
            setData(response.data.data.user);
            if (response.data.data.user.imageName) {
                setImage(`${response.data.data.user.imagePath}/users/${response.data.data.user.imageName}`);
            }

            setFormValues((prevState) => ({
                ...prevState,
                gender: response.data.data.user.gender,
                role: response.data.data.user.role,
                status: response.data.data.user.status
            }));

            // Check if dob is not null or undefined
            if (response.data.data.user.dob !== null && response.data.data.user.dob !== undefined) {
                setDOB(new Date(response.data.data.user.dob));
            }

            setPageLoading(false);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchUser();
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);

        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';

        // Check if dob is not null or undefined
        if (DOB !== null && DOB !== undefined) {
            e.dob = DOB.toISOString();
        }
        e.gender = formValues.gender;
        e.role = formValues.role;
        e.status = formValues.status;

        if (files.length > 0) {
            e.image = files[0];
        }

        try {
            const response = await axios.put(`${APP_URL}/users/update/${id}`, e, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });
            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/users`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Category Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <PreviewCard>
                                        <Form
                                            className={formClass}
                                            onSubmit={handleSubmit(onFormSubmit)}
                                            encType="multipart/form-data"
                                        >
                                            <Row className="g-gs">
                                                <Col lg="9">
                                                    <PreviewCard>
                                                        <Row className="g-gs">
                                                            <Col md="4">
                                                                <Text
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Full Name"
                                                                    fieldName="name"
                                                                    fieldId="fv-name"
                                                                    fieldRequired={true}
                                                                    fieldValue={data.name}
                                                                    fieldPlaceHolder="E.g. John Doe"
                                                                />
                                                            </Col>
                                                            <Col md="4">
                                                                <Email
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Email"
                                                                    fieldName="email"
                                                                    fieldId="fv-email"
                                                                    fieldRequired={true}
                                                                    fieldValue={data.email}
                                                                    fieldPlaceHolder="E.g. admin@example.com"
                                                                />
                                                            </Col>
                                                            <Col md="4">
                                                                <Password
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Password"
                                                                    fieldName="password"
                                                                    fieldId="fv-password"
                                                                    fieldRequired={true}
                                                                    fieldValue={data.password}
                                                                />
                                                            </Col>
                                                            <Col md="4">
                                                                <Tel
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Mobile Number"
                                                                    fieldName="mobile"
                                                                    fieldId="fv-mobile"
                                                                    fieldRequired={false}
                                                                    fieldValue={data.mobile}
                                                                    fieldPlaceHolder="E.g. 9876543210"
                                                                />
                                                            </Col>
                                                            <Col sm="4">
                                                                <DatePicker
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="DOB"
                                                                    fieldName="dob"
                                                                    fieldId="dob"
                                                                    fieldPlaceHolder="Select Date"
                                                                    selected={DOB}
                                                                    onChange={setDOB}
                                                                />
                                                            </Col>
                                                            <Col md="4">
                                                                <Radio
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Gender"
                                                                    fieldName="gender"
                                                                    fieldOptions={genders}
                                                                    fieldValue={data.gender}
                                                                    setValue={onFormChange}
                                                                    fieldToolTip={true}
                                                                    fieldToolTipContent="Gender value N/A means it's not disclosed by the user"
                                                                />
                                                            </Col>
                                                            <Col md="4">
                                                                <Select2
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Role"
                                                                    fieldName="role"
                                                                    fieldOptions={roles}
                                                                    setValue={onFormChange} // Pass setValue function
                                                                    fieldValue={
                                                                        roles.find(option => option.value === formValues.role)
                                                                    }
                                                                />
                                                            </Col>
                                                            <Col md="4">
                                                                <Select2
                                                                    register={register}
                                                                    errors={errors}
                                                                    fieldLabel="Status"
                                                                    fieldName="status"
                                                                    fieldOptions={statuses}
                                                                    setValue={onFormChange} // Pass setValue function
                                                                    fieldValue={
                                                                        statuses.find(option => option.value === formValues.status)
                                                                    }
                                                                />
                                                            </Col>
                                                            <Col md="12">
                                                                <ButtonPrimary
                                                                    fieldLabel="Save"
                                                                    formLoading={isFormSubmitted}
                                                                    formLoadingLabel="Saving..."
                                                                />
                                                            </Col>
                                                        </Row>
                                                    </PreviewCard>
                                                </Col>
                                                <Col lg="3">
                                                    <PreviewCard>
                                                        <Col md="12">
                                                            <FileUpload
                                                                fieldLabel="Upload Image"
                                                                fieldName="image-dropzone"
                                                                fieldId="image-dropzone"
                                                                handleDropChange={handleDropChange}
                                                                handleRemoveFile={handleRemoveFile}
                                                                fieldFiles={files}
                                                                fieldDefaultFile={image}
                                                            />
                                                        </Col>
                                                    </PreviewCard>
                                                </Col>
                                            </Row>
                                        </Form>
                                    </PreviewCard>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
