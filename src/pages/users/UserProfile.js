import React, { useEffect, useState } from 'react';
import Head from '../../layout/head/Head';
import DatePicker from 'react-datepicker';
import { Modal, ModalBody } from 'reactstrap';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon,
    Row,
    Col,
    Button,
    RSelect
} from '../../components/Component';
import { APP_URL, getDateStructured } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';

const UserProfile = ({ sm, updateSm, setProfileName, setProfileEmail, pageTitle }) => {
    let user = localStorage.getItem('loggedInUser');
    user = JSON.parse(user);

    const [genderOptions] = useState([
        { value: 'male', label: 'Male' },
        { value: 'female', label: 'Female' },
        { value: 'other', label: 'Other' }
    ]);

    const [userInfo, setUserInfo] = useState({});
    const [formData, setFormData] = useState({
        name: '',
        displayName: '',
        phone: '',
        dob: '1980-08-10',
        gender: '',
        role: '',
        email: ''
    });
    const [modal, setModal] = useState(false);

    const fetchUser = async () => {
        try {
            const response = await axios.get(`${APP_URL}/users/edit/${user._id}`);
            setUserInfo({
                id: response.data.data.user._id,
                avatarBg: 'purple',
                name: response.data.data.user.name,
                displayName: response.data.data.user.name,
                dob: (response.data.data.user && response.data.data.user.dob) ? response.data.data.user.dob : '--',
                checked: true,
                email: response.data.data.user.email,
                phone: response.data.data.user.mobile,
                lastLogin: '10 Feb 2020',
                status: response.data.data.user.status,
                gender: (response.data.data.user && response.data.data.user.gender) ? response.data.data.user.gender : '--',
                role: (response.data.data.user && response.data.data.user.role) ? response.data.data.user.role : '--'
            });
            setFormData({
                name: response.data.data.user.name,
                displayName: response.data.data.user.name,
                phone: response.data.data.user.mobile,
                dob: (response.data.data.user && response.data.data.user.dob) ? response.data.data.user.dob : '',
                gender: (response.data.data.user && response.data.data.user.gender) ? response.data.data.user.gender : '--',
                role: (response.data.data.user && response.data.data.user.role) ? response.data.data.user.role : '--',
                email: response.data.data.user.email
            });

            setProfileName(response.data.data.user.name);
            setProfileEmail(response.data.data.user.email);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    useEffect(() => {
        fetchUser();
    }, []);

    const submitForm = async () => {
        try {
            const response = await axios.put(`${APP_URL}/users/update/${user._id}`, formData);

            showToast('success', response.data.message, () => {
                setModal(false);
                // Reload the component
                fetchUser();
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            <Head title={pageTitle}></Head>
            <BlockHead size="lg">
                <BlockBetween>
                    <BlockHeadContent>
                        <BlockTitle tag="h4">Personal Information</BlockTitle>
                        <BlockDes>
                            <p>Basic info like your name, email, contact etc.</p>
                        </BlockDes>
                    </BlockHeadContent>
                    <BlockHeadContent className="align-self-start d-lg-none">
                        <Button
                            className={`toggle btn btn-icon btn-trigger mt-n1 ${sm ? 'active' : ''}`}
                            onClick={() => updateSm(!sm)}
                        >
                            <Icon name="menu-alt-r"></Icon>
                        </Button>
                    </BlockHeadContent>
                </BlockBetween>
            </BlockHead>

            <Block>
                <div className="nk-data data-list">
                    <div className="data-head">
                        <h6 className="overline-title">Basics</h6>
                    </div>
                    <div className="data-item" onClick={() => setModal(true)}>
                        <div className="data-col">
                            <span className="data-label">Full Name</span>
                            <span className="data-value">{userInfo.name}</span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more">
                                <Icon name="user"></Icon>
                            </span>
                        </div>
                    </div>
                    <div className="data-item" onClick={() => setModal(true)}>
                        <div className="data-col">
                            <span className="data-label">Display Name</span>
                            <span className="data-value">{userInfo.displayName}</span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more">
                                <Icon name="user"></Icon>
                            </span>
                        </div>
                    </div>
                    <div className="data-item">
                        <div className="data-col">
                            <span className="data-label">Email</span>
                            <span className="data-value">{userInfo.email}</span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more disable">
                                <Icon name="mail"></Icon>
                            </span>
                        </div>
                    </div>
                    <div className="data-item" onClick={() => setModal(true)}>
                        <div className="data-col">
                            <span className="data-label">Phone Number</span>
                            <span className="data-value text-soft">{userInfo.phone}</span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more">
                                <Icon name="call"></Icon>
                            </span>
                        </div>
                    </div>
                    <div className="data-item" onClick={() => setModal(true)}>
                        <div className="data-col">
                            <span className="data-label">Date of Birth</span>
                            <span className="data-value">{userInfo.dob}</span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more">
                                <Icon name="calendar"></Icon>
                            </span>
                        </div>
                    </div>
                    <div className="data-item" onClick={() => setModal(true)}>
                        <div className="data-col">
                            <span className="data-label">Gender</span>
                            <span className="data-value">{userInfo.gender}</span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more">
                                <Icon name="opt-alt"></Icon>
                            </span>
                        </div>
                    </div>
                    <div className="data-item" onClick={() => setModal(true)}>
                        <div className="data-col">
                            <span className="data-label">Role</span>
                            <span className="data-value">
                                {
                                    (userInfo.role === 'admin') ? 'Administrator' : 'Customer'
                                }
                            </span>
                        </div>
                        <div className="data-col data-col-end">
                            <span className="data-more">
                                <Icon name="user-c"></Icon>
                            </span>
                        </div>
                    </div>
                </div>
            </Block>

            <Modal isOpen={modal} className="modal-dialog-centered" size="lg" toggle={() => setModal(false)}>
                <a
                    href="#dropdownitem"
                    onClick={(ev) => {
                        ev.preventDefault();
                        setModal(false);
                    }}
                    className="close"
                >
                    <Icon name="cross-sm"></Icon>
                </a>
                <ModalBody>
                    <div className="p-2">
                        <h5 className="title">Update Profile</h5>
                        <div className="tab-content mt-4">
                            <div className={'tab-pane active'} id="personal">
                                <Row className="gy-4">
                                    <Col md="6">
                                        <div className="form-group">
                                            <label className="form-label" htmlFor="full-name">
                                                Full Name
                                            </label>
                                            <input
                                                type="text"
                                                id="full-name"
                                                className="form-control"
                                                name="name"
                                                onChange={(e) => {
                                                    setFormData({ ...formData, name: e.target.value });
                                                }}
                                                defaultValue={formData.name}
                                                placeholder="Enter Full name"
                                            />
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <div className="form-group">
                                            <label className="form-label" htmlFor="display-name">
                                                Display Name
                                            </label>
                                            <input
                                                type="text"
                                                id="display-name"
                                                className="form-control"
                                                name="displayName"
                                                onChange={(e) => {
                                                    setFormData({ ...formData, displayName: e.target.value });
                                                }}
                                                defaultValue={formData.displayName}
                                                placeholder="Enter display name"
                                            />
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <div className="form-group">
                                            <label className="form-label" htmlFor="email-id">
                                                Email
                                            </label>
                                            <input
                                                type="email"
                                                id="email-id"
                                                className="form-control"
                                                name="email"
                                                onChange={(e) => {
                                                    setFormData({ ...formData, email: e.target.value });
                                                }}
                                                defaultValue={formData.email}
                                                placeholder="Email Id"
                                            />
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <div className="form-group">
                                            <label className="form-label" htmlFor="phone-no">
                                                Phone Number
                                            </label>
                                            <input
                                                type="number"
                                                id="phone-no"
                                                className="form-control"
                                                name="phone"
                                                onChange={(e) => {
                                                    setFormData({ ...formData, phone: e.target.value });
                                                }}
                                                defaultValue={formData.phone}
                                                placeholder="Phone Number"
                                            />
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <div className="form-group">
                                            <label className="form-label" htmlFor="birth-day">
                                                Date of Birth
                                            </label>
                                            <DatePicker
                                                selected={new Date(formData.dob)}
                                                className="form-control"
                                                onChange={(date) => setFormData({ ...formData, dob: getDateStructured(date) })}
                                                maxDate={new Date()}
                                            />
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <div className="form-group">
                                            <label className="form-label" htmlFor="gender">
                                                Gender
                                            </label>
                                            <RSelect
                                                options={genderOptions}
                                                placeholder="Select a gender"
                                                defaultValue={[
                                                    {
                                                        value: formData.gender,
                                                        label: formData.gender
                                                    }
                                                ]}
                                                onChange={(e) => setFormData({ ...formData, gender: e.value })}
                                            />
                                        </div>
                                    </Col>
                                    <Col size="12">
                                        <ul className="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                            <li>
                                                <Button
                                                    color="primary"
                                                    size="lg"
                                                    onClick={(ev) => {
                                                        ev.preventDefault();
                                                        submitForm();
                                                    }}
                                                >
                                                    Update Profile
                                                </Button>
                                            </li>
                                            <li>
                                                <a
                                                    href="#dropdownitem"
                                                    onClick={(ev) => {
                                                        ev.preventDefault();
                                                        setModal(false);
                                                    }}
                                                    className="link link-light"
                                                >
                                                    Cancel
                                                </a>
                                            </li>
                                        </ul>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
            <ToastContainer />
        </>
    );
};

UserProfile.propTypes = {
    sm: PropTypes.bool,
    updateSm: PropTypes.func,
    setProfileName: PropTypes.func,
    setProfileEmail: PropTypes.func,
    pageTitle: PropTypes.string
};

export default UserProfile;
