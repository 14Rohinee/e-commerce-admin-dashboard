import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import UserDatatable from '../../components/data-table/pages/UserDatatable';
import { useTitle } from '../../contexts/TitleContext';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import PageLoader from '../../components/page-loader/PageLoader';
import { APP_URL, exportData, token } from '../../utils/Constants';
import ButtonPrimary from '../../components/form/ButtonPrimary';

const Index = ({ pageTitle }) => {
    const [data, setData] = useState([]);
    const [originalData, setOriginalData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    // const [exportLoading, setExportLoading] = useState(false);
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the users
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/users`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
            setData(response.data.users);
            setOriginalData(response.data.users);
            setTotal(response.data.total);
            setPageLoading(false);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    useEffect(() => {
        getData();
    }, []);

    // Function to export the data
    const exportUsers = async () => {
        try {
            // setExportLoading(true);
            const fieldName = null;

            const csvData = data.map((item) => {
                return {
                    Name: item.name,
                    EmployeeID: item.employeeId ? item.employeeId : '--',
                    Role: item.role,
                    Email: item.email,
                    Mobile: item.mobile,
                    Company: item.company ? item.company.name : '--',
                    Verification: item.verifiedAt === null || item.verifiedAt === undefined ? 'Send Reminder Email' : 'Verified',
                    Status: item.status
                };
            });

            // fileName, title, fieldName, data
            await exportData('users.csv', 'Users', fieldName, csvData);
        } catch (error) {
            showToast('error', error);
        } finally {
            // setExportLoading(false);
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Users..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li className="nk-block-tools-opt">
                                                    <ButtonPrimary
                                                        fieldLabel={pageTitle}
                                                        icon="plus"
                                                        redirectUrl={`${process.env.PUBLIC_URL}/account/users/create`}
                                                    />
                                                </li>
                                                <li>
                                                    <a href="#export" className="btn btn-white btn-outline-light" onClick={(ev) => {
                                                        ev.preventDefault();
                                                        exportUsers();
                                                    }} >
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <UserDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
