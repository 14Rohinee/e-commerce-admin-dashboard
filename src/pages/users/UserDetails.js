import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import { Badge, Button, Card, Col, Row, Tooltip } from 'reactstrap';
import {
    Block,
    BlockBetween,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon,
    Sidebar,
    UserAvatar,
    LoginLogTable
} from '../../components/Component';
import axios from 'axios';
import { APP_URL, findUpper, formatDateTime, token } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import { useParams, useHistory } from 'react-router';
import PageLoader from '../../components/page-loader/PageLoader';
import OrderDatatable from '../../components/data-table/pages/OrderDatatable';
import PaymentDatatable from '../../components/data-table/pages/PaymentDatatable';
import ShippingAddressDatatable from '../../components/data-table/pages/ShippingAddressDatatable';

const UserDetails = ({ pageTitle }) => {
    const [tab, setTab] = useState('1');
    const [user, setUser] = useState();
    const [sideBar, setSidebar] = useState(false);
    const { id } = useParams();
    const history = useHistory();
    const [orderOriginalData, setOrderOriginalData] = useState([]);
    const [orderData, setOrderData] = useState([]);
    const [totalOrder, setTotalOrder] = useState(0);
    const [pageLoading, setPageLoading] = useState(true);
    const [paymentOriginalData, setPaymentOriginalData] = useState([]);
    const [paymentData, setPaymentData] = useState([]);
    const [totalPayment, setTotalPayment] = useState(0);
    const [shippingAddressOriginalData, setShippingAddressOriginalData] = useState([]);
    const [shippingAddressData, setShippingAddressData] = useState([]);
    const [totalShippingAddress, setTotalShippingAddress] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    const [actions, setActions] = useState([
        {
            icon: 'edit',
            title: 'Edit User',
            link: `${process.env.PUBLIC_URL}/account/users/edit/${id}`,
            tooltipOpen: false,
            setTooltipOpen: () => {
                setActions((prevState) => {
                    const newState = [...prevState];
                    newState[0].tooltipOpen = !newState[0].tooltipOpen;
                    return newState;
                });
            }
        },
        {
            icon: 'check',
            title: 'Enable User',
            link: '#',
            tooltipOpen: false,
            setTooltipOpen: () => {
                setActions((prevState) => {
                    const newState = [...prevState];
                    newState[1].tooltipOpen = !newState[1].tooltipOpen;
                    return newState;
                });
            }
        },
        {
            icon: 'cross',
            title: 'Disable User',
            link: '#',
            tooltipOpen: false,
            setTooltipOpen: () => {
                setActions((prevState) => {
                    const newState = [...prevState];
                    newState[2].tooltipOpen = !newState[2].tooltipOpen;
                    return newState;
                });
            }
        },
        {
            icon: 'undo',
            title: 'Restore User',
            link: '#',
            tooltipOpen: false,
            setTooltipOpen: () => {
                setActions((prevState) => {
                    const newState = [...prevState];
                    newState[3].tooltipOpen = !newState[3].tooltipOpen;
                    return newState;
                });
            }
        },
        {
            icon: 'trash',
            title: 'Delete User',
            link: '#',
            tooltipOpen: false,
            setTooltipOpen: () => {
                setActions((prevState) => {
                    const newState = [...prevState];
                    newState[4].tooltipOpen = !newState[4].tooltipOpen;
                    return newState;
                });
            }
        }
    ]);

    // function to toggle sidebar
    const toggle = () => {
        setSidebar(!sideBar);
    };

    const fetchUser = async () => {
        try {
            const response = await axios.get(`${APP_URL}/users/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } });
            setUser(response.data.data.user);
            setPageLoading(false);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    // Function to remove class from element using getElementsByClassName
    const removeClassFromElement = (classNameToRemove) => {
        const elements = document.getElementsByClassName(classNameToRemove);
        for (const element of elements) {
            element.classList.remove('btn-secondary');
        }
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchUser();
        removeClassFromElement('remove-btn-secondary');
    }, []);

    // Function to get all the order
    const getOrder = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/orders`, { params: { ...pagination, userId: id }, headers: token });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setOrderData(response.data.orders);
        setOrderOriginalData(response.data.orders);
        setTotalOrder(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the payment
    const getPayment = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/payments`, { params: { ...pagination, userId: id }, headers: token });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setPaymentData(response.data.payments);
        setPaymentOriginalData(response.data.payments);
        setTotalPayment(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the shipping address
    const getShippingAddress = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/shipping-addresses`, { params: { ...pagination, userId: id }, headers: token });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setShippingAddressData(response.data.shippingAddresses);
        setShippingAddressOriginalData(response.data.shippingAddresses);
        setTotalShippingAddress(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the order, payment on page load
    useEffect(() => {
        getOrder();
        getPayment();
        getShippingAddress();
    }, []);

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Users Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <Button
                                                color="light"
                                                outline
                                                className="bg-white d-none d-sm-inline-flex"
                                                onClick={() => history.goBack()}
                                            >
                                                <Icon name="arrow-left"></Icon>
                                                <span>Back</span>
                                            </Button>
                                            <a
                                                href="#back"
                                                onClick={(ev) => {
                                                    ev.preventDefault();
                                                    history.goBack();
                                                }}
                                                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
                                            >
                                                <Icon name="arrow-left"></Icon>
                                            </a>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block>
                                    <Card className="card-bordered">
                                        <div className="card-aside-wrap" id="user-detail-block">
                                            <div className="card-content">
                                                <ul className="nav nav-tabs nav-tabs-mb-icon nav-tabs-card">
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '1'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#personal"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('1');
                                                            }}
                                                        >
                                                            <Icon name="user-circle"></Icon>
                                                            <span>Personal Details</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '2'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#transactions"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('2');
                                                            }}
                                                        >
                                                            <Icon name="cart"></Icon>
                                                            <span>Orders</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '3'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#documents"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('3');
                                                            }}
                                                        >
                                                            <Icon name="repeat"></Icon>
                                                            <span>Transactions</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '4'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#activities"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('4');
                                                            }}
                                                        >
                                                            <Icon name="home"></Icon>
                                                            <span>Shipping Addresses</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '5'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#activities"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('5');
                                                            }}
                                                        >
                                                            <Icon name="star"></Icon>
                                                            <span>Reviews</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '6'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#notifications"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('6');
                                                            }}
                                                        >
                                                            <Icon name="bell"></Icon>
                                                            <span>Notifications</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a
                                                            className={
                                                                tab === '7'
                                                                    ? 'nav-link active'
                                                                    : 'nav-link'
                                                            }
                                                            href="#activities"
                                                            onClick={(ev) => {
                                                                ev.preventDefault();
                                                                setTab('7');
                                                            }}
                                                        >
                                                            <Icon name="activity"></Icon>
                                                            <span>Activities</span>
                                                        </a>
                                                    </li>
                                                    <li className="nav-item nav-item-trigger d-xxl-none">
                                                        <Button
                                                            className={`toggle btn-icon btn-trigger remove-btn-secondary ${sideBar && 'active'
                                                            }`}
                                                            onClick={toggle}
                                                        >
                                                            <Icon name="user-list-fill"></Icon>
                                                        </Button>
                                                    </li>
                                                </ul>

                                                {/* START: Personal detail tab */}
                                                <div
                                                    className={
                                                        tab === '1' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="personal"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Personal Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                        </BlockHead>
                                                        <div className="profile-ud-list">
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Salutation
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        Mr.
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Full Name
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        {user?.name}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Date of Birth
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        {
                                                                            user &&
                                                                            (user.dob ===
                                                                                undefined ||
                                                                                user.dob === null ||
                                                                                user.dob === ''
                                                                                ? '--'
                                                                                : formatDateTime(
                                                                                    user.dob
                                                                                ))
                                                                        }
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Mobile Number
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        {user?.mobile}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Email Address
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        {user?.email}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Gender
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        {user &&
                                                                            (user.gender ===
                                                                                undefined ||
                                                                                user.gender === null ||
                                                                                user.gender === ''
                                                                                ? '--'
                                                                                : user.gender)}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Block>
                                                    <Block>
                                                        <BlockHead className="nk-block-head-line">
                                                            <BlockTitle
                                                                tag="h6"
                                                                className="overline-title text-base"
                                                            >
                                                                Additional Information
                                                            </BlockTitle>
                                                        </BlockHead>
                                                        <div className="profile-ud-list">
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Joining Date
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        08-16-2018 09:04PM
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Reg Method
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        Email
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Country
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        India
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="profile-ud-item">
                                                                <div className="profile-ud wider">
                                                                    <span className="profile-ud-label">
                                                                        Nationality
                                                                    </span>
                                                                    <span className="profile-ud-value">
                                                                        India
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Block>
                                                </div>
                                                {/* END: Personal detail tab */}

                                                {/*  START: Order tab */}
                                                <div
                                                    className={
                                                        tab === '2' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="orders"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Orders Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                            <OrderDatatable
                                                                data={orderData}
                                                                getData={getOrder}
                                                                setData={setOrderData}
                                                                originalData={orderOriginalData}
                                                                total={totalOrder}
                                                                setTotal={setTotalOrder}
                                                                pagination={pagination}
                                                                setPagination={setPagination}
                                                            />
                                                        </BlockHead>
                                                    </Block>
                                                </div>
                                                {/* END: Order tab */}

                                                {/* START: Transactions tab */}
                                                <div
                                                    className={
                                                        tab === '3' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="transactions"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Transactions Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                            <PaymentDatatable
                                                                data={paymentData}
                                                                getData={getPayment}
                                                                setData={setPaymentData}
                                                                originalData={paymentOriginalData}
                                                                total={totalPayment}
                                                                setTotal={setTotalPayment}
                                                                pagination={pagination}
                                                                setPagination={setPagination}
                                                            />
                                                        </BlockHead>
                                                    </Block>
                                                </div>
                                                {/* END: Transactions tab */}

                                                {/* START: Shipping Addresses tab */}
                                                <div
                                                    className={
                                                        tab === '4' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="shipping-addresses"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Shipping Address Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                            <ShippingAddressDatatable
                                                                data={shippingAddressData}
                                                                getData={getShippingAddress}
                                                                setData={setShippingAddressData}
                                                                originalData={shippingAddressOriginalData}
                                                                total={totalShippingAddress}
                                                                setTotal={setTotalShippingAddress}
                                                                pagination={pagination}
                                                                setPagination={setPagination}
                                                            />
                                                        </BlockHead>
                                                    </Block>
                                                </div>
                                                {/* END: Shipping Addresses tab */}

                                                {/* START: Review tab */}
                                                <div
                                                    className={
                                                        tab === '5' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="reviews"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Review Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                        </BlockHead>
                                                    </Block>
                                                </div>
                                                {/* END: Review tab */}

                                                {/* START: Notification tab */}
                                                <div
                                                    className={
                                                        tab === '6' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="reviews"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Notification Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                        </BlockHead>
                                                    </Block>
                                                </div>
                                                {/* END: Notification tab */}

                                                {/* START: Activities tab */}
                                                <div
                                                    className={
                                                        tab === '7' ? 'card-inner' : 'd-none'
                                                    }
                                                    id="reviews"
                                                >
                                                    <Block>
                                                        <BlockHead>
                                                            <BlockTitle tag="h5">
                                                                Activities Information
                                                            </BlockTitle>
                                                            <p>
                                                                Basic info, like your name and
                                                                address, that you use on Nio
                                                                Platform.
                                                            </p>
                                                            <LoginLogTable />
                                                        </BlockHead>
                                                    </Block>
                                                </div>
                                                {/* END: Activities tab */}
                                            </div>

                                            {/* START: Sidebar */}
                                            <Sidebar toggleState={sideBar}>
                                                <div className="card-inner">
                                                    <div className="user-card user-card-s2 mt-5 mt-xxl-0">
                                                        <UserAvatar
                                                            className="lg"
                                                            theme="primary"
                                                            text={
                                                                user && user.name !== null
                                                                    ? findUpper(user.name)
                                                                    : 'N/A'
                                                            }
                                                            image={
                                                                user && user.image ? 'http://localhost:5001/public/user-uploads/users/' + user.image : ''
                                                            }
                                                        />
                                                        <div className="user-info">
                                                            <Badge
                                                                color="outline-light"
                                                                pill
                                                                className="ucap"
                                                            >
                                                                {user?.role}
                                                            </Badge>
                                                            <h5>{user?.name}</h5>
                                                            <span className="sub-text">
                                                                {user?.email}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card-inner card-inner-sm">
                                                    <ul className="btn-toolbar justify-center gx-1">
                                                        {
                                                            actions.map((action, index) => {
                                                                return (
                                                                    <li key={index}>
                                                                        <Button
                                                                            href={action.link}
                                                                            className={action.icon === 'trash' ? 'btn-trigger btn-icon remove-btn-secondary text-danger' : 'btn-trigger btn-icon remove-btn-secondary'}
                                                                            id={action.icon}
                                                                            onClick={(ev) => {
                                                                                ev.preventDefault();
                                                                                history.push(action.link);
                                                                            }}
                                                                        >
                                                                            <Icon name={action.icon}></Icon>
                                                                        </Button>
                                                                        <Tooltip isOpen={action.tooltipOpen} target={action.icon} toggle={action.setTooltipOpen}>
                                                                            {action.title}
                                                                        </Tooltip>
                                                                    </li>
                                                                );
                                                            })
                                                        }

                                                    </ul>
                                                </div>
                                                <div className="card-inner">
                                                    <div className="overline-title-alt mb-2">
                                                        In Account
                                                    </div>
                                                    <div className="profile-balance">
                                                        <div className="profile-balance-group gx-4">
                                                            <div className="profile-balance-sub">
                                                                <div className="profile-balance-amount">
                                                                    <div className="number">
                                                                        289.00{' '}
                                                                        <small className="currency currency-usd">
                                                                            USD
                                                                        </small>
                                                                    </div>
                                                                </div>
                                                                <div className="profile-balance-subtitle">
                                                                    Purchased Amount
                                                                </div>
                                                            </div>
                                                            <div className="profile-balance-sub">
                                                                <span className="profile-balance-plus text-soft">
                                                                    <Icon className="ni-plus"></Icon>
                                                                </span>
                                                                <div className="profile-balance-amount">
                                                                    <div className="number">
                                                                        143.76{' '}
                                                                        <small className="currency currency-usd">
                                                                            USD
                                                                        </small>
                                                                    </div>
                                                                </div>
                                                                <div className="profile-balance-subtitle">
                                                                    Profit Earned
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card-inner">
                                                    <Row className="text-center">
                                                        <Col size="4">
                                                            <div className="profile-stats">
                                                                <span className="amount">
                                                                    50
                                                                </span>
                                                                <span className="sub-text">
                                                                    Total Order
                                                                </span>
                                                            </div>
                                                        </Col>
                                                        <Col size="4">
                                                            <div className="profile-stats">
                                                                <span className="amount">
                                                                    30
                                                                </span>
                                                                <span className="sub-text">
                                                                    Complete
                                                                </span>
                                                            </div>
                                                        </Col>
                                                        <Col size="4">
                                                            <div className="profile-stats">
                                                                <span className="amount">
                                                                    20
                                                                </span>
                                                                <span className="sub-text">
                                                                    Progress
                                                                </span>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>
                                                <div className="card-inner">
                                                    <h6 className="overline-title-alt mb-2">
                                                        Additional
                                                    </h6>
                                                    <Row className="row g-3">
                                                        <Col size="6" className="col-6">
                                                            <span className="sub-text">
                                                                User ID:
                                                            </span>
                                                            <span>
                                                                EMP0001
                                                                {/* { user.employeeId === null || user.employeeId === undefined ? "N/A" : user.employeeId } */}
                                                            </span>
                                                        </Col>
                                                        <Col size="6" className="col-6">
                                                            <span className="sub-text">
                                                                Last Login:
                                                            </span>
                                                            <span>10 Feb 2020 01:02 PM</span>
                                                        </Col>
                                                        <Col size="6" className="col-6">
                                                            <span className="sub-text">
                                                                Account Status:
                                                            </span>

                                                            <span
                                                                className={`lead-text text-${user &&
                                                                    (user.deletedAt ===
                                                                        undefined ||
                                                                        user.deletedAt === null ||
                                                                        user.deletedAt === ''
                                                                        ? user.status ===
                                                                            'active'
                                                                            ? 'success'
                                                                            : 'danger'
                                                                        : 'danger')
                                                                }
                                                `}
                                                            >
                                                                {user &&
                                                                    (user.deletedAt ===
                                                                        undefined ||
                                                                        user.deletedAt === null ||
                                                                        user.deletedAt === ''
                                                                        ? user.status ===
                                                                            'active'
                                                                            ? 'Active'
                                                                            : 'Inactive'
                                                                        : 'Deleted')}
                                                            </span>
                                                        </Col>
                                                        <Col size="6" className="col-6">
                                                            <span className="sub-text">
                                                                Verification Status:
                                                            </span>
                                                            <span
                                                                className={'lead-text text-success'}
                                                            >
                                                                Verified
                                                            </span>
                                                        </Col>
                                                        <Col size="6" className="col-6">
                                                            <span className="sub-text">
                                                                Register At:
                                                            </span>
                                                            <span>
                                                                {user &&
                                                                    (user.createdAt ===
                                                                        undefined ||
                                                                        user.createdAt === null ||
                                                                        user.createdAt === ''
                                                                        ? 'N/A'
                                                                        : formatDateTime(
                                                                            user.createdAt
                                                                        ))}
                                                            </span>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Sidebar>
                                            {sideBar && (
                                                <div
                                                    className="toggle-overlay"
                                                    onClick={() => toggle()}
                                                ></div>
                                            )}

                                            {/* END: Sidebar */}
                                        </div>
                                    </Card>
                                </Block>
                            </Content>
                        </>
                    )}
        </>
    );
};

UserDetails.propTypes = {
    pageTitle: propTypes.string
};
export default UserDetails;
