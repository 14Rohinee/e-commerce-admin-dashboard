import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import UserProfileRegularPage from './UserProfile';
import UserProfileSettingPage from './UserProfileSetting';
import UserProfileNotificationPage from './UserProfileNotification';
import UserProfileActivityPage from './UserProfileActivity';
import { Route, Switch, Link } from 'react-router-dom';
import { Icon, UserAvatar } from '../../components/Component';
import { findUpper } from '../../utils/Constants';
import { Card, DropdownItem, UncontrolledDropdown, DropdownMenu, DropdownToggle } from 'reactstrap';
import { useTitle } from '../../contexts/TitleContext';
import PropTypes from 'prop-types';

const UserProfileLayout = ({ pageTitle }) => {
    const [sm, updateSm] = useState(false);
    const [mobileView, setMobileView] = useState(false);
    const [profileName, setProfileName] = useState('');
    const [profileEmail, setProfileEmail] = useState('');

    // function to change the design view under 990 px
    const viewChange = () => {
        if (window.innerWidth < 990) {
            setMobileView(true);
        } else {
            setMobileView(false);
            updateSm(false);
        }
    };

    useEffect(() => {
        viewChange();
        window.addEventListener('load', viewChange);
        window.addEventListener('resize', viewChange);
        document.getElementsByClassName('nk-header')[0].addEventListener('click', function () {
            updateSm(false);
        });
        return () => {
            window.removeEventListener('resize', viewChange);
            window.removeEventListener('load', viewChange);
        };
    }, []);

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [setDocumentTitle]);

    return (
        <>
            <Content>
                <Card className="card-bordered">
                    <div className="card-aside-wrap">
                        <div
                            className={`card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg ${sm ? 'content-active' : ''
                            }`}
                        >
                            <div className="card-inner-group">
                                <div className="card-inner">
                                    <div className="user-card">
                                        <UserAvatar text={findUpper(profileName)} theme="primary" />
                                        <div className="user-info">
                                            <span className="lead-text">{profileName}</span>
                                            <span className="sub-text">{profileEmail}</span>
                                        </div>
                                        <div className="user-action">
                                            <UncontrolledDropdown>
                                                <DropdownToggle tag="a" className="btn btn-icon btn-trigger me-n2">
                                                    <Icon name="more-v"></Icon>
                                                </DropdownToggle>
                                                <DropdownMenu end>
                                                    <ul className="link-list-opt no-bdr">
                                                        <li>
                                                            <DropdownItem
                                                                tag="a"
                                                                href="#dropdownitem"
                                                                onClick={(ev) => {
                                                                    ev.preventDefault();
                                                                }}
                                                            >
                                                                <Icon name="camera-fill"></Icon>
                                                                <span>Change Photo</span>
                                                            </DropdownItem>
                                                        </li>
                                                    </ul>
                                                </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-inner">
                                    <div className="user-account-info py-0">
                                        <h6 className="overline-title-alt">Total Order Spend</h6>
                                        <div className="user-balance">
                                            12395.76 <small className="currency currency-btc">USD</small>
                                        </div>
                                        <div className="user-balance-sub">
                                            In Transit{' '}
                                            <span>
                                                909.78 <span className="currency currency-btc">USD</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-inner p-0">
                                    <ul className="link-list-menu">
                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/profile`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/profile` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="user-fill-c"></Icon>
                                                <span>Personal Information</span>
                                            </Link>
                                        </li>
                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/user-profile-notification`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/user-profile-notification`
                                                        ? 'active'
                                                        : ''
                                                }
                                            >
                                                <Icon name="bell-fill"></Icon>
                                                <span>Notifications</span>
                                            </Link>
                                        </li>
                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/user-profile-activity`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/user-profile-activity` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="activity-round-fill"></Icon>
                                                <span>Account Activity</span>
                                            </Link>
                                        </li>
                                        <li onClick={() => updateSm(false)}>
                                            <Link
                                                to={`${process.env.PUBLIC_URL}/account/user-profile-setting`}
                                                className={
                                                    window.location.pathname === `${process.env.PUBLIC_URL}/account/user-profile-setting` ? 'active' : ''
                                                }
                                            >
                                                <Icon name="lock-alt-fill"></Icon>
                                                <span>Security Settings</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="card-inner card-inner-lg">
                            {sm && mobileView && <div className="toggle-overlay" onClick={() => updateSm(!sm)}></div>}
                            <Switch>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/profile`}
                                    render={() => <UserProfileRegularPage updateSm={updateSm} sm={sm} setProfileName={setProfileName} setProfileEmail={setProfileEmail} pageTitle={pageTitle} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/user-profile-notification`}
                                    render={() => <UserProfileNotificationPage updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/user-profile-activity`}
                                    render={() => <UserProfileActivityPage updateSm={updateSm} sm={sm} />}
                                ></Route>
                                <Route
                                    exact
                                    path={`${process.env.PUBLIC_URL}/account/user-profile-setting`}
                                    render={() => <UserProfileSettingPage updateSm={updateSm} sm={sm} />}
                                ></Route>
                            </Switch>
                        </div>
                    </div>
                </Card>
            </Content>
        </>
    );
};

UserProfileLayout.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default UserProfileLayout;
