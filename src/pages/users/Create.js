import React, { useEffect, useState } from 'react';
import { Row, Col, Form } from 'reactstrap';
import { useForm, useWatch } from 'react-hook-form';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, genders, roles, statuses, token } from '../../utils/Constants';
import { showToast, generateEmployeeId, formClass } from '../../utils/Utils';
import { useTitle } from '../../contexts/TitleContext';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    BlockDes,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween
} from '../../components/Component';
import {
    Radio,
    Select2,
    Text,
    Email,
    Password,
    Tel,
    DatePicker,
    ButtonPrimary,
    ButtonSecondary
} from '../../components/form/Index';
import FileUpload from '../../components/form/FileUpload';
import PageLoader from '../../components/page-loader/PageLoader';

const Create = ({ pageTitle }) => {
    const history = useHistory();
    const { register, setValue, handleSubmit, errors, control } = useForm();
    const [totalUsers, setTotalUsers] = useState(0);
    const [files, setFiles] = useState([]);
    const [DOB, setDOB] = useState();
    const [formValues, setFormValues] = useState({
        role: '',
        status: ''
    });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [pageLoading, setPageLoading] = useState(true);

    // Get the current value of the 'gender' field
    const watchedGenderValue = useWatch({
        control, // Provide the control prop
        name: 'gender',
        defaultValue: null
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const handleDropChange = (acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })
            )
        );
    };

    const handleRemoveFile = (index) => {
        const newFiles = [...files];
        newFiles.splice(index, 1);
        setFiles(newFiles);
    };

    const fetchTotalUsers = async () => {
        try {
            const response = await axios.get(`${APP_URL}/users/total-taxes`,
                { headers: { Authorization: `Bearer ${token}` } });
            setTotalUsers(response.data.totalUsers);
            setPageLoading(false);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchTotalUsers();
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);

        const formData = new FormData();

        // Append text fields to the form data
        formData.append('name', e.name);
        formData.append('email', e.email);
        formData.append('password', e.password);
        formData.append('mobile', e.mobile);
        formData.append('companyId', '5f9d88f9d4b7a1b2c8c7e8b1');
        formData.append('employeeId', generateEmployeeId(totalUsers));
        formData.append('gender', watchedGenderValue);
        formData.append('role', formValues.role);
        formData.append('status', formValues.status);

        // Check if dob is not null or undefined
        if (DOB !== null && DOB !== undefined) {
            formData.append('dob', DOB.toISOString());
        }

        // Append file field to the form data
        // Create a Promise that resolves after the file has been appended
        const fileAppended = new Promise((resolve) => {
            // Append file field to the form data
            if (files.length > 0) {
                formData.append('image', files[0]);
            }
            resolve();
        });

        // Await the Promise before logging the final FormData
        await fileAppended;

        try {
            const response = await axios.post(`${APP_URL}/users/store`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });

            showToast('success', response.data.message, () => {
                setIsFormSubmitted(false);
                history.push(`${process.env.PUBLIC_URL}/account/users`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            setIsFormSubmitted(false);
        }
    };

    // Function to remove the preview image
    useEffect(
        () => () => {
            files.forEach((file) => URL.revokeObjectURL(file.preview));
        },
        [files]
    );

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading User Page..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <Form
                                        className={formClass}
                                        onSubmit={handleSubmit(onFormSubmit)}
                                        encType="multipart/form-data"
                                    >
                                        <Row className="g-gs">
                                            <Col lg="9">
                                                <PreviewCard>
                                                    <Row className="g-gs">
                                                        <Col md="4">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Name"
                                                                fieldName="name"
                                                                fieldId="name"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="E.g. John Doe"
                                                            />
                                                        </Col>
                                                        <Col md="4">
                                                            <Email
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Email"
                                                                fieldName="email"
                                                                fieldId="email"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="E.g. admin@example.com"
                                                            />
                                                        </Col>
                                                        <Col md="4">
                                                            <Password
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Password"
                                                                fieldName="password"
                                                                fieldId="password"
                                                                fieldRequired={true}
                                                            />
                                                        </Col>
                                                        <Col md="4">
                                                            <Tel
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Mobile Number"
                                                                fieldName="mobile"
                                                                fieldId="mobile"
                                                                fieldRequired={false}
                                                                fieldPlaceHolder="E.g. 9876543210"
                                                            />
                                                        </Col>
                                                        <Col sm="4">
                                                            <DatePicker
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="DOB"
                                                                fieldName="dob"
                                                                fieldId="dob"
                                                                fieldPlaceHolder="Select Date"
                                                                selected={DOB}
                                                                onChange={setDOB}
                                                            />
                                                        </Col>
                                                        <Col md="4">
                                                            <Radio
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Gender"
                                                                fieldName="gender"
                                                                fieldOptions={genders}
                                                                fieldValue={watchedGenderValue}
                                                                setValue={setValue}
                                                                fieldToolTip={true}
                                                                fieldToolTipContent="Gender value N/A means it's not disclosed by the user"
                                                            />
                                                        </Col>
                                                        <Col md="4">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Role"
                                                                fieldName="role"
                                                                fieldOptions={roles}
                                                                setValue={onFormChange} // Use setValue prop
                                                            />
                                                        </Col>
                                                        <Col md="4">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Status"
                                                                fieldName="status"
                                                                fieldOptions={statuses}
                                                                setValue={onFormChange} // Pass setValue function

                                                            />
                                                        </Col>
                                                        <Col md="12">
                                                            <ButtonPrimary
                                                                fieldLabel="Save"
                                                                formLoading={isFormSubmitted}
                                                                formLoadingLabel="Saving..."
                                                            />
                                                        </Col>
                                                    </Row>
                                                </PreviewCard>
                                            </Col>
                                            <Col lg="3">
                                                <PreviewCard>
                                                    <Col md="12">
                                                        <FileUpload
                                                            fieldLabel="Upload Image"
                                                            fieldName="image-dropzone"
                                                            fieldId="image-dropzone"
                                                            handleDropChange={handleDropChange}
                                                            fieldFiles={files}
                                                            handleRemoveFile={handleRemoveFile}
                                                        />
                                                    </Col>
                                                </PreviewCard>
                                            </Col>
                                        </Row>
                                    </Form>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
