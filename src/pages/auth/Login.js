import React, { useState } from 'react';
import Logo from '../../images/logo.png';
import PageContainer from '../../layout/page-container/PageContainer';
import Head from '../../layout/head/Head';
import AuthFooter from './AuthFooter';
import {
    Block,
    BlockContent,
    BlockDes,
    BlockHead,
    BlockTitle,
    Button,
    Icon,
    PreviewCard
} from '../../components/Component';
import { Form, Spinner } from 'reactstrap';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { showToast } from '../../utils/Utils';
import { APP_URL, token } from '../../utils/Constants';
import { ToastContainer } from 'react-toastify';

const Login = () => {
    const [loading, setLoading] = useState(false);
    const [passState, setPassState] = useState(false);

    const onFormSubmit = async (formData) => {
        setLoading(true);

        try {
            const response = await axios.post(`${APP_URL}/auth/login`, formData, { headers: { Authorization: `Bearer ${token}` } });
            showToast('success', response.data.message);

            localStorage.setItem('accessToken', response.data.data.token);
            localStorage.setItem('loggedInUser', JSON.stringify(response.data.data.user));
            localStorage.setItem('company', JSON.stringify(response.data.data.company));

            setTimeout(() => {
                window.history.pushState(
                    `${
                        process.env.PUBLIC_URL
                            ? process.env.PUBLIC_URL
                            : '/account/dashboard'
                    }`,
                    'login',
                    `${
                        process.env.PUBLIC_URL
                            ? process.env.PUBLIC_URL
                            : '/account/dashboard'
                    }`
                );
                window.location.reload();
            }, 1000);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setTimeout(() => {
            setLoading(false);
        }, 1000);
    };

    const { errors, register, handleSubmit } = useForm();

    return (
        <>
            <Head title="Login" />
            <PageContainer>
                <Block className="nk-block-middle nk-auth-body  wide-xs">
                    <div className="brand-logo pb-4 text-center">
                        <Link
                            to={process.env.PUBLIC_URL + '/'}
                            className="logo-link"
                        >
                            <img
                                className="logo-light logo-img logo-img-lg"
                                src={Logo}
                                alt="logo"
                            />
                            <img
                                className="logo-dark logo-img logo-img-lg"
                                src={Logo}
                                alt="logo-dark"
                            />
                        </Link>
                    </div>

                    <PreviewCard
                        className="card-bordered"
                        bodyClass="card-inner-lg"
                    >
                        <BlockHead>
                            <BlockContent>
                                <BlockTitle tag="h4">Sign-In</BlockTitle>
                                <BlockDes>
                                    <p>
                                        Access using your email and
                                        password. {process.env.REACT_APP_ENV}
                                    </p>
                                </BlockDes>
                            </BlockContent>
                        </BlockHead>
                        <Form
                            className="is-alter"
                            onSubmit={handleSubmit(onFormSubmit)}
                        >
                            <div className="form-group">
                                <div className="form-label-group">
                                    <label
                                        className="form-label"
                                        htmlFor="default-01"
                                    >
                                        Email or Username
                                    </label>
                                </div>
                                <div className="form-control-wrap">
                                    <input
                                        type="text"
                                        id="default-01"
                                        name="email"
                                        ref={register({
                                            required: 'This field is required'
                                        })}
                                        defaultValue="admin@example.com"
                                        placeholder="Enter your email address or username"
                                        className="form-control-lg form-control"
                                    />
                                    {errors.email && (
                                        <span className="invalid">
                                            {errors.email.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="form-label-group">
                                    <label
                                        className="form-label"
                                        htmlFor="password"
                                    >
                                        Password
                                    </label>
                                    <Link
                                        className="link link-primary link-sm"
                                        to={`${process.env.PUBLIC_URL}/account/auth-reset`}
                                    >
                                        Forgot Code?
                                    </Link>
                                </div>
                                <div className="form-control-wrap">
                                    <a
                                        href="#password"
                                        onClick={(ev) => {
                                            ev.preventDefault();
                                            setPassState(!passState);
                                        }}
                                        className={`form-icon lg form-icon-right passcode-switch ${
                                            passState ? 'is-hidden' : 'is-shown'
                                        }`}
                                    >
                                        <Icon
                                            name="eye"
                                            className="passcode-icon icon-show"
                                        ></Icon>

                                        <Icon
                                            name="eye-off"
                                            className="passcode-icon icon-hide"
                                        ></Icon>
                                    </a>
                                    <input
                                        type={passState ? 'text' : 'password'}
                                        id="password"
                                        name="password"
                                        defaultValue="1234567890"
                                        ref={register({
                                            required: 'This field is required'
                                        })}
                                        placeholder="Enter your password"
                                        className={`form-control-lg form-control ${
                                            passState ? 'is-hidden' : 'is-shown'
                                        }`}
                                    />
                                    {errors.password && (
                                        <span className="invalid">
                                            {errors.password.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                            <div className="form-group">
                                <Button
                                    size="lg"
                                    className="btn-block"
                                    type="submit"
                                    color="primary"
                                >
                                    {loading
                                        ? (
                                            <>
                                                <Spinner size="sm" color="light" />
                                                <span>Loading...</span>
                                            </>
                                        )
                                        : (
                                            'Sign in'
                                        )}
                                </Button>
                            </div>
                        </Form>
                        <div className="form-note-s2 text-center pt-4">
                            {' '}
                            New on our platform?{' '}
                            <Link
                                to={`${process.env.PUBLIC_URL}/account/auth-register`}
                            >
                                Create an account
                            </Link>
                        </div>
                        <div className="text-center pt-4 pb-3">
                            <h6 className="overline-title overline-title-sap">
                                <span>OR</span>
                            </h6>
                        </div>
                        <ul className="nav justify-center gx-4">
                            <li className="nav-item">
                                <a
                                    className="nav-link"
                                    href={process.env.REACT_APP_FRONT_SITE_URL}
                                    target='_blank'
                                    rel='noopener noreferrer'
                                >
                                    Go to front site
                                </a>
                            </li>
                            {/* <li className="nav-item">
                                <a
                                    className="nav-link"
                                    href="#socials"
                                    onClick={(ev) => {
                                        ev.preventDefault();
                                    }}
                                >
                                    Google
                                </a>
                            </li> */}
                        </ul>
                    </PreviewCard>
                </Block>
                <AuthFooter />
            </PageContainer>
            <ToastContainer />
        </>
    );
};
export default Login;
