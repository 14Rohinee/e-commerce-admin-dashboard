import React, { useEffect, useState } from 'react';
import {
    Block,
    Row,
    Col
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Form } from 'reactstrap';
import { APP_URL, defaultPaymentEnvironments, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import PropTypes from 'prop-types';
import {
    Select2,
    Text,
    ButtonPrimary,
    Checkbox
} from '../../../components/form/Index';

function Razorpay (props) {
    const [tab, setTab] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const [companySettings, setCompanySettings] = useState({});
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({});
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState(
        {}
    );
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const [formValues, setFormValues] = useState({
        razorpayEnvironment: ''
    });
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    useEffect(() => {
        setFormValues((prevState) => ({
            ...prevState,
            razorpayEnvironment: props.paymentGatewayEnvironment.razorpay
        }));
        setTab(props.tab);
        setCompanySettings(props.companySettings);
        setPaymentGatewayStatus(props.paymentGatewayStatus);
        setPaymentGatewayEnvironment(props.paymentGatewayEnvironment);
        // console.log(props);
    }, [props]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onCheckboxChange = (e) => {
        // give me code for checkbox change
        setPaymentGatewayStatus({
            ...paymentGatewayStatus,
            razorpay: e
        });
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);

        try {
            const response = await axios.put(
                `${APP_URL}/company-settings/update`,
                e, { headers: { Authorization: `Bearer ${token}` } }
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {/* START: Razorpay tab */}
            <div
                className={tab === '3' ? 'card-inner' : 'd-none'}
                id="razorpay"
            >
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="razorpayStatus"
                                            name="razorpayStatus"
                                            checked={
                                                paymentGatewayStatus.razorpay
                                            }
                                            onChange={(e) => {
                                                setPaymentGatewayStatus({
                                                    ...paymentGatewayStatus,
                                                    razorpay: e.target.checked
                                                });
                                            }}
                                            ref={register({
                                                required: true
                                            })}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="razorpayStatus"
                                        >
                                            Razorpay Status{' '}
                                            {paymentGatewayStatus.razorpay}
                                        </label>
                                    </div>
                                </div>
                                <Checkbox
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Razorpay Status"
                                    fieldName="razorpayStatus"
                                    fieldId="razorpayStatus"
                                    fieldValue={paymentGatewayStatus.razorpay}
                                    setValue={onCheckboxChange}
                                />
                            </Col>
                            <Col sm="12" md="12"
                                className={
                                    paymentGatewayStatus.razorpay
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Select2
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Select Environment"
                                    fieldName="razorpayEnvironment"
                                    fieldOptions={defaultPaymentEnvironments}
                                    setValue={onFormChange} // Pass setValue function
                                    fieldValue={
                                        defaultPaymentEnvironments.find(option => option.value === formValues.razorpayEnvironment)
                                    }
                                />
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                        paymentGatewayEnvironment.razorpay ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Sandbox Razorpay Client ID"
                                    fieldName="razorpaySandboxKeyId"
                                    fieldId="razorpaySandboxKeyId"
                                    fieldRequired={true}
                                    fieldValue={companySettings.razorpaySandboxKeyId}
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                        paymentGatewayEnvironment.razorpay ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Sandbox Razorpay Secret"
                                    fieldName="razorpaySandboxKeySecret"
                                    fieldId="razorpaySandboxKeySecret"
                                    fieldRequired={true}
                                    fieldValue={companySettings.razorpaySandboxKeySecret}
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                        paymentGatewayEnvironment.razorpay ===
                                        'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Live Razorpay Client ID"
                                    fieldName="razorpayLiveKeyId"
                                    fieldId="razorpayLiveKeyId"
                                    fieldRequired={true}
                                    fieldValue={companySettings.razorpayLiveKeyId}
                                />
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                        paymentGatewayEnvironment.razorpay ===
                                        'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Live Razorpay Secret"
                                    fieldName="razorpayLiveKeySecret"
                                    fieldId="razorpayLiveKeySecret"
                                    fieldRequired={true}
                                    fieldValue={companySettings.razorpayLiveKeySecret}
                                />
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <ButtonPrimary
                                        fieldLabel="Save"
                                        formLoading={isFormSubmitted}
                                        formLoadingLabel="Saving..."
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
            {/* END: Razorpay tab */}
        </>
    );
}

Razorpay.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object,
    paymentGatewayStatus: PropTypes.object,
    paymentGatewayEnvironment: PropTypes.object
};

export default Razorpay;
