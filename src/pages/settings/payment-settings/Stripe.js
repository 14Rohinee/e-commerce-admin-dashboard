import React, { useEffect, useState } from 'react';
import {
    Block,
    Row,
    Col
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Form } from 'reactstrap';
import { APP_URL, defaultPaymentEnvironments, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import PropTypes from 'prop-types';
import {
    Select2,
    Text,
    ButtonPrimary
} from '../../../components/form/Index';

function Stripe (props) {
    const [tab, setTab] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const [companySettings, setCompanySettings] = useState({});
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({});
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState(
        {}
    );
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [formValues, setFormValues] = useState({
        stripeEnvironment: ''
    });
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    useEffect(() => {
        setFormValues((prevState) => ({
            ...prevState,
            stripeEnvironment: props.paymentGatewayEnvironment.stripe
        }));
        setTab(props.tab);
        setCompanySettings(props.companySettings);
        setPaymentGatewayStatus(props.paymentGatewayStatus);
        setPaymentGatewayEnvironment(props.paymentGatewayEnvironment);
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        // console.log(e);
        try {
            const response = await axios.put(
                `${APP_URL}/company-settings/update`,
                e, { headers: { Authorization: `Bearer ${token}` } }
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {/* START: Stripe setting tab */}
            <div className={tab === '2' ? 'card-inner' : 'd-none'} id="stripe">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="stripeStatus"
                                            name="stripeStatus"
                                            checked={
                                                paymentGatewayStatus.stripe
                                            }
                                            onChange={(e) => {
                                                setPaymentGatewayStatus({
                                                    ...paymentGatewayStatus,
                                                    stripe: e.target.checked
                                                });
                                            }}
                                            ref={register({
                                                required: true
                                            })}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="stripeStatus"
                                        >
                                            Stripe Status{' '}
                                            {paymentGatewayStatus.stripe}
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col sm="12" md="12"
                                className={
                                    paymentGatewayStatus.stripe ? '' : 'd-none'
                                }
                            >
                                <Select2
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Select Environment"
                                    fieldName="stripeEnvironment"
                                    fieldOptions={defaultPaymentEnvironments}
                                    setValue={onFormChange} // Pass setValue function
                                    fieldValue={
                                        defaultPaymentEnvironments.find(option => option.value === formValues.stripeEnvironment)
                                    }
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                        paymentGatewayEnvironment.stripe ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Sandbox Stripe Client ID"
                                    fieldName="stripeSandboxPublishableKey"
                                    fieldId="stripeSandboxPublishableKey"
                                    fieldRequired={true}
                                    fieldValue={companySettings.stripeSandboxPublishableKey}
                                />
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                        paymentGatewayEnvironment.stripe ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Sandbox Stripe Secret"
                                    fieldName="stripeSandboxSecretKey"
                                    fieldId="stripeSandboxSecretKey"
                                    fieldRequired={true}
                                    fieldValue={companySettings.stripeSandboxSecretKey}
                                />
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                        paymentGatewayEnvironment.stripe === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Live Stripe Client ID"
                                    fieldName="stripeLivePublishableKey"
                                    fieldId="stripeLivePublishableKey"
                                    fieldRequired={true}
                                    fieldValue={companySettings.stripeLivePublishableKey}
                                />
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                        paymentGatewayEnvironment.stripe === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Live Stripe Secret"
                                    fieldName="stripeLiveSecretKey"
                                    fieldId="stripeLiveSecretKey"
                                    fieldRequired={true}
                                    fieldValue={companySettings.stripeLiveSecretKey}
                                />
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <ButtonPrimary
                                        fieldLabel="Save"
                                        formLoading={isFormSubmitted}
                                        formLoadingLabel="Saving..."
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
            {/* END: Stripe setting tab */}
        </>
    );
}

Stripe.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object,
    paymentGatewayStatus: PropTypes.object,
    paymentGatewayEnvironment: PropTypes.object
};

export default Stripe;
