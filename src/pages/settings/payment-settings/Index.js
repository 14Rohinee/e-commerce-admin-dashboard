import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import Paypal from './Paypal';
import Stripe from './Stripe';
import Razorpay from './Razorpay';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';
import PageLoader from '../../../components/page-loader/PageLoader';

function Index ({ pageTitle }) {
    const [tab, setTab] = useState('0');
    const [companySettings, setCompanySettings] = useState([]);
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({
        paypal: false,
        stripe: false,
        razorpay: false
    });
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState({
        paypal: '',
        stripe: '',
        razorpay: ''
    });

    const [pageLoading, setPageLoading] = useState(true);

    // Get all the languages
    const getCompanySettings = async () => {
        await axios
            .get(`${APP_URL}/company-settings/show`)
            .then((response) => {
                setCompanySettings(response.data.data);

                setPaymentGatewayStatus((prevEnvironment) => ({
                    ...prevEnvironment,
                    paypal: response.data.data.paypalStatus,
                    stripe: response.data.data.stripeStatus,
                    razorpay: response.data.data.razorpayStatus
                })
                );

                setPaymentGatewayEnvironment((prevEnvironment) => ({
                    ...prevEnvironment,
                    paypal: response.data.data.paypalEnvironment,
                    stripe: response.data.data.stripeEnvironment,
                    razorpay: response.data.data.razorpayEnvironment
                })
                );

                setTab('1');
                setPageLoading(false);
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    useEffect(() => {
        getCompanySettings();
    }, []);

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Payment Credentials..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <div className="card-aside-wrap" id="user-detail-block">
                                <div className="card-content">
                                    <ul className="nav nav-tabs nav-tabs-mb-icon nav-tabs-card">
                                        <li className="nav-item">
                                            <a
                                                className={
                                                    tab === '1' ? 'nav-link active' : 'nav-link'
                                                }
                                                href="#paypal"
                                                onClick={(ev) => {
                                                    ev.preventDefault();
                                                    setTab('1');
                                                }}
                                            >
                                                <span
                                                    className={`me-2 dot dot-lg dot-${
                                                        paymentGatewayStatus.paypal
                                                            ? 'success'
                                                            : 'danger'
                                                    }`}
                                                ></span>
                                                <span>
                                    Paypal
                                                </span>
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a
                                                className={
                                                    tab === '2' ? 'nav-link active' : 'nav-link'
                                                }
                                                href="#stripee"
                                                onClick={(ev) => {
                                                    ev.preventDefault();
                                                    setTab('2');
                                                }}
                                            >
                                                <span
                                                    className={`me-2 dot dot-lg dot-${
                                                        paymentGatewayStatus.stripe
                                                            ? 'success'
                                                            : 'danger'
                                                    }`}
                                                ></span>
                                                <span>Stripe</span>
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a
                                                className={
                                                    tab === '3' ? 'nav-link active' : 'nav-link'
                                                }
                                                href="#razorpayy"
                                                onClick={(ev) => {
                                                    ev.preventDefault();
                                                    setTab('3');
                                                }}
                                            >
                                                <span
                                                    className={`me-2 dot dot-lg dot-${
                                                        paymentGatewayStatus.razorpay
                                                            ? 'success'
                                                            : 'danger'
                                                    }`}
                                                ></span>
                                                <span>Razorpay</span>
                                            </a>
                                        </li>
                                    </ul>

                                    {tab === '1'
                                        ? (
                                            <Paypal
                                                tab={tab}
                                                paymentGatewayStatus={paymentGatewayStatus}
                                                paymentGatewayEnvironment={
                                                    paymentGatewayEnvironment
                                                }
                                                companySettings={companySettings}
                                            />
                                        )
                                        : tab === '2'
                                            ? (
                                                <Stripe
                                                    tab={tab}
                                                    paymentGatewayStatus={paymentGatewayStatus}
                                                    paymentGatewayEnvironment={
                                                        paymentGatewayEnvironment
                                                    }
                                                    companySettings={companySettings}
                                                />
                                            )
                                            : tab === '3'
                                                ? (
                                                    <Razorpay
                                                        tab={tab}
                                                        paymentGatewayStatus={paymentGatewayStatus}
                                                        paymentGatewayEnvironment={
                                                            paymentGatewayEnvironment
                                                        }
                                                        companySettings={companySettings}
                                                    />
                                                )
                                                : null}
                                </div>
                            </div>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
