import React, { useEffect, useState } from 'react';
import {
    Block,
    Row,
    Col
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { APP_URL, defaultPaymentEnvironments, token } from '../../../utils/Constants';
import { Form } from 'reactstrap';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import PropTypes from 'prop-types';
import {
    Select2,
    Text,
    ButtonPrimary
} from '../../../components/form/Index';

function Paypal (props) {
    const [tab, setTab] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const [companySettings, setCompanySettings] = useState({});
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({});
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState({});
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [formValues, setFormValues] = useState({ paypalEnvironment: '' });
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    useEffect(() => {
        setFormValues((prevState) => ({
            ...prevState,
            paypalEnvironment: props.paymentGatewayEnvironment.paypal
        }));
        setTab(props.tab);
        setCompanySettings(props.companySettings);
        setPaymentGatewayStatus(props.paymentGatewayStatus);
        setPaymentGatewayEnvironment(props.paymentGatewayEnvironment);
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        try {
            const response = await axios.put(
                `${APP_URL}/company-settings/update`,
                e, { headers: { Authorization: `Bearer ${token}` } }
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {/* START: Paypal setting tab */}
            <div className={tab === '1' ? 'card-inner' : 'd-none'} id="paypal">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="paypalStatus"
                                            name="paypalStatus"
                                            checked={paymentGatewayStatus.paypal}
                                            onChange={(e) => {
                                                setPaymentGatewayStatus({
                                                    ...paymentGatewayStatus,
                                                    paypal: e.target.checked
                                                });
                                            }}
                                            ref={register({
                                                required: true
                                            })}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="paypalStatus"
                                        >
                                            Paypal Status {paymentGatewayStatus.paypal}
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col sm="12" md="12"
                                className={
                                    paymentGatewayStatus.paypal ? '' : 'd-none'
                                }
                            >
                                <Select2
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Select Environment"
                                    fieldName="paypalEnvironment"
                                    fieldOptions={defaultPaymentEnvironments}
                                    setValue={onFormChange} // Pass setValue function
                                    fieldValue={
                                        defaultPaymentEnvironments.find(option => option.value === formValues.paypalEnvironment)
                                    }
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                        paymentGatewayEnvironment.paypal ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Sandbox Paypal Client ID"
                                    fieldName="paypalSandboxClientId"
                                    fieldId="paypalSandboxClientId"
                                    fieldRequired={true}
                                    fieldValue={companySettings.paypalSandboxClientId}
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                        paymentGatewayEnvironment.paypal ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Sandbox Paypal Secret"
                                    fieldName="paypalSandboxClientSecret"
                                    fieldId="paypalSandboxClientSecret"
                                    fieldRequired={true}
                                    fieldValue={companySettings.paypalSandboxClientSecret}
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                        paymentGatewayEnvironment.paypal === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >

                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Live Paypal Client ID"
                                    fieldName="paypalLiveClientId"
                                    fieldId="paypalLiveClientId"
                                    fieldRequired={true}
                                    fieldValue={companySettings.paypalLiveClientId}
                                />
                            </Col>
                            <Col md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                        paymentGatewayEnvironment.paypal === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Live Paypal Secret"
                                    fieldName="paypalLiveClientSecret"
                                    fieldId="paypalLiveClientSecret"
                                    fieldRequired={true}
                                    fieldValue={companySettings.paypalLiveClientSecret}
                                />
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <ButtonPrimary
                                        fieldLabel="Save"
                                        formLoading={isFormSubmitted}
                                        formLoadingLabel="Saving..."
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
            {/* END: Paypal setting tab */}
        </>
    );
}

Paypal.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object,
    paymentGatewayStatus: PropTypes.object,
    paymentGatewayEnvironment: PropTypes.object
};

export default Paypal;
