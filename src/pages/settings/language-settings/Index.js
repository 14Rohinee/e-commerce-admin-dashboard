import React, { useEffect, useState } from 'react';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon
} from '../../../components/Component';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button,
    ModalHeader,
    Modal,
    ModalBody
} from 'reactstrap';
import axios from 'axios';
import { APP_URL, capitalize, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import Swal from 'sweetalert2';
import { ToastContainer } from 'react-toastify';
import Create from './Create';
import Edit from './Edit';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';
import PageLoader from '../../../components/page-loader/PageLoader';

function Index ({ pageTitle }) {
    const [selectedLanguage, setSelectedLanguage] = useState({});
    const [languages, setLanguages] = useState([]);
    const [modal, setModal] = useState({
        create: false,
        edit: false
    });

    const [pageLoading, setPageLoading] = useState(true);

    const toggleCreateModal = () => {
        setModal((modal) => ({
            ...modal,
            create: !modal.create
        }));
    };

    const toggleEditModal = () => {
        setModal((modal) => ({
            ...modal,
            edit: !modal.edit
        }));
    };

    useEffect(() => {
        getLanguages();
    }, []);

    const getLanguages = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/languages`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setLanguages(response.data.languages);
        setPageLoading(false);
    };

    // Function to delete a category
    const onDeleteClick = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const result = await axios.delete(
                        `${APP_URL}/languages/delete/${id}`, { headers: { Authorization: `Bearer ${token}` } }
                    );
                    showToast('success', result.data.message);
                    getLanguages();
                } catch (error) {
                    showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
                }
            }
        });
    };

    const onRestoreClick = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            // text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, restore it!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const result = await axios.put(
                        `${APP_URL}/languages/restore/${id}`, { headers: { Authorization: `Bearer ${token}` } }
                    );
                    showToast('success', result.data.message);
                    getLanguages();
                } catch (error) {
                    showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
                }
            }
        });
    };

    const updateStatus = async (id, status) => {
        try {
            const result = await axios.put(
                `${APP_URL}/languages/change-status/${id}/${status}`, { headers: { Authorization: `Bearer ${token}` } }
            );

            showToast('success', result.data.message);
            getLanguages();
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    const DropdownTrans = (props) => {
        return (
            <UncontrolledDropdown>
                <DropdownToggle
                    tag="a"
                    className="text-soft dropdown-toggle btn btn-icon btn-trigger"
                >
                    <Icon name="more-h"></Icon>
                </DropdownToggle>
                <DropdownMenu end>
                    <ul className="link-list-plain">
                        <li>
                            <DropdownItem
                                tag="a"
                                href="#dropdownitem"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setSelectedLanguage(props.language);
                                    toggleEditModal();
                                }}
                            >
                                Edit
                            </DropdownItem>
                        </li>

                        {props.language.deletedAt === undefined ||
                        props.language.deletedAt === null ||
                        props.language.deletedAt === ''
                            ? (
                                <li>
                                    <DropdownItem
                                        tag="a"
                                        href="#dropdownitem"
                                        onClick={(ev) => {
                                            ev.preventDefault();
                                            onDeleteClick(props.language._id);
                                        }}
                                    >
                                    Delete
                                    </DropdownItem>
                                </li>
                            )
                            : null}

                        {props.language.deletedAt
                            ? (
                                <li>
                                    <DropdownItem
                                        tag="a"
                                        href="#dropdownitem"
                                        onClick={(ev) => {
                                            ev.preventDefault();
                                            onRestoreClick(props.language._id);
                                        }}
                                    >
                                    Restore
                                    </DropdownItem>
                                </li>
                            )
                            : null}
                    </ul>
                </DropdownMenu>
            </UncontrolledDropdown>
        );
    };

    DropdownTrans.propTypes = {
        language: PropTypes.object
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Languages..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <BlockHead size="lg" className="pb-4">
                                <BlockBetween>
                                    <BlockHeadContent>
                                        <BlockTitle tag="h4">Language Settings</BlockTitle>
                                        <BlockDes>
                                            <p>You can manage your language settings here.</p>
                                        </BlockDes>
                                    </BlockHeadContent>
                                </BlockBetween>
                            </BlockHead>

                            <Block size="sm">
                                <Button
                                    color="primary mb-2"
                                    onClick={() => {
                                        toggleCreateModal();
                                    }}
                                >
                                    <Icon name="plus"></Icon>
                                    <span>Add Language</span>
                                </Button>
                                <table className="table table-tranx is-compact">
                                    <thead>
                                        <tr className="tb-tnx-head">
                                            <th className="tb-tnx-id">
                                                <span className="">#</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Language Name</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Language Code</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Status</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Deleted At</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Action</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {languages
                                            ? (
                                                languages.map((language, index) => {
                                                    return (
                                                        <tr key={index} className="tb-tnx-item">
                                                            <td className="tb-tnx-id">
                                                                <span>{index + 1}</span>
                                                            </td>
                                                            <td className="tb-tnx-info">
                                                                <div className="tb-tnx-desc">
                                                                    <span className="title">
                                                                        {capitalize(language.name)}
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-amount is-alt">
                                                                <div className="tb-tnx-desc">
                                                                    <span className="text-uppercase">
                                                                        {language.code}
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-amount is-alt">
                                                                <div className="preview-block">
                                                                    <div className="custom-control custom-switch">
                                                                        <input
                                                                            type="checkbox"
                                                                            className="custom-control-input"
                                                                            checked={
                                                                                language.status ===
                                                            'active'
                                                                            }
                                                                            id={`id${language._id}`}
                                                                            onChange={(ev) => {
                                                                                ev.preventDefault();
                                                                                const status = ev
                                                                                    .target.checked
                                                                                    ? 'active'
                                                                                    : 'inactive';
                                                                                updateStatus(
                                                                                    language._id,
                                                                                    status
                                                                                );
                                                                            }}
                                                                        />
                                                                        <label
                                                                            className="custom-control-label"
                                                                            htmlFor={`id${language._id}`}
                                                                        ></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-info">
                                                                <div className="tb-tnx-desc">
                                                                    <span className="title">
                                                                        {
                                                                            language.deletedAt === undefined || language.deletedAt === null || language.deletedAt === '' ? '--' : language.deletedAt
                                                                        }
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-action">
                                                                <DropdownTrans
                                                                    language={language}
                                                                />
                                                            </td>
                                                        </tr>
                                                    );
                                                })
                                            )
                                            : (
                                                <tr>
                                                    <td colSpan="5" className="text-center">
                                    No data found
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </table>
                            </Block>

                            {/* START: Create Modal */}
                            <Modal
                                isOpen={modal.create}
                                toggle={toggleCreateModal}
                                className="modal-md"
                            >
                                <ModalHeader toggle={toggleCreateModal}>
                    Create Language
                                </ModalHeader>
                                <ModalBody>
                                    <Create
                                        onClose={toggleCreateModal}
                                        loadData={getLanguages}
                                    />
                                </ModalBody>
                            </Modal>
                            {/* END: Create Modal */}

                            {/* START: EDIT Modal */}
                            <Modal
                                isOpen={modal.edit}
                                toggle={toggleEditModal}
                                className="modal-md"
                            >
                                <ModalHeader toggle={toggleEditModal}>
                    Edit Language
                                </ModalHeader>
                                <ModalBody>
                                    <Edit
                                        onClose={toggleEditModal}
                                        loadData={getLanguages}
                                        data={selectedLanguage}
                                    />
                                </ModalBody>
                            </Modal>
                            {/* END: EDIT Modal */}

                            <ToastContainer />
                        </>
                    )}
        </>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
