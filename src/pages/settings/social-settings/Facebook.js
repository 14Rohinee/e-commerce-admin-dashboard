import React, { useState } from 'react';
import { Block } from '../../../components/Component';
import { Col, Form, Row } from 'reactstrap';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import axios from 'axios';
import { APP_URL, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';
import {
    Text,
    ButtonPrimary
} from '../../../components/form/Index';
import Callback from '../../../components/links/Callback';

function Facebook (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [companySettings] = useState(props.companySettings);
    const [status, setStatus] = useState(companySettings.facebookStatus === 'active');
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.facebookStatus = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(
                `${APP_URL}/social-auth-settings/update`,
                e, { headers: { Authorization: `Bearer ${token}` } }
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            <div className={tab === '2' ? 'card-inner' : 'd-none'} id="paypal">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="status"
                                            checked={status}
                                            onChange={(e) =>
                                                setStatus(e.target.checked)
                                            }
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="status"
                                        >
                                        Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'} >
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Facebook App ID"
                                    fieldName="facebookClientId"
                                    fieldId="facebookClientId"
                                    fieldRequired={true}
                                    fieldValue={companySettings.facebookClientId}
                                />
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Facebook Secret"
                                    fieldName="facebookClientSecret"
                                    fieldId="facebookClientSecret"
                                    fieldRequired={true}
                                    fieldValue={companySettings.facebookClientSecret}
                                />
                            </Col>
                            <Col md="12" className={ status ? '' : 'd-none'}>
                                <Callback
                                    fieldLabel="Callback"
                                    fieldId="callback"
                                    fieldName="facebook"
                                />
                            </Col>
                            <Col md="12">
                                <ButtonPrimary
                                    fieldLabel="Save"
                                    formLoading={isFormSubmitted}
                                    formLoadingLabel="Saving..."
                                />
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </>
    );
}

Facebook.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object
};

export default Facebook;
