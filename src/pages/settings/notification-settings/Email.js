import React, { useState, useEffect } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Form, Modal, ModalBody } from 'reactstrap';
import axios from 'axios';
import { APP_URL, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';
import {
    Select2,
    Text,
    ButtonPrimary
} from '../../../components/form/Index';

function Email (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [settings] = useState(props.settings.email);
    const [status, setStatus] = useState(props.settings.email.status === 'active');
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [formValues, setFormValues] = useState({ driver: '', encryption: '' });
    const [modal, setModal] = useState(false);
    const [emailFrom, setEmailFrom] = useState('');

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const mailDrivers = [
        { value: 'mail', label: 'Mail' },
        { value: 'smtp', label: 'SMTP' }
    ];

    const mailEncryptions = [
        { value: 'ssl', label: 'ssl' },
        { value: 'tls', label: 'tls' },
        { value: 'none', label: 'none' }
    ];

    const getCompanyDetails = async () => {
        await axios
            .get(`${APP_URL}/email-settings/show`)
            .then((response) => {
                setFormValues((prevState) => ({
                    ...prevState,
                    driver: response.data.data.emailSetting.driver,
                    encryption: response.data.data.emailSetting.encryption
                }));
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    useEffect(() => {
        getCompanyDetails();
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.status = status ? 'active' : 'inactive';
        e.driver = formValues.driver;
        e.encryption = formValues.encryption;
        try {
            const response = await axios.put(`${APP_URL}/email-settings/update`, e, { headers: { Authorization: `Bearer ${token}` } });
            showToast('success', response.data.message, () => {
                window.location.reload();
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    const sendTestEmail = async () => {
        try {
            const response = await axios.post(`${APP_URL}/email-settings/send-test-email`, { email: emailFrom }, { headers: { Authorization: `Bearer ${token}` } });
            showToast('success', response.data.message);
            setModal(false);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            <div className={tab === '1' ? 'card-inner' : 'd-none'} id="email">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="status"
                                            onChange={() => setStatus(!status)}
                                            name="status"
                                            checked={status}
                                            defaultValue={status ? 'active' : 'inactive'}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="status"
                                        >
                                            Email Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail From Name"
                                    fieldName="mailFromName"
                                    fieldId="mailFromName"
                                    fieldRequired={true}
                                    fieldValue={settings.mailFromName}
                                />
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail From Address"
                                    fieldName="mailFromAddress"
                                    fieldId="mailFromAddress"
                                    fieldRequired={true}
                                    fieldValue={settings.mailFromAddress}
                                />
                            </Col>
                            <Col md="12" className={status ? '' : 'd-none'}>
                                <Select2
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail Driver"
                                    fieldName="driver"
                                    fieldOptions={mailDrivers}
                                    setValue={onFormChange} // Pass setValue function
                                    fieldValue={
                                        mailDrivers.find(option => option.value === formValues.driver)
                                    }
                                />
                            </Col>
                            <Col md="12" className={status ? '' : 'd-none'}>
                                <Select2
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail Encryption"
                                    fieldName="encryption"
                                    fieldOptions={mailEncryptions}
                                    setValue={onFormChange} // Pass setValue function
                                    fieldValue={
                                        mailEncryptions.find(option => option.value === formValues.encryption)
                                    }
                                />
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail Host"
                                    fieldName="host"
                                    fieldId="host"
                                    fieldRequired={true}
                                    fieldValue={settings.host}
                                />
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail Port"
                                    fieldName="port"
                                    fieldId="port"
                                    fieldRequired={true}
                                    fieldValue={settings.port}
                                />
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail Username"
                                    fieldName="username"
                                    fieldId="username"
                                    fieldRequired={true}
                                    fieldValue={settings.username}
                                />
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="Mail Password"
                                    fieldName="password"
                                    fieldId="password"
                                    fieldRequired={true}
                                    fieldValue={settings.password}
                                />
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <ButtonPrimary
                                        fieldLabel="Save"
                                        formLoading={isFormSubmitted}
                                        formLoadingLabel="Saving..."
                                    />
                                    <a
                                        href="#export"
                                        onClick={(ev) => {
                                            ev.preventDefault();
                                            setModal(true);
                                        }}
                                        className={status ? 'btn btn-white btn-outline-light ml-3' : 'd-none'}
                                    >
                                        <Icon name="send"></Icon>
                                        <span>Send Test Email</span>
                                    </a>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>

            {/* Start: Send test email */}
            <Modal isOpen={modal} className="modal-dialog-centered" size="lg" toggle={() => setModal(false)}>
                <a
                    href="#dropdownitem"
                    onClick={(ev) => {
                        ev.preventDefault();
                        setModal(false);
                    }}
                    className="close"
                >
                    <Icon name="cross-sm"></Icon>
                </a>
                <ModalBody>
                    <div className="p-2">
                        <h5 className="title">
                            Send Test Email
                        </h5>

                        <Row className="gy-4">
                            <Col md="12">
                                <div className="form-group">
                                    <label className="form-label" htmlFor="full-name">
                                        Email From
                                    </label>
                                    <input
                                        type="text"
                                        id="emailFrom"
                                        name="emailFrom"
                                        className="form-control"
                                        placeholder="Enter the email address"
                                        defaultValue={emailFrom}
                                        onChange={(ev) => setEmailFrom(ev.target.value)}
                                    />
                                </div>
                            </Col>
                            <Col size="12">
                                <ul className="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <Button
                                            color="primary"
                                            size="lg"
                                            onClick={(ev) => {
                                                ev.preventDefault();
                                                sendTestEmail();
                                            }}
                                        >
                                            Send
                                        </Button>
                                    </li>
                                    <li>
                                        <a
                                            href="#dropdownitem"
                                            onClick={(ev) => {
                                                ev.preventDefault();
                                                setModal(false);
                                            }}
                                            className="link link-light"
                                        >
                                            Cancel
                                        </a>
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </div>
                </ModalBody>
            </Modal>
            {/* End: Send test email */}
        </>
    );
}

Email.propTypes = {
    tab: PropTypes.string,
    settings: PropTypes.object
};

export default Email;
