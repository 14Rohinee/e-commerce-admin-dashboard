import React, { useState } from 'react';
import {
    Block,
    Row,
    Col
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Form } from 'reactstrap';
import axios from 'axios';
import { APP_URL, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';
import {
    Text,
    ButtonPrimary
} from '../../../components/form/Index';

function PushNotification (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [settings] = useState(props.settings.pushNotification);
    const [status, setStatus] = useState(
        props.settings.pushNotification.status === 'active'
    );

    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.status = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(
                `${APP_URL}/push-notification-settings/update`,
                e, { headers: { Authorization: `Bearer ${token}` } }
            );
            showToast('success', response.data.message, () => {
                window.location.reload();
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            <div className={tab === '3' ? 'card-inner' : 'd-none'} id="email">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="customCheck1"
                                            checked={status}
                                            onChange={(e) =>
                                                setStatus(e.target.checked)
                                            }
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="customCheck1"
                                        >
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="One Signal App ID"
                                    fieldName="onesignalAppId"
                                    fieldId="onesignalAppId"
                                    fieldRequired={true}
                                    fieldValue={settings.onesignalAppId}
                                />
                            </Col>
                            <Col md="6" className={status ? '' : 'd-none'}>
                                <Text
                                    register={register}
                                    errors={errors}
                                    fieldLabel="One Signal Rest API Key"
                                    fieldName="onesignalApiKey"
                                    fieldId="onesignalApiKey"
                                    fieldRequired={true}
                                    fieldValue={settings.onesignalApiKey}
                                />
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <ButtonPrimary
                                        fieldLabel="Save"
                                        formLoading={isFormSubmitted}
                                        formLoadingLabel="Saving..."
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </>
    );
}

PushNotification.propTypes = {
    settings: PropTypes.object,
    tab: PropTypes.string
};

export default PushNotification;
