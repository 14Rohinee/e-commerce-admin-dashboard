import React, { useState, useEffect } from 'react';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';
import FileUpload from '../../../components/form/FileUpload';
import {
    Text,
    Email,
    TextArea,
    Tel,
    ButtonPrimary
} from '../../../components/form/Index';
import PageLoader from '../../../components/page-loader/PageLoader';

function Index ({ pageTitle }) {
    const [sm, updateSm] = useState(false);
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });
    const [pageLoading, setPageLoading] = useState(true);

    const [data, setData] = useState([]);
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [files, setFiles] = useState([]);
    const [image, setImage] = useState(null);
    const [longDescriptionText, setLongDescriptionText] = useState('');

    const getCompanyDetails = async () => {
        await axios
            .get(`${APP_URL}/companies/show`)
            .then((response) => {
                setData(response.data.data);

                if (response.data.data.logoName && response.data.data.logoPath) {
                    setImage(`${response.data.data.logoPath}/companies/${response.data.data.logoName}`);
                }
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });

        setPageLoading(false);
    };

    useEffect(() => {
        getCompanyDetails();
    }, []);

    const handleDropChange = (acceptedFiles) => {
        const updatedFiles = acceptedFiles.map((file) => {
            return Object.assign(file, {
                preview: URL.createObjectURL(file)
            });
        });
        setFiles(updatedFiles);
    };

    const handleRemoveFile = (index) => {
        setImage(null);

        if (index !== null) {
            const newFiles = [...files];
            newFiles.splice(index, 1);
            setFiles(newFiles);
        }
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.address = longDescriptionText;

        if (files.length > 0) {
            e.image = files[0];
        }

        try {
            const response = await axios.put(`${APP_URL}/companies/update`, e, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });
            showToast('success', response.data.message, () => {
                window.location.reload();
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Company Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <BlockHead size="lg">
                                <BlockBetween>
                                    <BlockHeadContent>
                                        <BlockTitle tag="h4">Company Settings</BlockTitle>
                                        <BlockDes>
                                            <p>
                                Basic info, like company name, email, phone, and
                                contact
                                            </p>
                                        </BlockDes>
                                    </BlockHeadContent>
                                    <BlockHeadContent className="align-self-start d-lg-none">
                                        <Button
                                            className={`toggle btn btn-icon btn-trigger mt-n1 ${sm ? 'active' : ''
                                            }`}
                                            onClick={() => updateSm(!sm)}
                                        >
                                            <Icon name="menu-alt-r"></Icon>
                                        </Button>
                                    </BlockHeadContent>
                                </BlockBetween>
                            </BlockHead>

                            <Block size="sm">
                                <Form
                                    className={formClass}
                                    onSubmit={handleSubmit(onFormSubmit)}
                                    encType="multipart/form-data"
                                >
                                    <Row className="g-gs">
                                        <Col md="6">
                                            <Text
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Company Name"
                                                fieldName="name"
                                                fieldId="name"
                                                fieldRequired={true}
                                                fieldValue={data.name}
                                                fieldPlaceHolder="e.g. Apple Inc."
                                            />
                                        </Col>
                                        <Col md="6">
                                            <Email
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Company Email"
                                                fieldName="email"
                                                fieldId="email"
                                                fieldRequired={true}
                                                fieldValue={data.email}
                                                fieldPlaceHolder="E.g. admin@example.com"
                                            />
                                        </Col>
                                        <Col md="6">
                                            <Tel
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Company Phone"
                                                fieldName="phone"
                                                fieldId="phone"
                                                fieldRequired={true}
                                                fieldValue={data.phone}
                                                fieldPlaceHolder="E.g. 9876543210"
                                            />
                                        </Col>
                                        <Col md="6">
                                            <Text
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Company Website"
                                                fieldName="website"
                                                fieldId="website"
                                                fieldRequired={false}
                                                fieldValue={data.website}
                                                fieldPlaceHolder="e.g. https://example.com"
                                            />
                                        </Col>
                                        <Col md="12">
                                            <TextArea
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Company Address"
                                                fieldName="address"
                                                fieldId="address"
                                                fieldRequired={false}
                                                fieldValue={data.address}
                                                fieldPlaceHolder="e.g. Near Statue of Liberty, New York, USA"
                                                onChange={setLongDescriptionText}
                                            />
                                        </Col>
                                        <Col lg="12">
                                            <Col md="12">
                                                <FileUpload
                                                    fieldLabel="Company Logo"
                                                    fieldName="image"
                                                    fieldId="image"
                                                    handleDropChange={handleDropChange}
                                                    handleRemoveFile={handleRemoveFile}
                                                    fieldFiles={files}
                                                    fieldDefaultFile={image}
                                                />
                                            </Col>
                                        </Col>
                                        <Col md="12">
                                            <ButtonPrimary
                                                fieldLabel="Save"
                                                formLoading={isFormSubmitted}
                                                formLoadingLabel="Saving..."
                                            />
                                        </Col>
                                    </Row>
                                </Form>
                            </Block>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
