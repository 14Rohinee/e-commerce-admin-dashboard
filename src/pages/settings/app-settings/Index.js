import React, { useState, useEffect } from 'react';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Row,
    Col
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import {
    APP_URL,
    dateFormats,
    timeFormats,
    token
} from '../../../utils/Constants';
import {
    showToast
} from '../../../utils/Utils';
import moment from 'moment-timezone';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';
import { Select2, ButtonPrimary } from '../../../components/form/Index';
import PageLoader from '../../../components/page-loader/PageLoader';

function Index ({ pageTitle }) {
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const [pageLoading, setPageLoading] = useState(true);
    const [languages, setLanguages] = useState([]);
    const [allTimeZones, setAllTimeZones] = useState(moment.tz.names());
    const [formValues, setFormValues] = useState({
        dateFormat: '',
        timeFormat: '',
        timeZone: '',
        language: ''
    });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const getCompanyDetails = async () => {
        await axios
            .get(`${APP_URL}/companies/show`)
            .then((response) => {
                setAllTimeZones(allTimeZones.map((timeZone) => {
                    return {
                        label: timeZone,
                        value: timeZone
                    };
                }
                ));
                setFormValues((prevState) => ({
                    ...prevState,
                    dateFormat: response.data.data.dateFormat,
                    timeFormat: response.data.data.timeFormat,
                    timeZone: response.data.data.timeZone,
                    language: response.data.data.language
                }));
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    // Get all the languages
    const getLanguages = async () => {
        await axios
            .get(`${APP_URL}/languages`)
            .then((response) => {
                const language = response.data.languages.map((language) => {
                    return {
                        label: language.name,
                        value: language.code
                    };
                });
                setLanguages(language);
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    useEffect(() => {
        const fetchData = async () => {
            await Promise.all([getCompanyDetails(), getLanguages()]);
            setPageLoading(false);
        };

        fetchData();
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.dateFormat = formValues.dateFormat;
        e.timeFormat = formValues.timeFormat;
        e.timeZone = formValues.timeZone;
        e.language = formValues.language;
        try {
            const response = await axios.put(`${APP_URL}/companies/update`, e, { headers: { Authorization: `Bearer ${token}` } });
            showToast('success', response.data.message, () => {
                window.location.reload();
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading App Settings..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <BlockHead size="lg">
                                <BlockBetween>
                                    <BlockHeadContent>
                                        <BlockTitle tag="h4">App Settings</BlockTitle>
                                        <BlockDes>
                                            <p>
                                Basic info, like time zone, language, date
                                format, and time format
                                            </p>
                                        </BlockDes>
                                    </BlockHeadContent>
                                </BlockBetween>
                            </BlockHead>

                            <Block size="sm">
                                <Form
                                    className={formClass}
                                    onSubmit={handleSubmit(onFormSubmit)}
                                >
                                    <Row className="g-gs">
                                        <Col md="3">
                                            <Select2
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Date Format"
                                                fieldName="dateFormat"
                                                fieldOptions={dateFormats}
                                                setValue={onFormChange} // Pass setValue function
                                                fieldValue={
                                                    dateFormats.find(option => option.value === formValues.dateFormat)
                                                }
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Select2
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Time Format"
                                                fieldName="timeFormat"
                                                fieldOptions={timeFormats}
                                                setValue={onFormChange} // Pass setValue function
                                                fieldValue={
                                                    timeFormats.find(option => option.value === formValues.timeFormat)
                                                }
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Select2
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Default Time Zone"
                                                fieldName="timeZone"
                                                fieldOptions={allTimeZones}
                                                setValue={onFormChange} // Pass setValue function
                                                fieldValue={
                                                    allTimeZones.find(option => option.value === formValues.timeZone)
                                                }
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Select2
                                                register={register}
                                                errors={errors}
                                                fieldLabel="Default Language"
                                                fieldName="language"
                                                fieldOptions={languages}
                                                setValue={onFormChange} // Pass setValue function
                                                fieldValue={
                                                    languages.find(option => option.value === formValues.language)
                                                }
                                            />
                                        </Col>
                                        <Col md="12">
                                            <ButtonPrimary
                                                fieldLabel="Save"
                                                formLoading={isFormSubmitted}
                                                formLoadingLabel="Saving..."
                                            />
                                        </Col>
                                    </Row>
                                </Form>
                            </Block>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
