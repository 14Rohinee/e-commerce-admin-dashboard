import React, { useState } from 'react';
import { Col, Row, Form } from 'reactstrap';
import axios from 'axios';
import { APP_URL, token, statuses } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import { ToastContainer } from 'react-toastify';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {
    Text,
    Select2,
    ButtonPrimary
} from '../../../components/form/Index';

function Create (props) {
    const { errors, register, handleSubmit } = useForm();
    const [formValues, setFormValues] = useState({ status: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;

        try {
            const response = await axios.post(`${APP_URL}/taxes/store`, e, { headers: { Authorization: `Bearer ${token}` } });

            showToast('success', response.data.message);
            props.loadData(); // Reload the data
            props.onClose(); // Close the modal
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <Form className={formClass} onSubmit={handleSubmit(onFormSubmit)}>
            <Row className="g-gs">
                <Col md="4">
                    <Text
                        register={register}
                        errors={errors}
                        fieldLabel="Name"
                        fieldName="name"
                        fieldId="name"
                        fieldRequired={true}
                        fieldPlaceHolder="E.g. CGST"
                    />
                </Col>
                <Col md="4">
                    <Text
                        register={register}
                        errors={errors}
                        fieldLabel="Rate"
                        fieldName="rate"
                        fieldId="rate"
                        fieldRequired={true}
                    />
                </Col>
                <Col md="4">
                    <Select2
                        register={register}
                        errors={errors}
                        fieldLabel="Status"
                        fieldName="status"
                        fieldOptions={statuses}
                        setValue={onFormChange} // Pass setValue function

                    />
                </Col>
                <Col md="12">
                    <div className="form-group">
                        <ButtonPrimary
                            fieldLabel="Save"
                            formLoading={isFormSubmitted}
                            formLoadingLabel="Saving..."
                        />
                    </div>
                </Col>
            </Row>
            <ToastContainer />
        </Form>
    );
}

Create.propTypes = {
    loadData: PropTypes.func,
    onClose: PropTypes.func
};

export default Create;
