import React, { useEffect, useState } from 'react';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon
} from '../../../components/Component';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button,
    ModalHeader,
    Modal,
    ModalBody
} from 'reactstrap';
import axios from 'axios';
import { APP_URL, capitalize, token } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import Swal from 'sweetalert2';
import { ToastContainer } from 'react-toastify';
import Create from './Create';
import Edit from './Edit';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';
import PageLoader from '../../../components/page-loader/PageLoader';

function Index ({ pageTitle }) {
    const [selectedTax, setSelectedTax] = useState({});
    const [pageLoading, setPageLoading] = useState(true);

    const DropdownTrans = props => {
        const { tax } = props;

        return (
            <UncontrolledDropdown>
                <DropdownToggle
                    tag="a"
                    className="text-soft dropdown-toggle btn btn-icon btn-trigger"
                >
                    <Icon name="more-h"></Icon>
                </DropdownToggle>
                <DropdownMenu end>
                    <ul className="link-list-plain">
                        <li>
                            <DropdownItem
                                tag="a"
                                href="#dropdownitem"
                                onClick={ev => {
                                    ev.preventDefault();
                                    setSelectedTax(tax);
                                    toggleEditModal();
                                }}
                            >
                Edit
                            </DropdownItem>
                        </li>

                        {tax.deletedAt === undefined ||
            tax.deletedAt === null ||
            tax.deletedAt === ''
                            ? (
                                <li>
                                    <DropdownItem
                                        tag="a"
                                        href="#dropdownitem"
                                        onClick={ev => {
                                            ev.preventDefault();
                                            onDeleteClick(tax._id);
                                        }}
                                    >
                        Delete
                                    </DropdownItem>
                                </li>
                            )
                            : null}

                        {tax.deletedAt
                            ? (
                                <li>
                                    <DropdownItem
                                        tag="a"
                                        href="#dropdownitem"
                                        onClick={ev => {
                                            ev.preventDefault();
                                            onRestoreClick(tax._id);
                                        }}
                                    >
                        Restore
                                    </DropdownItem>
                                </li>
                            )
                            : null}
                    </ul>
                </DropdownMenu>
            </UncontrolledDropdown>
        );
    };

    DropdownTrans.propTypes = {
        tax: PropTypes.shape({
            _id: PropTypes.string.isRequired,
            deletedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)])
        }).isRequired
    };

    // get tax list from api
    const [taxes, setTaxes] = useState([]);
    const [modal, setModal] = useState({
        create: false,
        edit: false
    });

    const toggleCreateModal = () => {
        setModal(modal => ({
            ...modal,
            create: !modal.create
        }));
    };

    const toggleEditModal = () => {
        setModal(modal => ({
            ...modal,
            edit: !modal.edit
        }));
    };

    useEffect(() => {
        getTaxes();
    }, []);

    const getTaxes = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/taxes`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setTaxes(response.data.taxes);
        setPageLoading(false);
    };

    // Function to delete a category
    const onDeleteClick = id => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(async result => {
            if (result.isConfirmed) {
                try {
                    const result = await axios.delete(
                        `${APP_URL}/taxes/delete/${id}`, { headers: { Authorization: `Bearer ${token}` } }
                    );
                    showToast('success', result.data.message);
                    getTaxes();
                } catch (error) {
                    showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
                }
            }
        });
    };

    const onRestoreClick = id => {
        Swal.fire({
            title: 'Are you sure?',
            // text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, restore it!'
        }).then(async result => {
            if (result.isConfirmed) {
                try {
                    const result = await axios.put(
                        `${APP_URL}/taxes/restore/${id}`, { headers: { Authorization: `Bearer ${token}` } }
                    );
                    showToast('success', result.data.message);
                    getTaxes();
                } catch (error) {
                    showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
                }
            }
        });
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Taxes..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <BlockHead size="lg" className="pb-4">
                                <BlockBetween>
                                    <BlockHeadContent>
                                        <BlockTitle tag="h4">Tax Settings</BlockTitle>
                                        <BlockDes>
                                            <p>Manage taxes from here. Create new tax, edit or delete existing taxes.
                                                <span className="text-danger">
                                                    {' '}
                                                    <b>Note:</b> All these taxes will be used to calculate tax on invoices.
                                                </span>
                                            </p>
                                        </BlockDes>
                                    </BlockHeadContent>
                                </BlockBetween>
                            </BlockHead>

                            <Block size="sm">
                                <Button
                                    color="primary mb-2"
                                    onClick={() => {
                                        toggleCreateModal();
                                    }}
                                >
                                    <Icon name="plus"></Icon>
                                    <span>Add Tax</span>
                                </Button>
                                <table className="table table-tranx is-compact">
                                    <thead>
                                        <tr className="tb-tnx-head">
                                            <th className="tb-tnx-id">
                                                <span className="">#</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Tax Name</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Rate</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Status</span>
                                            </th>
                                            <th className="tb-tnx-id">
                                                <span className="">Action</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {taxes
                                            ? (
                                                taxes.map((tax, index) => {
                                                    return (
                                                        <tr key={tax._id} className="tb-tnx-item">
                                                            <td className="tb-tnx-info">
                                                                <div className="tb-tnx-desc">
                                                                    <span className="title">
                                                                        {index + 1}
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-info">
                                                                <div className="tb-tnx-desc">
                                                                    <span className="title">
                                                                        {tax.name}
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-amount is-alt">
                                                                <div className="tb-tnx-total">
                                                                    <span className="amount">
                                                                        {tax.rate}%
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-amount is-alt">
                                                                <div className="tb-tnx-total">
                                                                    <span
                                                                        className={`tb-status text-${tax.deletedAt ===
                                                                undefined ||
                                                                tax.deletedAt ===
                                                                null ||
                                                                tax.deletedAt === ''
                                                                            ? tax.status ===
                                                                    'active'
                                                                                ? 'success'
                                                                                : 'danger'
                                                                            : 'danger'
                                                                        }`}
                                                                    >
                                                                        {capitalize(
                                                                            tax.deletedAt ===
                                                                undefined ||
                                                                tax.deletedAt ===
                                                                null
                                                                                ? tax.status
                                                                                : 'Deleted'
                                                                        )}
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td className="tb-tnx-action">
                                                                <DropdownTrans tax={tax} />
                                                            </td>
                                                        </tr>
                                                    );
                                                })
                                            )
                                            : (
                                                <tr>
                                                    <td colSpan="5" className="text-center">
                                        No data found
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </table>
                            </Block>

                            {/* START: Create Modal */}
                            <Modal
                                isOpen={modal.create}
                                toggle={toggleCreateModal}
                                className="modal-md"
                            >
                                <ModalHeader toggle={toggleCreateModal}>Create Tax</ModalHeader>
                                <ModalBody>
                                    <Create onClose={toggleCreateModal} loadData={getTaxes} />
                                </ModalBody>
                            </Modal>
                            {/* END: Create Modal */}

                            {/* START: EDIT Modal */}
                            <Modal
                                isOpen={modal.edit}
                                toggle={toggleEditModal}
                                className="modal-md"
                            >
                                <ModalHeader toggle={toggleEditModal}>Edit Tax</ModalHeader>
                                <ModalBody>
                                    <Edit
                                        onClose={toggleEditModal}
                                        loadData={getTaxes}
                                        data={selectedTax}
                                    />
                                </ModalBody>
                            </Modal>
                            {/* END: EDIT Modal */}

                            <ToastContainer />
                        </>
                    )}
        </>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
