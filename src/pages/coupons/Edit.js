import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory, useParams } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, discountTypes, statuses, token } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import {
    Select2,
    Text,
    ButtonSecondary,
    ButtonPrimary,
    DatePicker
} from '../../components/form/Index';
import PageLoader from '../../components/page-loader/PageLoader';

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [data, setData] = useState({});
    const [formValues, setFormValues] = useState({ status: '', discountType: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [fromDate, setFromDate] = useState();
    const [toDate, setToDate] = useState();
    const [pageLoading, setPageLoading] = useState(true);

    // Category id from params
    const { id } = useParams();

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const getCoupon = async () => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/coupons/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } }
            );

            setFormValues((prevState) => ({
                ...prevState,
                status: response.data.data.coupon.status,
                discountType: response.data.data.coupon.discountType
            }));

            // Check if fromDate is not null or undefined
            if (response.data.data.coupon.fromDate !== null && response.data.data.coupon.fromDate !== undefined) {
                setFromDate(new Date(response.data.data.coupon.fromDate));
            }

            // Check if toDate is not null or undefined
            if (response.data.data.coupon.toDate !== null && response.data.data.coupon.toDate !== undefined) {
                setToDate(new Date(response.data.data.coupon.toDate));
            }
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.data.coupon);
        setPageLoading(false);
    };

    useEffect(() => {
        // Fetching data from api
        getCoupon();
    }, []);

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.discountType = formValues.discountType;

        // Check if fromDate is not null or undefined
        if (fromDate !== null && fromDate !== undefined) {
            e.fromDate = fromDate.toISOString();
        }

        // Check if toDate is not null or undefined
        if (toDate !== null && toDate !== undefined) {
            e.toDate = toDate.toISOString();
        }

        try {
            const response = await axios.put(`${APP_URL}/coupons/update/${id}`, e, { headers: { Authorization: `Bearer ${token}` } });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/coupons`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Category Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <PreviewCard>
                                        <Form
                                            className={formClass}
                                            onSubmit={handleSubmit(onFormSubmit)}
                                        >
                                            <Row className="g-gs">
                                                <Col md="4">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Coupon Code"
                                                        fieldName="code"
                                                        fieldId="code"
                                                        fieldRequired={true}
                                                        fieldPlaceHolder="e.g. 10OFF"
                                                        fieldValue={data.code}
                                                    />
                                                </Col>

                                                <Col md="4">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Discount"
                                                        fieldName="discount"
                                                        fieldId="discount"
                                                        fieldRequired={true}
                                                        fieldPlaceHolder="e.g. 10"
                                                        fieldValue={data.discount}
                                                    />
                                                </Col>

                                                <Col md="4">
                                                    <Select2
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Discount Type"
                                                        fieldName="discountType"
                                                        fieldOptions={discountTypes}
                                                        setValue={onFormChange}
                                                        fieldValue={
                                                            discountTypes.find(option =>
                                                                option.value === formValues.discountType)
                                                        }
                                                    />
                                                </Col>

                                                <Col md="4">
                                                    <Select2
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Status"
                                                        fieldName="status"
                                                        fieldOptions={statuses}
                                                        setValue={onFormChange}
                                                        fieldValue={
                                                            statuses.find(option => option.value === formValues.status)
                                                        }
                                                    />
                                                </Col>
                                                <Col md="4">
                                                    <DatePicker
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="From Date"
                                                        fieldName="fromDate"
                                                        fieldId="fromDate"
                                                        fieldPlaceHolder="Select Date"
                                                        selected={fromDate}
                                                        onChange={setFromDate}
                                                    />
                                                </Col>
                                                <Col md="4">
                                                    <DatePicker
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="To Date"
                                                        fieldName="toDate"
                                                        fieldId="toDate"
                                                        fieldPlaceHolder="Select Date"
                                                        selected={toDate}
                                                        onChange={setToDate}
                                                    />
                                                </Col>

                                                <Col md="12">
                                                    <div className="form-group">
                                                        <ButtonPrimary
                                                            fieldLabel="Save"
                                                            formLoading={isFormSubmitted}
                                                            formLoadingLabel="Saving..."
                                                        />
                                                    </div>
                                                </Col>
                                            </Row>
                                        </Form>
                                    </PreviewCard>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
