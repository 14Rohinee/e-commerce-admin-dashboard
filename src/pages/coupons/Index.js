import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import { APP_URL, token, exportData } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import PageLoader from '../../components/page-loader/PageLoader';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import CouponDatatable from '../../components/data-table/pages/CouponDatatable';
import ButtonPrimary from '../../components/form/ButtonPrimary';

const Index = ({ pageTitle }) => {
    const [originalData, setOriginalData] = useState([]);
    const [data, setData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    const { setDocumentTitle } = useTitle();
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the coupons
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/coupons`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.coupons);
        setOriginalData(response.data.coupons);
        setTotal(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the coupons on page load
    useEffect(() => {
        getData();
    }, []);

    // Function to export the data
    const exportCoupons = () => {
        const fieldName = ['Code', 'Discount', 'Status'];

        // fileName, title, fieldName, data
        exportData('coupons.csv', 'Coupons', fieldName, data);
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Coupons..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li className="nk-block-tools-opt">
                                                    <ButtonPrimary
                                                        fieldLabel={pageTitle}
                                                        icon="plus"
                                                        redirectUrl={`${process.env.PUBLIC_URL}/account/brands/create`} />
                                                </li>
                                                <li>
                                                    <button type='button' className="btn btn-white btn-outline-light"
                                                        onClick={() => {
                                                            exportCoupons();
                                                        } }>
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <CouponDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
