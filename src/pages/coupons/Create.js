import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, discountTypes, statuses, token } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import {
    Select2,
    Text,
    ButtonSecondary,
    ButtonPrimary,
    DatePicker
} from '../../components/form/Index';

const Create = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [formValues, setFormValues] = useState({ status: '', discountType: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [fromDate, setFromDate] = useState();
    const [toDate, setToDate] = useState();

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.discountType = formValues.discountType;

        // Check if fromDate is not null or undefined
        if (fromDate !== null && fromDate !== undefined) {
            e.fromDate = fromDate.toISOString();
        }

        // Check if toDate is not null or undefined
        if (toDate !== null && toDate !== undefined) {
            e.toDate = toDate.toISOString();
        }

        try {
            const response = await axios.post(`${APP_URL}/coupons/store`, e, { headers: { Authorization: `Bearer ${token}` } });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/coupons`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            <Head title={pageTitle}></Head>
            <Content page="component" CardSize="xl">
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent>
                            <BlockDes className="text-soft"></BlockDes>
                        </BlockHeadContent>
                        <BlockHeadContent>
                            <ButtonSecondary
                                fieldLabel="Back"
                                icon="arrow-left"
                                redirectUrl="go-back"
                            />
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block size="sm">
                    <PreviewCard>
                        <Form
                            className={formClass}
                            onSubmit={handleSubmit(onFormSubmit)}
                        >
                            <Row className="g-gs">
                                <Col md="4">
                                    <Text
                                        register={register}
                                        errors={errors}
                                        fieldLabel="Coupon Code"
                                        fieldName="code"
                                        fieldId="code"
                                        fieldRequired={true}
                                        fieldPlaceHolder="e.g. 10OFF"
                                    />
                                </Col>

                                <Col md="4">
                                    <Text
                                        register={register}
                                        errors={errors}
                                        fieldLabel="Discount"
                                        fieldName="discount"
                                        fieldId="discount"
                                        fieldRequired={true}
                                        fieldPlaceHolder="e.g. 10"
                                    />
                                </Col>

                                <Col md="4">
                                    <Select2
                                        register={register}
                                        errors={errors}
                                        fieldLabel="Discount Type"
                                        fieldName="discountType"
                                        fieldOptions={discountTypes}
                                        setValue={onFormChange}
                                    />
                                </Col>

                                <Col md="4">
                                    <Select2
                                        register={register}
                                        errors={errors}
                                        fieldLabel="Status"
                                        fieldName="status"
                                        fieldOptions={statuses}
                                        setValue={onFormChange}
                                    />
                                </Col>
                                <Col md="4">
                                    <DatePicker
                                        register={register}
                                        errors={errors}
                                        fieldLabel="From Date"
                                        fieldName="fromDate"
                                        fieldId="fromDate"
                                        fieldPlaceHolder="Select Date"
                                        selected={fromDate}
                                        onChange={setFromDate}
                                    />
                                </Col>
                                <Col md="4">
                                    <DatePicker
                                        register={register}
                                        errors={errors}
                                        fieldLabel="To Date"
                                        fieldName="toDate"
                                        fieldId="toDate"
                                        fieldPlaceHolder="Select Date"
                                        selected={toDate}
                                        onChange={setToDate}
                                    />
                                </Col>

                                <Col md="12">
                                    <div className="form-group">
                                        <ButtonPrimary
                                            fieldLabel="Save"
                                            formLoading={isFormSubmitted}
                                            formLoadingLabel="Saving..."
                                        />
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </PreviewCard>
                </Block>
            </Content>
            <ToastContainer />
        </>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
