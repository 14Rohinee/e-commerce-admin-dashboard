import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    Icon
} from '../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Label, Form, Button } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { showToast } from '../../utils/Utils';
import { APP_URL, slugify, statuses, token } from '../../utils/Constants';
import { useHistory, useParams } from 'react-router';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import {
    Select2,
    Text,
    ButtonPrimary,
    TextArea
} from '../../components/form/Index';
import Tags from '@yaireo/tagify/dist/react.tagify';
import PageLoader from '../../components/page-loader/PageLoader';

const tagifySettings = {
    blacklist: ['xxx', 'yyy', 'zzz'],
    // maxTags: 6,
    // backspace: 'edit',
    addTagOnBlur: false,
    placeholder: 'E.g. Apple, iPhone, 11 Pro Max, Mobile, etc.',
    dropdown: {
        enabled: 1 // shows suggestions dropdown
    }
};

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });
    const [categories, setCategories] = useState([]);
    const [subCategories, setSubCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [longDescriptionText, setLongDescriptionText] = useState('');
    const [product, setProduct] = useState([]);
    const { id } = useParams();
    const [formValues, setFormValues] = useState({ status: '', categoryId: '', subCategoryId: '', brandId: '' });
    const [tagifyOptions, setTagifyOptions] = useState([]);
    const [category, setCategory] = useState([]);
    const [subCategory, setSubCategory] = useState([]);
    const [brand, setBrand] = useState([]);
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [pageLoading, setPageLoading] = useState(true);

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const getCategories = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/categories`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setCategories(response.data.categories);

        const allCategories = response.data.categories.map(option => ({
            value: option._id,
            label: option.name
        }));

        setCategory(allCategories);
    };

    const getSubCategories = async (id) => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/sub-categories/get-sub-categories-by-category-id/${id}`, { headers: { Authorization: `Bearer ${token}` } }
            );

            setSubCategories(response.data.subCategories);
            const allSubCategories = response.data.subCategories.map(option => ({
                value: option._id,
                label: option.name
            }));

            setSubCategory(allSubCategories);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    const getBrands = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/brands`, { headers: { Authorization: `Bearer ${token}` } });

            setBrands(response.data.brands);
            const allBrands = response.data.brands.map(option => ({
                value: option._id,
                label: option.name
            }));

            setBrand(allBrands);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    const getProduct = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/products/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } });

            setFormValues((prevState) => ({
                ...prevState,
                categoryId: response.data.data.product.categoryId,
                subCategoryId: response.data.data.product.subCategoryId,
                brandId: response.data.data.product.brandId,
                status: response.data.data.product.status
            }));

            setTagifyOptions(response.data.data.product.tags);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setProduct(response.data.data.product);
        getSubCategories(response.data.data.product.categoryId);
    };

    // Function to get all the categories on page load
    useEffect(() => {
        const fetchData = async () => {
            await Promise.all([getCategories(), getBrands(), getProduct()]);
            setPageLoading(false);
        };

        fetchData();
    }, []);

    const onTagifyChange = (e) => {
        if (e.detail.value !== '') {
            const arrayVal = JSON.parse(e.detail.value);
            setTagifyOptions(arrayVal);
        } else {
            setTagifyOptions(['']);
        }
    };

    const settings = {
        ...tagifySettings,
        addTagOnBlur: true
    };

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.longDescription = longDescriptionText;
        // Create product name slug
        e.slug = slugify(e.name);
        e.status = formValues.status;
        e.categoryId = formValues.categoryId;
        e.subCategoryId = formValues.subCategoryId;
        e.brandId = formValues.brandId;
        e.tags = [];
        if (tagifyOptions.length > 0) {
            e.tags = tagifyOptions.map((tag) => tag.value);
        }

        try {
            const response = await axios.put(`${APP_URL}/products/update/${id}`, e, { headers: { Authorization: `Bearer ${token}` } });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/products`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Products Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent></BlockHeadContent>
                                        <BlockHeadContent>
                                            <Button
                                                color="light"
                                                outline
                                                className="bg-white d-none d-sm-inline-flex"
                                                onClick={() => history.goBack()}
                                            >
                                                <Icon name="arrow-left"></Icon>
                                                <span>Back</span>
                                            </Button>
                                            <a
                                                href="#back"
                                                onClick={(ev) => {
                                                    ev.preventDefault();
                                                    history.goBack();
                                                }}
                                                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
                                            >
                                                <Icon name="arrow-left"></Icon>
                                            </a>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <PreviewCard>
                                        <Form
                                            className={formClass}
                                            onSubmit={handleSubmit(onFormSubmit)}
                                        >
                                            <Row className="g-gs">
                                                <Col md="12">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Product Name"
                                                        fieldName="name"
                                                        fieldId="name"
                                                        fieldRequired={true}
                                                        fieldPlaceholder="e.g. Apple iPhone 11 Pro Max"
                                                        fieldValue={product.name}
                                                    />
                                                </Col>

                                                <Col md="12">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Short Product Description"
                                                        fieldName="shortDescription"
                                                        fieldId="shortDescription"
                                                        fieldRequired={true}
                                                        fieldPlaceholder="e.g. Apple iPhone 11 Pro Max"
                                                        fieldValue={product.shortDescription}
                                                    />
                                                </Col>

                                                <Col md="12">
                                                    <TextArea
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Long Product Description"
                                                        fieldName="longDescription"
                                                        fieldId="longDescription"
                                                        fieldRequired={false}
                                                        fieldPlaceHolder="e.g. This is best product in mobile category...!"
                                                        onChange={setLongDescriptionText}
                                                        fieldValue={product.longDescription}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Select2
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Category"
                                                        fieldName="categoryId"
                                                        fieldOptions={categories.map((category) => ({
                                                            value: category._id, label: category.name
                                                        }))}
                                                        setValue={(fieldName, value) => {
                                                            onFormChange(fieldName, value);
                                                            getSubCategories(value);
                                                        }}
                                                        fieldValue={category.find(option => option.value === formValues.categoryId)}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Select2
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Sub Category"
                                                        fieldName="subCategoryId"
                                                        fieldOptions={subCategories.map((subCategory) => ({
                                                            value: subCategory._id, label: subCategory.name
                                                        }))}
                                                        setValue={onFormChange}
                                                        fieldValue={subCategory.find(option => option.value === formValues.subCategoryId)}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Select2
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Brands"
                                                        fieldName="brandId"
                                                        fieldOptions={brands.map((brand) => ({
                                                            value: brand._id, label: brand.name
                                                        }))}
                                                        setValue={onFormChange}
                                                        fieldValue={brand.find(option => option.value === formValues.brandId)}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Select2
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Status"
                                                        fieldName="status"
                                                        fieldOptions={statuses}
                                                        setValue={onFormChange} // Pass setValue function
                                                        fieldValue={
                                                            statuses.find(option => option.value === formValues.status)
                                                        }
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Price"
                                                        fieldName="price"
                                                        fieldId="price"
                                                        fieldRequired={true}
                                                        fieldPlaceHolder="e.g. 1000"
                                                        fieldValue={product.price}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Discount Price"
                                                        fieldName="discount"
                                                        fieldId="discount"
                                                        fieldPlaceHolder="e.g. 800"
                                                        fieldValue={product.discount}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Quantity In Stock"
                                                        fieldName="quantityInStock"
                                                        fieldId="quantityInStock"
                                                        fieldRequired={true}
                                                        fieldPlaceHolder="e.g. 109"
                                                        fieldValue={product.quantityInStock}
                                                    />
                                                </Col>

                                                <Col md="3">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="Display Order"
                                                        fieldName="displayOrder"
                                                        fieldId="displayOrder"
                                                        fieldPlaceHolder="e.g. 1"
                                                        fieldValue={product.displayOrder}
                                                    />
                                                </Col>
                                                <Col md="3">
                                                    <Text
                                                        register={register}
                                                        errors={errors}
                                                        fieldLabel="SKU"
                                                        fieldName="sku"
                                                        fieldId="sku"
                                                        fieldRequired={true}
                                                        fieldPlaceHolder="e.g. KHJYGFUF6676"
                                                        fieldValue={product.sku}
                                                    />
                                                </Col>
                                                <Col md="9">
                                                    <div className="form-group">
                                                        <Label
                                                            className="form-label"
                                                            htmlFor="fv-sku"
                                                        >
                                            Tags
                                                        </Label>
                                                        <div className="form-control-wrap">
                                                            <Tags
                                                                className="form-control"
                                                                mode="textarea"
                                                                value={tagifyOptions}
                                                                onChange={(e) => onTagifyChange(e)}
                                                                settings={settings}
                                                                showDropdown={false}
                                                            />
                                                        </div>
                                                    </div>
                                                </Col>

                                                <Col md="12">
                                                    <ButtonPrimary
                                                        fieldLabel="Save"
                                                        formLoading={isFormSubmitted}
                                                        formLoadingLabel="Saving..."
                                                    />
                                                </Col>
                                            </Row>
                                        </Form>
                                    </PreviewCard>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
