import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Label, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { showToast } from '../../utils/Utils';
import { APP_URL, slugify, token, statuses } from '../../utils/Constants';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import Tags from '@yaireo/tagify/dist/react.tagify';
import {
    Select2,
    Text,
    ButtonSecondary,
    ButtonPrimary,
    TextArea
} from '../../components/form/Index';
import FileUpload from '../../components/form/FileUpload';
import PageLoader from '../../components/page-loader/PageLoader';

const tagifySettings = {
    blacklist: ['xxx', 'yyy', 'zzz'],
    // maxTags: 6,
    // backspace: 'edit',
    addTagOnBlur: false,
    placeholder: 'E.g. Apple, iPhone, 11 Pro Max, Mobile, etc.',
    dropdown: {
        enabled: 1 // shows suggestions dropdown
    }
};

const Create = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });
    const [categories, setCategories] = useState([]);
    const [subCategories, setSubCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [longDescriptionText, setLongDescriptionText] = useState('');
    const [tagifyOptions, setTagifyOptions] = useState([]);
    const [formValues, setFormValues] = useState({ status: '', categoryId: '', subCategoryId: '', brandId: '' });
    const [files, setFiles] = useState([]);
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [pageLoading, setPageLoading] = useState(true);

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const handleDropChange = (acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })
            )
        );
    };

    const handleRemoveFile = (index) => {
        const newFiles = [...files];
        newFiles.splice(index, 1);
        setFiles(newFiles);
    };

    const fetchCategories = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/categories`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setCategories(response.data.categories);
    };

    const fetchSubCategories = async (id) => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/sub-categories/get-sub-categories-by-category-id/${id}`, { headers: { Authorization: `Bearer ${token}` } }
            );
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setSubCategories(response.data.subCategories);
    };

    const fetchBrands = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/brands`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setBrands(response.data.brands);
    };

    // Function to get all the categories on page load
    useEffect(() => {
        const fetchData = async () => {
            await Promise.all([fetchCategories(), fetchBrands()]);
            setPageLoading(false);
        };

        fetchData();
    }, []);

    const onTagifyChange = (e) => {
        if (e.detail.value !== '') {
            const arrayVal = JSON.parse(e.detail.value);
            setTagifyOptions(arrayVal);
        } else {
            setTagifyOptions(['']);
        }
    };

    const settings = {
        ...tagifySettings,
        addTagOnBlur: true
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.longDescription = longDescriptionText;
        // Create product name slug
        e.slug = slugify(e.name);
        e.status = formValues.status;
        e.categoryId = formValues.categoryId;
        e.subCategoryId = formValues.subCategoryId;
        e.brandId = formValues.brandId;

        // Create tags array
        e.tags = [];
        if (tagifyOptions.length > 0) {
            e.tags = tagifyOptions.map((tag) => tag.value);
        }

        try {
            const response = await axios.post(`${APP_URL}/products/store`, e, { headers: { Authorization: `Bearer ${token}` } });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/products`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Category Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <Form
                                        className={formClass}
                                        onSubmit={handleSubmit(onFormSubmit)}
                                    >
                                        <Row>
                                            <Col lg="9">
                                                <PreviewCard>
                                                    <Row className="g-gs">
                                                        <Col md="3">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Category"
                                                                fieldName="categoryId"
                                                                fieldOptions={categories.map((category) => ({
                                                                    value: category._id, label: category.name
                                                                }))}
                                                                setValue={(fieldName, value) => {
                                                                    onFormChange(fieldName, value);
                                                                    fetchSubCategories(value);
                                                                }}
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Sub Category"
                                                                fieldName="subCategoryId"
                                                                fieldOptions={subCategories.map((subCategory) => ({
                                                                    value: subCategory._id, label: subCategory.name
                                                                }))}
                                                                setValue={onFormChange}
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Brands"
                                                                fieldName="brandId"
                                                                fieldOptions={brands.map((subCategory) => ({
                                                                    value: subCategory._id, label: subCategory.name
                                                                }))}
                                                                setValue={onFormChange}
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Status"
                                                                fieldName="status"
                                                                fieldOptions={statuses}
                                                                setValue={onFormChange}
                                                            />
                                                        </Col>

                                                        <Col md="12">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Product Name"
                                                                fieldName="name"
                                                                fieldId="name"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="e.g. Apple iPhone 11 Pro Max"
                                                            />
                                                        </Col>

                                                        <Col md="12">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Short Product Description"
                                                                fieldName="shortDescription"
                                                                fieldId="shortDescription"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="e.g. Apple iPhone 11 Pro Max"
                                                            />
                                                        </Col>

                                                        <Col md="12">
                                                            <TextArea
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Long Product Description"
                                                                fieldName="longDescription"
                                                                fieldId="longDescription"
                                                                fieldRequired={false}
                                                                fieldPlaceHolder="e.g. This is best product in mobile category...!"
                                                                onChange={setLongDescriptionText}
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Price"
                                                                fieldName="price"
                                                                fieldId="price"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="e.g. 1000"
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Discount Price"
                                                                fieldName="discount"
                                                                fieldId="discount"
                                                                fieldPlaceHolder="e.g. 800"
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Quantity In Stock"
                                                                fieldName="quantityInStock"
                                                                fieldId="quantityInStock"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="e.g. 109"
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Display Order"
                                                                fieldName="Display Order"
                                                                fieldId="Display Order"
                                                                fieldPlaceHolder="e.g. 1"
                                                            />
                                                        </Col>

                                                        <Col md="3">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="SKU"
                                                                fieldName="sku"
                                                                fieldId="sku"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="e.g. KHJYGFUF6676"
                                                            />
                                                        </Col>

                                                        <Col md="9">
                                                            <div className="form-group">
                                                                <Label
                                                                    className="form-label"
                                                                    htmlFor="fv-sku"
                                                                >
                                                                    Tags
                                                                </Label>
                                                                <div className="form-control-wrap">
                                                                    <Tags
                                                                        className="form-control"
                                                                        mode="textarea"
                                                                        value={tagifyOptions}
                                                                        onChange={(e) => onTagifyChange(e)}
                                                                        settings={settings}
                                                                        showDropdown={false}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </Col>

                                                        <Col md="12">
                                                            <ButtonPrimary
                                                                fieldLabel="Save"
                                                                formLoading={isFormSubmitted}
                                                                formLoadingLabel="Saving..."
                                                            />
                                                        </Col>
                                                    </Row>
                                                </PreviewCard>
                                            </Col>
                                            <Col lg="3">
                                                <PreviewCard>
                                                    <Col md="12">
                                                        <FileUpload
                                                            fieldLabel="Upload Image"
                                                            fieldName="image-dropzone"
                                                            fieldId="image-dropzone"
                                                            handleDropChange={handleDropChange}
                                                            fieldFiles={files}
                                                            handleRemoveFile={handleRemoveFile}
                                                        />
                                                    </Col>
                                                </PreviewCard>
                                            </Col>
                                        </Row>
                                    </Form>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
