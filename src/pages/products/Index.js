import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    formatDate,
    APP_URL,
    token,
    exportData
} from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import PageLoader from '../../components/page-loader/PageLoader';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import ProductDatatable from '../../components/data-table/pages/ProductDatatable';
import ButtonPrimary from '../../components/form/ButtonPrimary';

const Index = ({ pageTitle }) => {
    const [originalData, setOriginalData] = useState([]);
    const [data, setData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the users
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/products`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.products);
        setOriginalData(response.data.products);
        setTotal(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the users on page load
    useEffect(() => {
        getData();
    }, []);

    // Function to export the data
    const exportProducts = async () => {
        const fieldName = null;

        const csvData = data.map((item) => {
            return {
                ProductName: item.name,
                CategoryName: item.category ? item.category.name : '--',
                SubCategoryName: item.subCategory ? item.subCategory.name : '--',
                Brand: item.brand ? item.brand.name : '--',
                Amount: item.price,
                Status: item.status,
                CreatedAt: formatDate(item.createdAt),
                UpdatedAt: item.updatedAt === undefined || item.updatedAt === null ? '--' : formatDate(item.updatedAt)
            };
        });

        // fileName, title, fieldName, data
        exportData('products.csv', 'Products', fieldName, csvData);
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Products..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li className="nk-block-tools-opt">
                                                    <ButtonPrimary
                                                        fieldLabel={pageTitle}
                                                        icon="plus"
                                                        redirectUrl={`${process.env.PUBLIC_URL}/account/products/create`} />
                                                </li>
                                                <li>
                                                    <button type='button' className="btn btn-white btn-outline-light"
                                                        onClick={() => {
                                                            exportProducts();
                                                        } }>
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <ProductDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
