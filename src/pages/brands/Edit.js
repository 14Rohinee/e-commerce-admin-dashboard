import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useParams, useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { showToast } from '../../utils/Utils';
import { APP_URL, slugify, token, statuses } from '../../utils/Constants';
import { useTitle } from '../../contexts/TitleContext';
import PropTypes from 'prop-types';
import FileUpload from '../../components/form/FileUpload';
import {
    Select2,
    Text,
    ButtonSecondary,
    ButtonPrimary
} from '../../components/form/Index';
import PageLoader from '../../components/page-loader/PageLoader';

const Edit = ({ pageTitle }) => {
    const { errors, register, handleSubmit } = useForm();
    const [data, setData] = useState({});
    const [files, setFiles] = useState([]);
    const [formValues, setFormValues] = useState({ status: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [image, setImage] = useState(null);
    const history = useHistory();
    const [pageLoading, setPageLoading] = useState(true);

    // Brand id from params
    const { id } = useParams();

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();

    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const getBrand = async () => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/brands/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } }
            );

            setData(response.data.data);

            setFormValues((prevState) => ({
                ...prevState,
                status: response.data.data.status
            }));

            if (response.data.data.imageName && response.data.data.imagePath) {
                setImage(`${response.data.data.imagePath}/brands/${response.data.data.imageName}`);
            }
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.data);
        setPageLoading(false);
    };

    useEffect(() => {
        // Fetching data from api
        getBrand();
    }, []);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const handleDropChange = (acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })
            )
        );
    };

    const handleRemoveFile = (index) => {
        setImage(null);

        if (index !== null) {
            const newFiles = [...files];
            newFiles.splice(index, 1);
            setFiles(newFiles);
        }
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);

        e.slug = slugify(e.name);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.image = files[0];

        try {
            const response = await axios.put(`${APP_URL}/brands/update/${data._id}`, e, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/brands`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Brands Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <Form
                                        className={formClass}
                                        onSubmit={handleSubmit(onFormSubmit)}
                                        encType="multipart/form-data"
                                    >
                                        <Row>
                                            <Col lg="9">
                                                <PreviewCard>
                                                    <Row className="g-gs">
                                                        <Col md="6">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Brand Name"
                                                                fieldName="name"
                                                                fieldId="name"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="E.g. Samsung"
                                                                fieldValue={data.name}
                                                            />
                                                        </Col>
                                                        <Col md="6">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Status"
                                                                fieldName="status"
                                                                fieldOptions={statuses}
                                                                setValue={onFormChange}
                                                                fieldValue={
                                                                    statuses.find(option => option.value === formValues.status)
                                                                }
                                                            />
                                                        </Col>
                                                        <Col md="12">
                                                            <div className="form-group">
                                                                <ButtonPrimary
                                                                    fieldLabel="Save"
                                                                    formLoading={isFormSubmitted}
                                                                    formLoadingLabel="Saving..."
                                                                />
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </PreviewCard>
                                            </Col>
                                            <Col lg="3">
                                                <PreviewCard>
                                                    <Col md="12">
                                                        <FileUpload
                                                            fieldLabel="Upload Image"
                                                            fieldName="image-dropzone"
                                                            fieldId="image-dropzone"
                                                            handleDropChange={handleDropChange}
                                                            handleRemoveFile={handleRemoveFile}
                                                            fieldFiles={files}
                                                            fieldDefaultFile={image}
                                                        />
                                                    </Col>
                                                </PreviewCard>
                                            </Col>
                                        </Row>
                                    </Form>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>)
            }
        </>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
