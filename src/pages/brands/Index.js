import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import { APP_URL, token, exportData } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { useTitle } from '../../contexts/TitleContext';
import PropTypes from 'prop-types';
import PageLoader from '../../components/page-loader/PageLoader';
import BrandDatatable from '../../components/data-table/pages/BrandDatatable';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import ButtonPrimary from '../../components/form/ButtonPrimary';

const Index = ({ pageTitle }) => {
    const [originalData, setOriginalData] = useState([]);
    const [data, setData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    const { setDocumentTitle } = useTitle();
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the brands
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/brands`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.brands);
        setOriginalData(response.data.brands);
        setTotal(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the brands on page load
    useEffect(() => {
        getData();
    }, []);

    const exportBrands = () => {
        const fieldName = ['Name', 'Status'];

        // fileName, title, fieldName, data
        exportData('brands.csv', 'Brands', fieldName, data);
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Brands..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li className="nk-block-tools-opt">
                                                    <ButtonPrimary
                                                        fieldLabel="Add Brand"
                                                        icon="plus"
                                                        redirectUrl={`${process.env.PUBLIC_URL}/account/brands/create`} />
                                                </li>
                                                <li>
                                                    <button type='button' className="btn btn-white btn-outline-light"
                                                        onClick={() => {
                                                            exportBrands();
                                                        } }>
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <BrandDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
