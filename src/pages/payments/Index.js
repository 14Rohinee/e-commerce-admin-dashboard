import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    formatDate,
    APP_URL,
    exportData,
    token
} from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import PageLoader from '../../components/page-loader/PageLoader';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import PaymentDatatable from '../../components/data-table/pages/PaymentDatatable';

const Index = ({ pageTitle }) => {
    const [originalData, setOriginalData] = useState([]);
    const [data, setData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    const { setDocumentTitle } = useTitle();
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the categories
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/payments`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.payments);
        setOriginalData(response.data.payments);
        setTotal(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the categories on page load
    useEffect(() => {
        getData();
    }, []);

    // Function to export the data
    const exportPayments = async () => {
        const fieldName = null;

        const csvData = data.map((item) => {
            return {
                PaymentID: item.order ? `#INV${item.order.orderNumber}` : '--',
                OrderID: item.order ? `#INV${item.order.orderNumber}` : '--',
                Customer: item.user ? item.user.name : '--',
                Amount: item.amount,
                Status: item.status,
                CreatedAt: formatDate(item.createdAt),
                UpdatedAt: item.updatedAt === undefined || item.updatedAt === null ? '--' : formatDate(item.updatedAt)
            };
        });

        // fileName, title, fieldName, data
        exportData('products.csv', 'Products', fieldName, csvData);
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Payments..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li>
                                                    <button type='button' className="btn btn-white btn-outline-light"
                                                        onClick={() => {
                                                            exportPayments();
                                                        } }>
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <PaymentDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
