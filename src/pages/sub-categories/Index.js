import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    formatDate,
    APP_URL,
    token,
    exportData
} from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import PageLoader from '../../components/page-loader/PageLoader';
import SubCategoryDatatable from '../../components/data-table/pages/SubCategoryDatatable';
import { Block, BlockBetween, BlockHead, BlockHeadContent, Icon } from '../../components/Component';
import ButtonPrimary from '../../components/form/ButtonPrimary';

const Index = ({ pageTitle }) => {
    const [data, setData] = useState([]);
    const [originalData, setOriginalData] = useState([]);
    const [pageLoading, setPageLoading] = useState(true);
    const [total, setTotal] = useState(0);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        itemPerPage: 10,
        sort: 'asc'
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the sub-categories
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/sub-categories`, { params: pagination, headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
        setData(response.data.subCategories);
        setOriginalData(response.data.subCategories);
        setTotal(response.data.total);
        setPageLoading(false);
    };

    // Function to get all the sub-categories on page load
    useEffect(() => {
        getData();
    }, []);

    // Function to export the data
    const exportSubCategories = async () => {
        const fieldName = null;

        const csvData = data.map((item) => {
            return {
                SubCategoryName: item.name,
                CategoryName: item.category ? item.category.name : '--',
                Status: item.status,
                CreatedAt: formatDate(item.createdAt),
                UpdatedAt: item.updatedAt === undefined || item.updatedAt === null ? '--' : formatDate(item.updatedAt)
            };
        });

        // fileName, title, fieldName, data
        exportData('subCategories.csv', 'SubCategories', fieldName, csvData);
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Categories..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content>
                                <BlockHead>
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <ul className="nk-block-tools g-3">
                                                <li className="nk-block-tools-opt">
                                                    <ButtonPrimary
                                                        fieldLabel={pageTitle}
                                                        icon="plus"
                                                        redirectUrl={`${process.env.PUBLIC_URL}/account/sub-categories/create`} />
                                                </li>
                                                <li>
                                                    <button type='button' className="btn btn-white btn-outline-light"
                                                        onClick={() => {
                                                            exportSubCategories();
                                                        } }>
                                                        <Icon name="download-cloud"></Icon>
                                                        <span>Export</span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>
                                <Block>
                                    <SubCategoryDatatable
                                        data={data}
                                        getData={getData}
                                        setData={setData}
                                        originalData={originalData}
                                        total={total}
                                        setTotal={setTotal}
                                        pagination={pagination}
                                        setPagination={setPagination}
                                    />
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )}
        </>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
