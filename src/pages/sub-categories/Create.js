import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, slugify, statuses, token } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import {
    Select2,
    Text,
    ButtonSecondary,
    ButtonPrimary
} from '../../components/form/Index';
import FileUpload from '../../components/form/FileUpload';
import PageLoader from '../../components/page-loader/PageLoader';

const Create = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [categories, setCategories] = useState([]);
    const [formValues, setFormValues] = useState({ category: '', status: '' });
    const [files, setFiles] = useState([]);
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [pageLoading, setPageLoading] = useState(true);

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const handleDropChange = (acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })
            )
        );
    };

    const handleRemoveFile = (index) => {
        const newFiles = [...files];
        newFiles.splice(index, 1);
        setFiles(newFiles);
    };

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);

        e.slug = slugify(e.name);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.categoryId = formValues.category;
        e.image = files[0];

        try {
            const response = await axios.post(`${APP_URL}/sub-categories/store`, e, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                }
            });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/sub-categories`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    // Function to get all the categories
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/categories`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
        setCategories(response.data.categories);
        setPageLoading(false);
    };

    // Function to get all the categories on page load
    useEffect(() => {
        getData();
    }, []);

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Category Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <Form
                                        className={formClass}
                                        onSubmit={handleSubmit(onFormSubmit)}
                                        encType="multipart/form-data"
                                    >
                                        <Row>
                                            <Col lg="9">
                                                <PreviewCard>
                                                    <Row className="g-gs">
                                                        <Col md="6">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Category"
                                                                fieldName="category"
                                                                fieldOptions={categories.map((category) => ({
                                                                    value: category._id, label: category.name
                                                                }))}
                                                                setValue={onFormChange}
                                                            />
                                                        </Col>
                                                        <Col md="6">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Sub Category Name"
                                                                fieldName="name"
                                                                fieldId="name"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="E.g. Samsung"
                                                            />
                                                        </Col>
                                                        <Col md="6">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Status"
                                                                fieldName="status"
                                                                fieldOptions={statuses}
                                                                setValue={onFormChange}
                                                            />
                                                        </Col>
                                                        <Col md="12">
                                                            <ButtonPrimary
                                                                fieldLabel="Save"
                                                                formLoading={isFormSubmitted}
                                                                formLoadingLabel="Saving..."
                                                            />
                                                        </Col>
                                                    </Row>
                                                </PreviewCard>
                                            </Col>
                                            <Col lg="3">
                                                <PreviewCard>
                                                    <Col md="12">
                                                        <FileUpload
                                                            fieldLabel="Upload Image"
                                                            fieldName="image-dropzone"
                                                            fieldId="image-dropzone"
                                                            handleDropChange={handleDropChange}
                                                            fieldFiles={files}
                                                            handleRemoveFile={handleRemoveFile}
                                                        />
                                                    </Col>
                                                </PreviewCard>
                                            </Col>
                                        </Row>
                                    </Form>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
