import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    BlockDes
} from '../../components/Component';
import { useHistory, useParams } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, slugify, token, statuses } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import FileUpload from '../../components/form/FileUpload';
import {
    Select2,
    Text,
    ButtonSecondary,
    ButtonPrimary
} from '../../components/form/Index';
import PageLoader from '../../components/page-loader/PageLoader';

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [data, setData] = useState({});
    const [categories, setCategories] = useState([]);
    const [files, setFiles] = useState([]);
    const [formValues, setFormValues] = useState({ status: '', category: '' });
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [image, setImage] = useState(null);
    const [category, setCategory] = useState([]);

    const [pageLoading, setPageLoading] = useState(true);
    // Category id from params
    const { id } = useParams();

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormChange = (fieldName, value) => {
        setFormValues((prevState) => ({
            ...prevState,
            [fieldName]: value
        }));
    };

    const handleDropChange = (acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file)
                })
            )
        );
    };

    const handleRemoveFile = (index) => {
        setImage(null);

        if (index !== null) {
            const newFiles = [...files];
            newFiles.splice(index, 1);
            setFiles(newFiles);
        }
    };

    const getSubCategory = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/sub-categories/edit/${id}`, { headers: { Authorization: `Bearer ${token}` } });
            const categoryResponse = await axios.get(`${APP_URL}/categories/edit/${response.data.data.subCategory.categoryId}`, { headers: { Authorization: `Bearer ${token}` } });

            setData(response.data.data.subCategory);
            setFormValues((prevState) => ({
                ...prevState,
                status: response.data.data.subCategory.status,
                category: categoryResponse.data.data.category._id
            }));

            if (response.data.data.subCategory.imageName && response.data.data.subCategory.imagePath) {
                setImage(`${response.data.data.subCategory.imagePath}/sub-categories/${response.data.data.subCategory.imageName}`);
            }
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    // Function to get all the categories
    const getCategories = async () => {
        console.log(pageLoading);
        let response;

        try {
            response = await axios.get(`${APP_URL}/categories`, { headers: { Authorization: `Bearer ${token}` } });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setCategories(response.data.categories);

        const allCategories = response.data.categories.map(option => ({
            value: option._id,
            label: option.name
        }));

        setCategory(allCategories);
    };

    useEffect(() => {
        const fetchData = async () => {
            await Promise.all([getSubCategory(), getCategories()]);
            setPageLoading(false);
        };

        fetchData();
    }, []);

    const onFormSubmit = async (e) => {
        setIsFormSubmitted(true);

        e.slug = slugify(e.name);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.status = formValues.status;
        e.categoryId = formValues.category;
        e.image = files[0];

        try {
            const response = await axios.put(
                `${APP_URL}/sub-categories/update/${data._id}`,
                e, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        Authorization: `Bearer ${token}`
                    }
                });

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/sub-categories`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <>
            {
                pageLoading
                    // eslint-disable-next-line multiline-ternary
                    ? <PageLoader
                        text="Loading Category Details..."
                    /> : (
                        <>
                            <Head title={pageTitle}></Head>
                            <Content page="component" CardSize="xl">
                                <BlockHead size="sm">
                                    <BlockBetween>
                                        <BlockHeadContent>
                                            <BlockDes className="text-soft"></BlockDes>
                                        </BlockHeadContent>
                                        <BlockHeadContent>
                                            <ButtonSecondary
                                                fieldLabel="Back"
                                                icon="arrow-left"
                                                redirectUrl="go-back"
                                            />
                                        </BlockHeadContent>
                                    </BlockBetween>
                                </BlockHead>

                                <Block size="sm">
                                    <Form
                                        className={formClass}
                                        onSubmit={handleSubmit(onFormSubmit)}
                                        encType="multipart/form-data"
                                    >
                                        <Row className="g-gs">
                                            <Col lg="9">
                                                <PreviewCard>
                                                    <Row className="g-gs">
                                                        <Col md="6">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Category"
                                                                fieldName="category"
                                                                fieldOptions={categories.map((category) => ({
                                                                    value: category._id, label: category.name
                                                                }))}
                                                                setValue={onFormChange}
                                                                fieldValue={
                                                                    category.find(option => option.value === formValues.category)
                                                                }
                                                            />
                                                        </Col>
                                                        <Col md="6">
                                                            <Text
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Sub Category Name"
                                                                fieldName="name"
                                                                fieldId="name"
                                                                fieldRequired={true}
                                                                fieldPlaceHolder="E.g. Samsung"
                                                                fieldValue={data.name}
                                                            />
                                                        </Col>
                                                        <Col md="6">
                                                            <Select2
                                                                register={register}
                                                                errors={errors}
                                                                fieldLabel="Status"
                                                                fieldName="status"
                                                                fieldOptions={statuses}
                                                                setValue={onFormChange}
                                                                fieldValue={
                                                                    statuses.find(option => option.value === formValues.status)
                                                                }
                                                            />
                                                        </Col>
                                                        <Col md="12">
                                                            <div className="form-group">
                                                                <ButtonPrimary
                                                                    fieldLabel="Save"
                                                                    formLoading={isFormSubmitted}
                                                                    formLoadingLabel="Saving..."
                                                                />
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </PreviewCard>
                                            </Col>
                                            <Col lg="3">
                                                <PreviewCard>
                                                    <Col md="12">
                                                        <FileUpload
                                                            fieldLabel="Upload Image"
                                                            fieldName="image-dropzone"
                                                            fieldId="image-dropzone"
                                                            handleDropChange={handleDropChange}
                                                            handleRemoveFile={handleRemoveFile}
                                                            fieldFiles={files}
                                                            fieldDefaultFile={image}
                                                        />
                                                    </Col>
                                                </PreviewCard>
                                            </Col>
                                        </Row>
                                    </Form>
                                </Block>
                            </Content>
                            <ToastContainer />
                        </>
                    )
            }
        </>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
