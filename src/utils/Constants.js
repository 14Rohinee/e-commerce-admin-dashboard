import { mkConfig, generateCsv, download } from 'export-to-csv';

const APP_URL = process.env.REACT_APP_ENV === 'live' ? process.env.REACT_APP_API_BASE_URL_LIVE : process.env.REACT_APP_API_BASE_URL_LOCAL;

const genders = [
    { value: null, label: 'N/A' },
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' },
    { value: 'other', label: 'Other' }
];

const defaultPaymentEnvironments = [
    { value: 'sandbox', label: 'Sandbox' },
    { value: 'live', label: 'Live' }
];

const statuses = [
    { value: 'all', label: 'All' },
    { value: 'active', label: 'Active' },
    { value: 'inactive', label: 'Inactive' }
];

const roles = [
    // { value: '', label: 'Select' },
    { value: 'admin', label: 'Administrator' },
    { value: 'customer', label: 'Customer' }
];

const discountTypes = [
    { value: 'flat', label: 'Flat' },
    { value: 'percentage', label: 'Percentage' }
];

const bulkActionOptions = [
    { value: 'active', label: 'Enable' },
    { value: 'inactive', label: 'Disable' },
    { value: 'delete', label: 'Delete' },
    { value: 'restore', label: 'Restore' }
];

function getMonthName (month) {
    const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    return monthNames[month];
}

function getShortDayName (day) {
    const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    return dayNames[day];
}

// Function that returns the first or first two letters from a name
const findUpper = (string) => {
    const extractedString = [];

    for (let i = 0; i < string.length; i++) {
        if (
            string.charAt(i) === string.charAt(i).toUpperCase() &&
            string.charAt(i) !== ' '
        ) {
            extractedString.push(string.charAt(i));
        }
    }
    if (extractedString.length > 1) {
        return extractedString[0] + extractedString[1];
    } else {
        return extractedString[0];
    }
};

// Function that calculates the from current date
const setDeadline = (days) => {
    const todayDate = new Date();
    const newDate = new Date(todayDate);
    newDate.setDate(newDate.getDate() + days);
    return newDate;
};

const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

// Function to structure date ex : Jun 4, 2011;
const getDateStructured = (date) => {
    const d = date.getDate();
    const m = date.getMonth();
    const y = date.getFullYear();
    const final = monthNames[m] + ' ' + d + ', ' + y;
    return final;
};

// Function to structure date ex: YYYY-MM-DD
const setDateForPicker = (rdate) => {
    let d = rdate.getDate();
    d < 10 && (d = '0' + d);
    let m = rdate.getMonth() + 1;
    m < 10 && (m = '0' + m);
    const y = rdate.getFullYear();
    rdate = y + '-' + m + '-' + d;

    return rdate;
};

// Set deadlines for projects
const setDeadlineDays = (deadline) => {
    const currentDate = new Date();
    const difference = deadline.getTime() - currentDate.getTime();
    const days = Math.ceil(difference / (1000 * 3600 * 24));
    return days;
};

// Date formatter function Example : 10-02-2004
const dateFormatterAlt = (date, reverse) => {
    const d = date.getDate();
    const m = date.getMonth();
    const y = date.getFullYear();
    reverse ? (date = m + '-' + d + '-' + y) : (date = y + '-' + d + '-' + m);
    return date;
};

// Date formatter function
const dateFormatter = (date, reverse, string) => {
    const dateformat = date.split('-');
    // var date = dateformat[1]+"-"+dateformat[2]+"-"+dateformat[0];
    reverse
        ? (date = dateformat[2] + '-' + dateformat[0] + '-' + dateformat[1])
        : (date = dateformat[1] + '-' + dateformat[2] + '-' + dateformat[0]);

    return date;
};

// todays Date
const todaysDate = new Date();

// current Time
const currentTime = () => {
    let hours = todaysDate.getHours();
    let minutes = todaysDate.getMinutes();
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours || 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    const strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
};

// Percentage calculation
const calcPercentage = (str1, str2) => {
    let result = Number(str2) / Number(str1);
    result = result * 100;
    return Math.floor(result);
};

const truncate = (str, n) => {
    return str.length > n
        ? str.substr(0, n - 1) +
        ' ' +
        truncate(str.substr(n - 1, str.length), n)
        : str;
};

// Converts KB to MB
const bytesToMegaBytes = (bytes) => {
    const result = bytes / (1024 * 1024);
    return result.toFixed(2);
};

// Function to Capitalize first letter of a string
const capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

const dateFormats = [
    { value: 'DD-MM-YYYY', label: 'DD-MM-YYYY (12-01-2024)' },
    { value: 'MM-DD-YYYY', label: 'MM-DD-YYYY (01-12-2024)' },
    { value: 'YYYY-MM-DD', label: 'YYYY-MM-DD (2024-01-12)' },
    { value: 'DD.MM.YYYY', label: 'DD.MM.YYYY (12.01.2024)' },
    { value: 'MM.DD.YYYY', label: 'MM.DD.YYYY (01.12.2024)' },
    { value: 'YYYY.MM.DD', label: 'YYYY.MM.DD (2024.01.12)' },
    { value: 'DD/MM/YYYY', label: 'DD/MM/YYYY (12/01/2024)' },
    { value: 'MM/DD/YYYY', label: 'MM/DD/YYYY (01/12/2024)' },
    { value: 'YYYY/MM/DD', label: 'YYYY/MM/DD (2024/01/12)' },
    { value: 'DD/MMM/YYYY', label: 'DD/MMM/YYYY (12/Jan/2024)' },
    { value: 'DD.MMM.YYYY', label: 'DD.MMM.YYYY (12.Jan.2024)' },
    { value: 'DD-MMM-YYYY', label: 'DD-MMM-YYYY (12-Jan-2024)' },
    { value: 'DD MMM YYYY', label: 'DD MMM YYYY (12 Jan 2024)' },
    { value: 'DD MMMM, YYYY', label: 'DD MMMM, YYYY (12 January, 2024)' },
    { value: 'ddd/MMM/YYYY', label: 'ddd/MMM/YYYY (Fri/Jan/2024)' },
    { value: 'ddd.MMM.YYYY', label: 'ddd.MMM.YYYY (Fri.Jan.2024)' },
    { value: 'ddd-MMM-YYYY', label: 'ddd-MMM-YYYY (Fri-Jan-2024)' },
    { value: 'ddd MMM YYYY', label: 'ddd MMM YYYY (Fri Jan 2024)' },
    { value: 'DD ddd MMM YYYY', label: 'DD ddd MMM YYYY (12 Fri Jan 2024)' },
    { value: 'ddd DD MMM YYYY', label: 'ddd DD MMM YYYY (Fri 12 Jan 2024)' },
    { value: 'DD[th] MMM YYYY', label: 'DD[th] MMM YYYY (12th Jan 2024)' }
];

/*
  Function to format date 2024-01-12T18:19:38.488Z to following format in javascript
    DD-MM-YYYY (12-01-2024)
    MM-DD-YYYY (01-12-2024)
    YYYY-MM-DD (2024-01-12)
    DD.MM.YYYY (12.01.2024)
    MM.DD.YYYY (01.12.2024)
    YYYY.MM.DD (2024.01.12)
    DD/MM/YYYY (12/01/2024)
    MM/DD/YYYY (01/12/2024)
    YYYY/MM/DD (2024/01/12)
    DD/MMM/YYYY (12/Jan/2024)
    DD.MMM.YYYY (12.Jan.2024)
    DD-MMM-YYYY (12-Jan-2024)
    DD MMM YYYY (12 Jan 2024)
    DD MMMM, YYYY (12 January, 2024)
    ddd/MMM/YYYY (Fri/Jan/2024)
    ddd.MMM.YYYY (Fri.Jan.2024)
    ddd-MMM-YYYY (Fri-Jan-2024)
    ddd MMM YYYY (Fri Jan 2024)
    DD ddd MMM YYYY (12 Fri Jan 2024)
    ddd DD MMM YYYY (Fri 12 Jan 2024)
    DD[th] MMM YYYY (12th Jan 2024)
*/

const formatDate = (inputDate, format = 'DD MMM YYYY') => {
    // Convert inputDate to a Date object
    const dateObject = new Date(inputDate);

    const options = {
        day: dateObject.getDate().toString().padStart(2, '0'),
        month: (dateObject.getMonth() + 1).toString().padStart(2, '0'),
        year: format.includes('YYYY')
            ? dateObject.getFullYear().toString()
            : dateObject.getFullYear().toString().slice(-2),
        weekday: getShortDayName(dateObject.getDay())
    };

    if (format.includes('/')) {
        return `${options.day}/${options.month}/${options.year}`;
    } else if (format.includes('.')) {
        return `${options.day}.${options.month}.${options.year}`;
    } else if (format.includes('-')) {
        return `${options.day}-${options.month}-${options.year}`;
    } else if (format.includes(' ')) {
        return `${options.day} ${getMonthName(dateObject.getMonth())} ${options.year
        }`;
    } else {
        return `${options.day} ${getMonthName(dateObject.getMonth())} ${options.year
        }`;
    }
};

/*
1. HH:mm:ss (24-Hour Clock) - 23:45:12
2. hh:mm:ss a (12-Hour Clock with AM/PM) - 11:45:12 PM
3. HH:mm (24-Hour Clock) - 23:45
4. hh:mm a (12-Hour Clock with AM/PM) - 11:45 PM
5. H:mm:ss (24-Hour Clock) - 8:30:45
6. h:mm:ss a (12-Hour Clock with AM/PM) - 8:30:45 AM
7. H:mm (24-Hour Clock) - 8:30
8. h:mm a (12-Hour Clock with AM/PM) - 8:30 AM
*/

const timeFormats = [
    { value: 'hh:mm a', label: 'hh:mm a (11:45 PM)' },
    { value: 'HH:mm:ss', label: 'HH:mm:ss (23:45:12)' },
    { value: 'hh:mm a', label: 'hh:mm a (11:45 PM)' },
    { value: 'HH:mm', label: 'HH:mm (23:45)' },
    { value: 'hh:mm a', label: 'hh:mm a (11:45 PM)' },
    { value: 'HH:mm', label: 'HH:mm (23:45)' },
    { value: 'hh:mm a', label: 'hh:mm a (11:45 PM)' },
    { value: 'HH:mm', label: 'HH:mm (23:45)' }
];

const formatDateTime = (inputDate, format = 'hh:mm a') => {
    // Convert inputDate to a Date object
    const dateObject = new Date(inputDate);

    // Get the date from dateObject and pass it to formatDate function to get the date in the specified format
    const date = formatDate(inputDate, 'DD MMM YYYY');

    const options = {
        hour: dateObject.getHours().toString().padStart(2, '0'),
        minute: dateObject.getMinutes().toString().padStart(2, '0'),
        second: dateObject.getSeconds().toString().padStart(2, '0'),
        ampm: dateObject.getHours() >= 12 ? 'PM' : 'AM'
    };

    if (format.includes('HH')) {
        // return date along with time
        return `${date} ${options.hour}:${options.minute}:${options.second}`;
    }
    if (format.includes('hh')) {
        // return date along with time
        return `${date} ${options.hour}:${options.minute}:${options.second} ${options.ampm}`;
    }
    if (format.includes('H')) {
        // return date along with time
        return `${date} ${options.hour}:${options.minute}`;
    }
    if (format.includes('h')) {
        // return date along with time
        return `${date} ${options.hour}:${options.minute} ${options.ampm}`;
    }
};

// Create a function to generate unique slug for passed string
const slugify = (string) => {
    return string
        .toString()
        .toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w-]+/g, '') // Remove all non-word chars
        .replace(/--+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
};

// Function to validate and return errors for a form
const checkForm = (formData) => {
    const errorState = {};
    Object.keys(formData).forEach((item) => {
        if (formData[item] === null || formData[item] === '') {
            errorState[item] = 'This field is required';
        }
    });
    return errorState;
};

// Bearer Token
const token = localStorage.getItem('accessToken');

// Export data
const exportData = async (fileName, title, fieldNames, data) => {
    let csvData = data;

    if (fieldNames) {
        csvData = data.map((item) => {
            const rowData = {};
            fieldNames.forEach(fieldName => {
                rowData[fieldName] = item[fieldName.toLowerCase()];

                if (fieldName.toLowerCase() === 'status') {
                    rowData[fieldName] = capitalize(item.deletedAt === undefined || item.deletedAt === null ? item.status : 'Deleted');
                }
            });

            rowData.CreatedAt = formatDate(item.createdAt);
            rowData.UpdatedAt = item.updatedAt === undefined || item.updatedAt === null ? '--' : formatDate(item.updatedAt);
            return rowData;
        });
    }

    const csvOptions = {
        filename: fileName,
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true,
        showTitle: false,
        title,
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true
    };

    // mkConfig merges your options with the defaults
    // and returns WithDefaults<ConfigOptions>
    const csvConfig = mkConfig(csvOptions);

    // Converts your Array<Object> to a CsvOutput string based on the configs
    const csv = generateCsv(csvConfig)(csvData);

    return download(fileName, csvConfig)(csv);
};

export { genders, APP_URL, getMonthName, getShortDayName, findUpper, setDeadline, getDateStructured, setDateForPicker, setDeadlineDays, dateFormatterAlt, dateFormatter, todaysDate, currentTime, calcPercentage, truncate, statuses, bytesToMegaBytes, capitalize, dateFormats, formatDate, timeFormats, formatDateTime, slugify, defaultPaymentEnvironments, roles, discountTypes, bulkActionOptions, checkForm, token, exportData };

