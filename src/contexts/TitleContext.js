import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

const TitleContext = createContext();

const useTitle = () => {
    const context = useContext(TitleContext);
    if (!context) {
        throw new Error('useTitle must be used within a TitleProvider');
    }
    return context;
};

const TitleProvider = ({ children }) => {
    const [title, setTitle] = useState('');

    const setDocumentTitle = newTitle => {
        setTitle(newTitle);
    };

    return (
        <TitleContext.Provider value={{ title, setDocumentTitle }}>
            {children}
        </TitleContext.Provider>
    );
};

TitleProvider.propTypes = {
    children: PropTypes.node.isRequired
};

export { TitleProvider, useTitle };
